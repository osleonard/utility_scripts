#!/bin/bash

CHKPNT1="General Steps to be followed during installation"
CHKPNT2="Disk Partitioning and Mounting"
CHKPNT3="Disable IPv6"
CHKPNT4="Add nodev Option to Non-Root Local Partitions"
CHKPNT5="Restrict device ownership to root only"
CHKPNT6="Disable USB device support"
CHKPNT7="Disable GNOME Automounting"
CHKPNT8="Verify Permissions on passwd shadow group and gshadow Files"
CHKPNT9="Add .nodev. Option To Appropriate Partitions In /etc/fstab"
CHKPNT10="Add .nosuid. and .nodev. Option For Removable Media In /etc/fstab"
CHKPNT11="Restrict Programs from Dangerous Execution Patterns"
CHKPNT12="Disable User-Mounted Removable File Systems"
CHKPNT13="Disabling Group and Outside File Access Permissions"
CHKPNT14="Set LILO/GRUB Password"
CHKPNT15="Require Authentication for Single-User Mode"
CHKPNT16="Log as much information about the system through syslog."
CHKPNT17="Syslog permissions."
CHKPNT18="SSH Server"
CHKPNT19="Managing cron and at Jobs"
CHKPNT20="Cron and At Daemons"
CHKPNT21="Services"
CHKPNT22="Page File System (Virtual Memory)/Swap"
CHKPNT23="Berkeley Remote Access Commands (rlogin rsh rcp) should not be allowed"
CHKPNT24="Remote Execution Daemon (rexd) must be disabled"
CHKPNT25="Printer Daemon (lpd, cups) System must be disabled"
CHKPNT26="Post Office Protocol (POP) System must be disabled"
CHKPNT27="IMAP must be disabled completely"
CHKPNT28="Net News Transfer Protocol (NNTP) System must be disabled"
CHKPNT29="NNTP authentication and identification(If enabled)"
CHKPNT30="DNS should be turned off (Except the servers which are hosting DNS and proxy applications)"
CHKPNT31="Sendmail server should be disabled completely unless it is an application requirement or business need"
CHKPNT32="SMB should be turned off completely"
CHKPNT33="BOOTP/DHCP should be disabled completely."
CHKPNT34="All services except ones to support business requirements must be disabled on all servers. "
CHKPNT35="Clock Synchronization . NTP"
CHKPNT36="NFS Below are some Network File System (NFS) System Settings (If enabled)"
CHKPNT37="/etc/exports"
CHKPNT38="/etc/exports "
CHKPNT39="Network File System (NFS) Process for Exporting Confidential Data Without Strong Authentication"
CHKPNT40="FTP Below are Anonymous FTP System Settings (if enabled)"
CHKPNT41="ftpd daemon options"
CHKPNT42="Configuration of the ftp account home directory"
CHKPNT43="Configuration of the bin subdirectory of the ftp account home directory"
CHKPNT44="Configuration of the lib subdirectory of the ftp account home directory"
CHKPNT45="Configuration of the etc subdirectory of the ftp account home directory"
CHKPNT46="Configuration of other files and subdirectories within the ftp account home directory"
CHKPNT47="Directories enabled for Anonymous FTP access "
CHKPNT48="Access permissions for directories accessible via Anonymous FTP"
CHKPNT49="Trivial File Transfer Protocol (TFTP) System Settings (If enabled) tftp access control"
CHKPNT50="Directories enabled for TFTP access"
CHKPNT51="SNMP"
CHKPNT52="/etc/sysctl.conf"
CHKPNT53="/etc/sysctl.conf"
CHKPNT54="/etc/sysctl.conf"
CHKPNT55="Samba"
CHKPNT56="Xinetd"
CHKPNT57="Configure xinetd Access Control "
CHKPNT58="Disable standard Boot Services"
CHKPNT59="Lock the daemon-user accounts related to servers"
CHKPNT60="Enable TCP SYN Cookie Protection"
CHKPNT61="XDMCP should not be  allowed over unencrypted sessions"
CHKPNT62="Disable X Window System Listening"
CHKPNT63="Disable GUI login"
CHKPNT64="Disable X Font Server if possible"
CHKPNT65="Screensaver-password  based Locking of inactive GUI sessions"
CHKPNT66="X-server access control"
CHKPNT67="Preventing Unauthorized Monitoring of Remote X Server"
CHKPNT68="/.rhosts and /.netrc files"
CHKPNT69="/etc/hosts.equiv"
CHKPNT70="/etc/pam.d/rlogin /etc/pam.d/rsh"
CHKPNT71="Network Information Services (NIS) Settings including NIS+ in NIS compatibility mode"
CHKPNT72="yppasswd daemon"
CHKPNT73="NIS maps"
CHKPNT74="Network Information Services Plus (NIS+) Settings NIS+ maps"
CHKPNT75="Enable cryptographic support which is an integral part of the operating systems, subsystems and applications"
CHKPNT76="Secret-key (also known as symmetric or shared key) cryptographic keys "
CHKPNT77="Public/Private key pairs (also known as asymmetric or dual keys) cryptographic keys"
CHKPNT78="Cryptographic Keys for transmitting Confidential Data over Public Networks"
CHKPNT79="Bharti Confidential data stored on Bharti systems/servers"
CHKPNT80="Login success or failure"
CHKPNT81="Messages to be captured"
CHKPNT82="Permissions of System Log Files"
CHKPNT83="/var/log/wtmp"
CHKPNT84="/var/log/messages"
CHKPNT85="/var/log/faillog"
CHKPNT86="/var/log/secure or /var/log/auth.log"
CHKPNT87="/var/log/kernel"
CHKPNT88="/var/log/daemon"
CHKPNT89="/var/log/syslog"
CHKPNT90="Turn On Additional Logging For FTP Daemon"
CHKPNT91="Confirm Permissions On System Log Files"
CHKPNT92="Configure syslogd to Send Logs to a Remote LogHost"
CHKPNT93="Log Archiving"
CHKPNT94="PASS_MAX_DAYS"
CHKPNT95="PASS_MIN_LEN"
CHKPNT96="Minimum Password Age"
CHKPNT97="RedHat 7.1 and newer systems support the remember parameter to the pam_unix.so module"
CHKPNT98="Immediately expire new and manually reset passwords"
CHKPNT99="loginretries"
CHKPNT100="UID"
CHKPNT101="Default UID"
CHKPNT102="Guest/demo id"
CHKPNT103="/etc/passwd"
CHKPNT104="/etc/ftpusers or /etc/vsftpd.ftpusers"
CHKPNT105="/etc/shadow"
CHKPNT106="/etc/shadow"
CHKPNT107="/etc/pam.d/system-auth"
CHKPNT108="/etc/security/$FILENAME"
CHKPNT109="/etc/ftpusers or /etc/vsftpd.ftpusers"
CHKPNT110="/etc/ssh/sshd_config"
CHKPNT111="root/.rhosts"
CHKPNT112="root/.netrc"
CHKPNT113="Verify passwd shadow and group File Permissions"
CHKPNT114="/"
CHKPNT115="/usr"
CHKPNT116="/var"
CHKPNT117="/var/log"
CHKPNT118="/tmp"
CHKPNT119="/etc/snmpd/snmpd.conf"
CHKPNT120="World-Writable Directories Should Have Their Sticky Bit Set"
CHKPNT121="Find Unauthorized World-Writable Files"
CHKPNT122="Find Unauthorized SUID/SGID System Executables"
CHKPNT123="Find and Repair Unowned Files"
CHKPNT124="Disable USB Devices (AKA Hotplugger)"
CHKPNT125="Special Files(socket/pipe/block/character/symbolic link) may be world writeable"
CHKPNT126="LX1.2.4 Protecting Resources - User Resources"
CHKPNT127="$HOME"
CHKPNT128="System id home directories"
CHKPNT129="Yes( In/etc/motd or /etc/issue)"
CHKPNT130="Protect Banner"
CHKPNT131="Create Warning for GUI Based Logins"
CHKPNT132="Transmission"
CHKPNT133="File/Database Storage"
CHKPNT134="Storage of passwords"
CHKPNT135="Protection of private keys"
CHKPNT136="Kernel Tuning"
CHKPNT137="Block System Accounts"
CHKPNT138="Verify there are no accounts with empty password fields"
CHKPNT139="Remove Legacy .+. Entries from Password Files"
CHKPNT140="Verify that No Non-Root Accounts Have UID 0 "
CHKPNT141="Ensure No Dangerous Directories Exist in Root.s Path"
CHKPNT142="User Home Directories are not Group-Writable or World-Readable"
CHKPNT143="No User Dot-Files Should Be World-Writable"
CHKPNT144="Userids that are defined as having Security & System Administrative Authority"
CHKPNT145="ROOT"
CHKPNT146="/etc/pam.d/other"
CHKPNT147="/etc/ftpusers (or /etc/vsftpd.ftpusers for vsftpd)"
CHKPNT148="Lockout Accounts After 3 Failures"
CHKPNT149="Restrict Root Logins to System Console"
CHKPNT150="Limit Access to the Root Account from su"
CHKPNT151="Webmin should be disabled"
CHKPNT152="Remove .rhosts Support In PAM Configuration Files"
CHKPNT153="/etc/passwd"
CHKPNT154="/etc/shadow"
CHKPNT155="/etc/shadow"


#####################################################################################
#####################################################################################

echo "__________________________________________________________________________________"
echo " General Steps to be followed during installation          "
echo "__________________________________________________________________________________"
echo " "
echo " IN Genral We install  The Redhat Server By Redhat Trusted media And We Install the Package  "
echo "  As per the Application requiremsnts "
echo " Checking Sudo is Installed or not  .......... "
if rpm -q sudo 2>/dev/null 
then
echo " Sudo Package is installed on the server ------------  PASSED --------------- "
RSLT1="YES"
else
echo " Sudo Package is not installed           !!!!!!! -----------FAILED -------------!!!!!!!!"
RSLT1="NO"
fi

############################################

echo "__________________________________________________________________________________"
echo "   Disk Partitioning and Mounting             "
echo "__________________________________________________________________________________"
echo " "
 
VG1=`df -h |  grep  -Ev 'none' | grep -w  "\/var$" |awk '{print $NF}' | grep -v /tmp`
VG2=`df -h | grep -Ev 'none' |grep -w  /home$ |awk '{print $NF}'`
VG3=` df -h | grep -Ev 'none' |grep -w /tmp$ |awk '{print $NF}'`
 
if [ -n "$VG1" -a "$VG1" = "/var" ] ; then
echo " /var is Seperate partitions on the System              --------   PASSED --------------- "
else
echo " /var is not the Seperate partitions on the system !!!!!!!!--------------   FAILED ----------!!!!!!!!  "
MOK="no"
fi
 
if [ -n "$VG2" -a "$VG2" = "/home" ] ; then
echo " /home is Seperate partitions on the System              --------   PASSED --------------- "
else
echo " /home is not the Seperate partitions on the system !!!!!!!!--------------   FAILED ----------!!!!!!!!  "
MOK="no"
fi
 
if [ -n "$VG3" -a "$VG3" = "/tmp" ] ; then
echo " /tmp is Seperate partitions on the System                      --------   PASSED --------------- "
else
echo " /tmp is not the Seperate partitions on the system  !!!!!!!!--------------   FAILED ----------!!!!!!!!  "
MOK="no"
fi
 
if [ "$MOK" = "no" ] ; then
echo " Over All testting of Partitions Checked is     !!!!!!!!--------------   FAILED ----------!!!!!!!!  "
RSLT2="NO"
echo " Checking With Disk Usage ....... "
df -h 
echo " ------------------------------- "
else
echo " Over ALL Testing of Partitions Checked is           -------------  PASSED ---------- "
RSLT2="YES"
 
fi

###########################################

echo "__________________________________________________________________________________"
echo "   Disable IPv6   alias net-pf-10 off  , NETWORKING_IPV6=no  ,  IPV6INIT=no  "
echo "__________________________________________________________________________________"
echo " "
 
VG4=`grep net-pf-10  /etc/modprobe.conf | awk '{print $3}' | sed -e 's/ //g'`
VG5=`grep NETWORKING_IPV6  /etc/sysconfig/network | awk -F= '{print $2}' | sed -e 's/ //g' `
VG6=`grep IPV6INIT  /etc/sysconfig/network | awk -F= '{print $2}' | sed -e 's/ //g' `
MOK=""
if [ -n "$VG4" -a -n "$VG4" -a "$VG4" = "off" -a "$VG5" = "no" -a "$VG6" = "no" ] ; then
 
echo " IPv6 Disableing is enable            ----------------     PASSED    ---------- "
RSLT3="YES"
 
else
echo " IPV6 Disabling is  not  Disabled  !!!!!!!!--------------   FAILED ----------!!!!!!!! " 
RSLT3="NO"
echo "  add or change the following lines in /etc/sysconfig/network:  NETWORKING_IPV6=no "
echo "adding the following line to /etc/modprobe.conf:                alias net-pf-10 off  "
echo " "
echo " Add the line to /etc/sysconfig/network:                            IPV6INIT=no  "
 
fi

###########################################

echo " "
echo "__________________________________________________________________________________"
echo "Add nodev Option to Non-Root Local Partitions"
echo "__________________________________________________________________________________"
echo " "
PNT4=`awk ' $3=="ext3" {print $0 }' /etc/fstab | grep -w -v / | grep -v nodev | awk '{print $1}'`
  if [ -n "$PNT4" ]; then
      echo " nodev is not added on /etc/fstab file as per the GSD !!!!!------------FAILED------------- !!!!!!!! "
       echo "_________________________________________________"
       awk ' $3=="ext3" {print $0 }' /etc/fstab | grep -w -v / | grep -v nodev
       echo "_________________________________________________"
       RSLT4="NO"
     else
       echo " nodev is added as per the GSD .. PASSED !!! "
           echo " Checking the /etc/fstab permission and Ownership "
           CHEKFSTAB=`ls -l /etc/fstab | awk '{print $1 $3 $4}'`
           if [ "$CHEKFSTAB" = "-rw-r--r--rootroot" ] ; then
           echo " Permission is OK PASSED !!!........ Permission 644 /etc/fstab and ownership root:root "
           RSLT4="YES"
           else
           echo ""
           echo "Permission is not OK !!!!!!!FAILED !!!!!!..  One or More Settings is Failed in /etc/fstab "
           echo " "
           echo "Check /etc/fstab permission and Ownership it should be 644 and ownership would be root:root "
           echo "`ls -l /etc/fstab`  current settings of /etc/fstab  ....... "
           RSLT4="NO"
           fi
           
       
       echo "_________________________________________________"
       awk ' $3=="ext3" {print $0 }' /etc/fstab | grep -w -v /
       echo "_________________________________________________"
 
  fi
  echo ""
  echo "__________________________________________________________________________________"
 echo "OUTPUT of /etc/fstsb "
cat  /etc/fstab 
echo "__________________________________________________________________________________"

##########################################

echo " "
echo "__________________________________________________________________________________"
echo " Restrict device ownership to root only "
echo "__________________________________________________________________________________"
echo " "
echo " Output of the Files /etc/security/console.perms  "
echo ""
cat /etc/security/console.perms |grep console
echo ""
echo "__________________________________________________________________________________"
 
PNT6=`cat /etc/security/console.perms | awk ' $1=="<console>" {print $0 }' |egrep -v 'sound|fb|kbd|joystick|v4l|mainboard|gpm|scann'`
CHKCONS=`grep  :0\.*:0$  /etc/security/console.perms | grep -v \# | wc -l`
if [ -n "$PNT6" ]; then
  echo " UserMounted Removable File System found enable   !!!!!!!!!------------FAILED------------- !!!!!!!! "
  echo $PNT6
  RSLT5="NO"
 else
  if [ "$CHKCONS" = "2" ] ; then
   echo "UserMounted Removable File System is  disabled  PASSED !!!! "
  RSLT5="YES"
  else
  echo "UserMounted Removable File System is  FAILED !!!!"
  echo " please add below line remove rest line ... "
  echo " <console>=tty[0-9][0-9]* vc/[0-9][0-9]* :0\.[0-9] :0"
  echo "<xconsole>=:0\.[0-9] :0 "
  RSLT5="NO"
  fi
  
fi

########################################

echo "__________________________________________________________________________________"
echo "Disable USB Devices (AKA Hotplugger)"
echo "__________________________________________________________________________________"
echo ""
CHKUSB=`cat /etc/modprobe.conf  |  grep -v \# | grep usb-storage`
if [ -n "$CHKUSB" -a -f /lib/modules/2.6.18-164.el5xen/kernel/drivers/usb/storage/usb-storage.ko ] ; then
echo " Please rename or delete /lib/modules/2.6.18-164.el5xen/kernel/drivers/usb/storage/usb-storage.ko "
echo  " please remove usb-storage from /etc/modprobe.conf "
RSLT6="NO"
else
echo " Disable USB Device (AKA Hotplugger )   PASSED !!!"
echo "  Rest This Disabled  Through BIOS ... PASSED !!!"
RSLT6="YES"
fi

########################################

echo "__________________________________________________________________________________"
echo "   Disable GNOME Automounting...           "
echo "__________________________________________________________________________________"
echo " "
 
if [ -f /usr/bin/gconftool-2 ] ; then  
echo " gconftool-2 is installed ........"
if  gconftool-2 -R /desktop/gnome/volume_manager | grep -i automount_drives| grep false  2>/dev/null 
then
echo " GNOME is Disabled ...........  PASSED ------------  "
RSLT7="YES"
 
else
echo " GNOME Setting is not DISABLED ... !!!!!!!--------------   FAILED ----------!!!!!!! "
echo " Run this command to Disable the  Automount  "
echo " gconftool-2 --direct --config-source xml:readwrite:/etc/gconf/gconf.xml.mandatory --type bool --set /desktop/gnome/volume_manager/automount_media false  "
RSLT7="NO"
fi
else
echo " Gconf-tool2 is  not installed ----------------------PASSED ---------  "
RSLT7="YES"
fi

########################################

echo "__________________________________________________________________________________"
echo "    Verify passwd, shadow, and group File Permissions 644 passwd 400 shadow."
echo "__________________________________________________________________________________"
echo ""
 
val3=` ls -l /etc/passwd | awk '{ print $1$3$4 }' `
val4=`ls -l /etc/shadow | awk '{ print $1$3$4 }'`
val5=`ls -l /etc/group  | awk '{ print $1$3$4 }' `
val6=`ls -l /etc/gshadow| awk '{ print $1$3$4 }'`
if [ "$val3" = "-rw-r--r--rootroot" -a "$val4" = "-r--------rootroot"  -a  "$val5" = "-rw-r--r--rootroot" -a "$val6" = "-r--------rootroot"  ] ; then
   echo " passwd - $val3  and shadow and gshadow -$val4  permission is OK  !!!!!!!!PASSED !!!!!!!!!!!!!! "
    echo " group permisson $val5    PASSED  !!!!!!!!!!!!! "
 
    RSLT8="YES"
    #RSLT134="YES"  ###check the last point
    #RSLT135="YES"   ###check the last point 
 else
 echo "  passwd - $val3  and shadow  -$val4  permission  .!!!!---------FAILED ---------!! "
 echo " group permisson $val5   "
 echo " Please do [ chown root:root /etc/passwd /etc/shadow /etc/group , chmod 644 passwd group ,chmod 400 shadow gshadow ] "
  RSLT8="NO"
  #RSLT134="NO"
  #RSLT135="NO"
 
 fi

########################################

echo " "
echo "__________________________________________________________________________________"
echo " Add nodev Option To Appropriate Partitions In /etc/fstab  "
echo "__________________________________________________________________________________"
echo " "
PNT4=`awk ' $3=="ext3" {print $0 }' /etc/fstab | grep -w -v / | grep -v nodev | awk '{print $1}'`
  if [ -n "$PNT4" ]; then
      echo " nodev is not added on /etc/fstab file as per the GSD !!!!!------------FAILED------------- !!!!!!!! "
       echo "_________________________________________________"
       awk ' $3=="ext3" {print $0 }' /etc/fstab | grep -w -v / | grep -v nodev
       echo "_________________________________________________"
       RSLT9="NO"
     else
       echo " nodev is added as per the GSD .. PASSED !!! "
           echo " Checking the /etc/fstab permission and Ownership "
           CHEKFSTAB=`ls -l /etc/fstab | awk '{print $1 $3 $4}'`
           if [ "$CHEKFSTAB" = "-rw-r--r--rootroot" ] ; then
           echo " Permission is OK PASSED !!!........ Permission 644 /etc/fstab and ownership root:root "
           RSLT9="YES"
           else
           echo ""
           echo "Permission is not OK !!!!!!!FAILED !!!!!!..  One or More Settings is Failed in /etc/fstab "
           echo " "
           echo "Check /etc/fstab permission and Ownership it should be 644 and ownership would be root:root "
           echo "`ls -l /etc/fstab`  current settings of /etc/fstab  ....... "
           RSLT9="NO"
           fi
           
       
       echo "_________________________________________________"
       awk ' $3=="ext3" {print $0 }' /etc/fstab | grep -w -v /
       echo "_________________________________________________"
 
  fi
  echo ""
  echo "__________________________________________________________________________________"
 echo "OUTPUT of /etc/fstsb "
cat  /etc/fstab 
echo "__________________________________________________________________________________"

########################################

echo " "
echo "__________________________________________________________________________________"
echo " Add nosuid and nodev Option For Removable Media In /etc/fstab  "
echo "__________________________________________________________________________________"
 
echo "__________________________________________________________________________________"
 
echo " OUTPUT of /etc/fstsb  for nodev and nosuid "
echo ""
cat  /etc/fstab 
echo ""
echo "__________________________________________________________________________________"
 
echo " "
echo " "
PNT5=`cat /etc/fstab | awk '$3!="ext3" && $3 != "swap" && $3 != "nfs"  &&  $3 != "ext2" { print $0 }' | egrep -w  -v 'devpts|proc|tmpfs|sysfs' |grep -v \# |  grep -v nodev`
 
  if [ -n "$PNT5" ]; then
      echo " nodev is not added on  removal media /etc/fstab file as per the GSD  !!!!!------------FAILED------------- !!!!!!!!! "
 
       RSLT10="NO"
cat /etc/fstab | awk '$3!="ext3" && $3 != "swap" && $3 != "nfs" { print $0 }' | egrep -v 'devpts|proc|tmpfs|sysfs' |grep -v \# |  grep -v nodev
else
       echo " nodev is added on Removal Media as per the GSD .. PASSED !!! "
        PNT5=""
 
PNT5=`cat /etc/fstab | awk '$3!="ext3" && $3 != "swap"  && $3 != "ext2" { print $0 }' | egrep -v -w 'devpts|proc|tmpfs|sysfs' |grep -v \# |  grep -v nosuid`
 
                        if [ -n "$PNT5" ]; then
                           echo " nosuid is not added on removal media /etc/fstab file as per the GSD  FAILED !!! "
cat /etc/fstab | awk '$3!="ext3" && $3 != "swap"  { print $0 }' | egrep  -w -v  'devpts|proc|tmpfs|sysfs' |grep -v \# |  grep -v nosuid            
  RSLT10="NO"
                        else
                                echo " nosuid and nodev is added  on removal media as per the GSD .. PASSED !!! "
                                 RSLT10="YES"
 
 
                 fi
 
  fi

#######################################

echo "__________________________________________________________________________________"
echo "Restrict Programs from Dangerous Execution Patterns  umaks 027 in /etc/sysconfig/init . "
echo "__________________________________________________________________________________"
echo " "
echo " Checking the Enable EXecShield ..... "
VG8=`grep kernel.exec-shield /etc/sysctl.conf | grep -v \# | awk -F= '{print $2}' `
VG9=`grep kernel.randomize_va_space /etc/sysctl.conf |grep -v \# | awk -F= '{print $2}' `
VG10=`grep umask  /etc/sysconfig/init  |awk  '{print $2}' `
if [ -n "$VG8"  -a -n "$VG9" -a -n "$VG10" ] ; then 
  
if [ "$VG8" -eq "1"  -a     "$VG9" -eq "1"  -a  "$VG10" = "027" -o  "$VG10" = "0027"  ] ; then
echo " Kernel Setting OK & Umask in /etc/sysconfig/init--  -------------  PASSED -----------  : "
RSLT11="YES"
 
else
echo " Kernel Setting FAILED --------!!!--------------   FAILED ----------!!!---- "
RSLT11="NO"
echo " add entry in /etc/sysctl.conf kernel.exec-shield = 1     ;   kernel.randomize_va_space = 1 "
echo " Add umask 027 /etc/sysconfig/init   it should be like .. umask 027   " 
fi
else
echo " Value not Found ..for umask and kernel setting          FAILED !!!!!!!!!!!!!!! "
RSLT11="NO"
echo " add entry in /etc/sysctl.conf kernel.exec-shield = 1     ;   kernel.randomize_va_space = 1 "
fi

#######################################

echo " "
echo "__________________________________________________________________________________"
echo " Disable User- Mounted Removable File Systems "
echo "__________________________________________________________________________________"
echo " "
echo " Output of the Files /etc/security/console.perms  "
echo ""
cat /etc/security/console.perms |grep console
echo ""
echo "__________________________________________________________________________________"
 
PNT6=`cat /etc/security/console.perms | awk ' $1=="<console>" {print $0 }' |egrep -v 'sound|fb|kbd|joystick|v4l|mainboard|gpm|scann'`
CHKCONS=`grep  :0\.*:0$  /etc/security/console.perms | grep -v \# | wc -l`
if [ -n "$PNT6" ]; then
  echo " UserMounted Removable File System found enable   !!!!!!!!!------------FAILED------------- !!!!!!!! "
  echo $PNT6
  RSLT12="NO"
 else
  if [ "$CHKCONS" = "2" ] ; then
   echo "UserMounted Removable File System is  disabled  PASSED !!!! "
  RSLT12="YES"
  else
  echo "UserMounted Removable File System is  FAILED !!!!"
  echo " please add below line remove rest line ... "
  echo " <console>=tty[0-9][0-9]* vc/[0-9][0-9]* :0\.[0-9] :0"
  echo "<xconsole>=:0\.[0-9] :0 "
  RSLT12="NO"
  fi
  
fi

#######################################

echo " "
echo "__________________________________________________________________________________"
echo " Disabling Group and Outside File Access Permissions umask value to 077 atleast 027        "
echo "__________________________________________________________________________________"
echo " "
echo " Current UMASK Setting  of $USER ----------  : ` umask ` "
echo "__________________________________________________________________________________"
echo ""
 
 
if [ -f /etc/bashrc  ] ; then
PN1=` grep umask /etc/bashrc | head -2 | tail -1 | grep -v \# | awk '{print $2 }'`
PN5=`grep umask /etc/bashrc | head -1 | tail -1 | grep -v \# | awk '{print $2 }'`
fi
if [ -f  /etc/csh.cshrc ] ; then
PN2=` grep umask /etc/csh.cshrc | head -2 | tail -1 | grep -v \# | awk '{print $2 }'`
fi
if [ -f  /etc/csh.login  ] ; then
PN3=` grep umask /etc/csh.login | head -2 | tail -1 | grep -v \# | awk '{print $2 }'`
fi
 
if [ -f /etc/profile ] ; then
PN4=` grep umask /etc/profile | head -2 | tail -1 | grep -v \# | awk '{print $2 }'`
fi
 
if [ -n "$PN1" -a "$PN1"  = "0077"  -o "$PN1"  = "077"   -o "$PN1"  = "027"  -o "$PN1"  = "0027" -a "$PN5" = "077" -o  "$PN5" = "0077"  -o "$PN5" = "027" -o  "$PN5" = "0027" ] ; then
 echo " /etc/bashrc  -------------------  umask PASSED !!!!!!!!!!!!!!!!!!!!! "
else
mytest="no"
 echo " /etc/bashrc  -------------------  umask !!!------FAILED ---------!!!!!! "
 echo " Set umask  for normal user 077 to all setting in /etc/bashrc .. "
 
fi
 
if [ -n "$PN2" -a "$PN2" = "077" -o "$PN2" = "0077"  -o "$PN2" = "077" -o  "$PN2" = "0077" -o "$PN2" = "027" -o  "$PN2" = "0027" ] ; then
 echo " /etc/csh.cshrc -------------------  umask PASSED !!!!!!!!!!!!!!!!!!!!! "
else
mytest="no"
 echo " /etc/csh.cshrc -------------------  umask !!!------FAILED ---------!!!!!!! "
 
fi
 
if [ -n "$PN3" -a "$PN3" = "077" -o "$PN3" = "0077"  -o "$PN3" = "077" -o  "$PN3" = "0077" -o "$PN3" = "027" -o  "$PN3" = "0027" ] ; then
 echo "  umask /etc/csh.login----------------  umask PASSED !!!!!!!!!!!!!!!!!!!!! "
else
mytest="no"
 echo "  umask /etc/csh.login----------------  umask !!!!------FAILED ---------!!!!!!!!!!!! "
 
fi
 
if [ -n "$PN4" -o "$PN4" = "077" -o "$PN4" = "0077"  -o "$PN4" = "077" -o  "$PN4" = "0077" -o "$PN5" = "0077" -o "$PN5" = "0077"  -o "$PN4" = "0027"  -o "$PN4" = "027" ] ; then
 echo "  /etc/profile  umask PASSED !!!!!!!!!!!!!!!!!!!!! "
else
mytest="no"
 echo "  /etc/profile  umask!!!!!!!------FAILED ---------!!!!!!!!!!!!!!!!! "
 
fi
 
 
### Code UPdated as 2009-October-IM
echo " "
echo " Cheking ALL Users Umask Settings if they Have configured on it's own Profile ... "
echo " "
 
for CHECKUSER in `cat /etc/passwd | awk -F: '$3 > 499 && $3 < 5000{ print $1":"$6 }'| egrep -v 'oradb|oracle|db2|db2inst1'`
do 
USERNAME=`echo $CHECKUSER | awk -F: '{print $1}'`
HOMEDIR=`echo $CHECKUSER | awk -F: '{print $2}'`
 
if [  -f $HOMEDIR/.bash_profile  ] ; then
   if grep umask  $HOMEDIR/.bash_profile  2>/dev/null  >/dev/null
    then
        CHECKUMASK=`grep umask $HOMEDIR/.bash_profile | awk '{print $2}' | tail -1`
               if [ "$CHECKUMASK" = "027" -o "$CHECKUMASK" = "0027" -o "$CHECKUMASK" = "0077" -o "$CHECKUMASK" = "077" ] ; then
                     
                       echo " User --->$USERNAME Umask Settings is -->$CHECKUMASK is OK PASSED !!!!!!! "
                         else
                           echo " User --->$USERNAME Umask Settings is -->$CHECKUMASK is OK FAILED  !!!!!!! [$HOMEDIR/.bash_profile] Should be 027 or 077 "
                           echo " This would be exception for who can be over-ruled by the sub-system settings IM. i.e oracle,oradb,db2inst1 "
                           UTEST=1
                    fi 
    else
        FAKE=1
        fi
elif [ -f $HOMEDIR/.bashrc ] ; then
 
      if grep umask  $HOMEDIR/.bashrc   2>/dev/null  >/dev/null
    then
        CHECKUMASK=`grep $HOMEDIR/.bashrc | awk '{print $2}' | tail -1`
               if [ "$CHECKUMASK" = "027" -o "$CHECKUMASK" = "0027" -o "$CHECKUMASK" = "0077" -o "$CHECKUMASK" = "077" ] ; then
                     
                       echo " User --->$USERNAME Umask Settings is -->$CHECKUMASK is OK PASSED !!!!!!! "
                         else
                           echo " User --->$USERNAME Umask Settings is -->$CHECKUMASK is OK FAILED !!!!!!![$HOMEDIR/.bashrc]  Should be 027 or 077 "
                           echo " This would be exception for who can be over-ruled by the sub-system settings IM. i.e oracle,oradb,db2inst1 "
                           UTEST=1
                    fi 
    else
        FAKE=1
        fi
elif [ -f $HOMEDIR/.cshrc  ] ; then
          
      if grep umask  $HOMEDIR/.cshrc  2>/dev/null  >/dev/null
    then
        CHECKUMASK=`grep umask $HOMEDIR/.cshrc | awk '{print $2}' | tail -1`
               if [ "$CHECKUMASK" = "027" -o "$CHECKUMASK" = "0027" -o "$CHECKUMASK" = "0077" -o "$CHECKUMASK" = "077" ] ; then
                     
                       echo " User --->$USERNAME Umask Settings is -->$CHECKUMASK is OK PASSED !!!!!!! "
                         else
                           echo " User --->$USERNAME Umask Settings is -->$CHECKUMASK is OK FAILED !!!!!!![$HOMEDIR/.cshrc]  Should be 027 or 077 "
                           echo " This would be exception for who can be over-ruled by the sub-system settings IM. i.e oracle,oradb,db2inst1 "
                           UTEST=1
                    fi 
    else
        FAKE=1
        fi
 
 
elif [ -f $HOMEDIR/.tcshrc  ] ; then
          
                  if grep umask  $HOMEDIR/.tcshrc   2>/dev/null  >/dev/null
    then
        CHECKUMASK=`grep umask $HOMEDIR/.tcshrc | awk '{print $2}' | tail -1`
               if [ "$CHECKUMASK" = "027" -o "$CHECKUMASK" = "0027" -o "$CHECKUMASK" = "0077" -o "$CHECKUMASK" = "077" ] ; then
                     
                       echo " User --->$USERNAME Umask Settings is -->$CHECKUMASK is OK PASSED !!!!!!! "
                         else
                           echo " User --->$USERNAME Umask Settings is -->$CHECKUMASK is OK FAILED !!!!!!![$HOMEDIR/.tcshrc]  Should be 027 or 077 "
                           echo " This would be exception for who can be over-ruled by the sub-system settings IM. i.e oracle,oradb,db2inst1 "
                           UTEST=1
                    fi 
    else
        FAKE=1
        fi
 
fi
 
done
 
 
##  END 
 
 
 
if [ "$mytest" = "no" -o "$UTEST" = "1" ] ; then
echo " "
echo " Over ALL -------    Testing of Umask is !!!!!!!!!!!!------FAILED ---------!!!!!!!!!!!!!       "
echo " Note -:Please add all possible umask values to 077 or 027 on failed configuration files  !!!"
echo " Check above logs in failed section to find out which users section umask  failed!!! "
 RSLT13="NO"
else
echo " "
echo " Over ALL including all Users ----------------  Testing of Umask is !!!!!!!!!!!!PASSED !!!!!!!!!!!!! "
 RSLT13="YES"
fi

#######################################

echo " "
echo "__________________________________________________________________________________"
echo "Set LILO/GRUB Password  "
echo "__________________________________________________________________________________"
echo " "
 
if [ -f /etc/lilo.conf ] ; then 
PNT8=`grep password /etc/lilo.conf  | grep -v \# | awk '{ print $1 }'`
else
PNT8=`grep password /etc/grub.conf  | grep -v \# | awk '{ print $3 }'`
 
echo " Testing the Grub Configuration files " 
CHK_PERM=`ls -ltr /boot/grub/grub.conf  | awk '{print $1$3$4}'`
 
fi
echo "__________________________________________________________________________________"
echo " "
cat /etc/grub.conf | grep -v \#
echo "__________________________________________________________________________________"
echo " "
 
 
 
if [ -n "$PNT8" -a "$CHK_PERM" = "-rw-------rootroot" ]; then
  echo " password is set in Grub and Permission is Compliant as per NEW IM  PASSED !!!!!!!!!! "
  RSLT14="YES"
 else
  echo " Password is not set in Grub !!!!!------------FAILED------------- !!!!!!!! "
  echo " Either Permission and ownership  is not Set in /etc/grub.conf "
  echo " Note -: Set the password fields and change the permission to 600 and ownership root:root to /etc/grub.conf " 
  echo " `ls -ltr /boot/grub/grub.conf ` "
   RSLT14="NO"
 
fi

#######################################
 
echo " "
echo "__________________________________________________________________________________"
echo "Require Authentication for Single-User Mode "
echo "__________________________________________________________________________________"
echo " "
echo " "
grep  sulogin /etc/inittab
echo " "
echo "Double ~~ indicates  Authentcation Present in Single User Mode  and Also checking permission and ownership"
echo "__________________________________________________________________________________"
echo " "
 
PNT9=`grep  sulogin /etc/inittab | awk -F: '{print $1 }'`
CHEKPERINIT=`ls -l /etc/inittab | awk '{print $1$3$4}'`
if [ "$PNT9" = "~~" -a "$CHEKPERINIT" = "-rw-r--r--rootroot" -o "$CHEKPERINIT" = "-rw-------rootroot" ]; then
echo " Authentication is enable in Single User Mode and Permission is OK!!  PASSED !!!"
  RSLT15="YES"
 
else
echo " "
echo " Authentication NOT is enable in Single User Mode  !!!!!------------FAILED------------- !!!!!!!!! "
echo " Please add ~~:S:wait:/sbin/sulogin as IM and Permission would be 644 or less  and ownership root:root "
echo " Current Permssion `ls -l /etc/inittab`  "
   RSLT15="NO"
fi

#######################################

echo " "
echo "__________________________________________________________________________________"
echo "syslog setting Log as much information about the system through syslog. "
echo "__________________________________________________________________________________"
echo " "
 
echo "__________________________________________________________________________________"
echo "/etc/syslog.conf has the following entries must be enabled  As per the GSD "
echo "      mail.debug /var/log/mail.log                  Must be enabled  "
echo "       *.info;mail.none /var/log/syslog             Must be enabled "
echo "      auth.*, user.*;daemon.none /var/log/loginlog  Must be enabled "
echo "      kern.* /var/log/kernel                        Must be enabled "
echo "      *.emerg *                                     Must be enabled  "
echo "      *.alert root                                 Must be enabled "
echo "__________________________________________________________________________________"
 
JK1=`grep mail.debug /etc/syslog.conf |grep -v \# | awk '{ print $2 }' ` 
JK2=`grep \*.info\;mail.none /etc/syslog.conf| grep -v \# |awk '{print $2 }' | grep syslog `
JK3=`grep auth.\*\;user.\*  /etc/syslog.conf |grep -v \# | awk '{print $2 }' `
JK4=`grep kern /etc/syslog.conf | grep -v \# | awk '{ print $2 }'`
JK5=`grep \*.emerg /etc/syslog.conf |grep -v \# | awk '{ print $2 }' |head -1`
JK6=`grep  *.alert /etc/syslog.conf |grep -v \#  | awk '{ print $2 }'|head -1`
 
 
 
if [ -n "$JK1" -a  "$JK1" = "/var/log/mail.log" ]
then
    echo "Mail entry is OK "
    
    else
     echo " Mail entry  is not OK !!!!!------------FAILED------------- !!!!!!!!  !!! "
     RKTEST=1
fi 
 
 
if [ -n "$JK2" -a  "$JK2" = "/var/log/syslog" ]
then
    echo "syslog entry is OK "
    else
     echo " syslog entry  is not OK !!!!!------------FAILED------------- !!!!!!!!   !!! "
      RKTEST=1
fi 
 
if [ -n "$JK3" -a  "$JK3" = "/var/log/loginlog" ]
then
    echo "auth entry is OK "
    else
     echo " auth entry  is not  OK   !!!!!------------FAILED------------- !!!!!!!!  !!! "
      RKTEST=1
fi 
 
 
if [ -n "$JK4" -a  "$JK4" = "/var/log/kernel" ]
then
    echo "kernel entry is OK "
    else
     echo "Kernel entry  is not OK   !!!!!------------FAILED------------- !!!!!!!!   !!! "
      RKTEST=1
fi 
 
if [ -n "$JK5" -a  "$JK5" = "*" ]
then
    echo "Emerg entry is OK "
    else
     echo " Emerg entry  is not OK !!!!!------------FAILED------------- !!!!!!!!  !!! "
      RKTEST=1
fi 
 
if [ -n "$JK6" -a  "$JK6" = "root" ]
then
    echo "alert entry is OK "
    else
     echo " alert  entry  is not OK  !!!!!------------FAILED------------- !!!!!!!!   !!! "
      RKTEST=1
fi 
 
  if [ -f  /var/log/mail.log ] ; then
      echo  " /var/log/mail.log  File Found !! "
       if [  -f /var/log/syslog ] ; then
       echo " /var/log/syslog  FILE Found !!! "
           if [ -f /var/log/loginlog ] ; then
            echo " /var/log/loginlog  File Found !!!"
              if [ -f  /var/log/kernel ] ; then
               echo " /var/log/kernel File Found !!!"
                          if [ "$RKTEST" != "1" ] ; then
                            echo " ALL ENTRY OF /etc/syslog.conf is OK          PASSED !!!"
                             RSLT16="YES"
                            else
                             echo " ALL ENTRY OF /etc/syslog.conf is NOT OK      !!!!!------------FAILED------------- !!!!!!!!!!!"
                               RSLT16="NO"
                             fi 
 
               else
               echo "/var/log/kernel File NOT FOUND!!!!!------------FAILED------------- !!!!!!!! !!!"
               fi
            else
            echo " /var/log/loginlog File NOT FOUND !!!!!------------FAILED------------- !!!!!!!! !!!"
            fi
        else
        echo " /var/log/syslog  FILE NOT FOUND  !!!!!------------FAILED------------- !!!!!!!!!!"
        fi
         
        else
        echo " mail.log file not FOUND !! !!!!!------------FAILED------------- !!!!!!!!!"
    fi

########################################

echo " "
echo "__________________________________________________________________________________"
echo "Syslog permissions 600. "
echo "__________________________________________________________________________________"
echo " "
 
for  file in /var/log/syslog  /var/log/loginlog  /var/log/mail.log  /var/log/maillog /var/log/messages  /var/log/secure  /var/log/boot.log  /var/log/kernel 
  do 
    PERM=`ls -l $file | awk '{ print $1 }' `
     if [ "$PERM" = "-rw-------" ]; then 
       echo " File Permisssion of $file is OK   $PERM   PASSED !!!"
      else
       echo " File Permission of $file is Not OK $PERM  !!!!!------------FAILED------------- !!!!!!!!!!"
       RKTEST=2
     fi
done
 if [ "$RKTEST" != "2" ]; then
   echo " All Logs Permission is OK  PASSED !!!!!!!!!!!"
    RSLT17="YES"
  else
    echo " ALL Logs Permission is NOT OK!!!!!!!!!------------FAILED------------- !!!!!!!!!!!!"
     RSLT17="NO"
  fi

#######################################

echo "__________________________________________________________________________________"
echo " /etc/ssh/ssh_config to put Protocol 2"
echo "__________________________________________________________________________________"
echo " "
 
K1=`grep  -w  IgnoreRhosts  /etc/ssh/sshd_config  | grep -v \#  | tail -1  | awk '{print $2 }' `
K2=`grep  -w  RhostsAuthentication   /etc/ssh/sshd_config  | grep -v \#  | tail -1  | awk '{print $2 }' `
K3=`grep  -w  RhostsRSAAuthentication   /etc/ssh/sshd_config  | grep -v \# | tail -1  | awk '{print $2 }' `
K4=`grep  -w  HostbasedAuthentication   /etc/ssh/sshd_config   | grep -v \# | tail -1  | awk '{print $2 }' `
K5=`grep  -w  PermitEmptyPasswords   /etc/ssh/sshd_config  | grep -v \#  | tail -1 | awk '{print $2 }'`
K6=`grep  -w  Banner    /etc/ssh/sshd_config  | grep -v \# | tail -1  | awk '{print $2 }'`
K7=`grep UsePAM /etc/ssh/sshd_config | grep -v \# | awk '{print $2}' | sed -e 's/ //g'`   #### 2009-Oct-Checked 
 
PNT1=` cat /etc/ssh/ssh_config  | grep Protocol | grep -v \# | awk  '{ print $2}'`
    if [ "$PNT1" = "2" ]; then
          echo " Protocol  2 is OK  PASSED !!! "
            PNT2=` grep -i  X11Forwarding  /etc/ssh/sshd_config  | grep -v \# | awk '{ print $2 }' `
            if [ "$PNT2" = "yes" ]; then
               echo " X11 Forwarding is Enabled  PASSED !!!"
                      
                      PNT2=` grep -i  PermitRootLogin /etc/ssh/sshd_config  | grep -v \# | awk '{ print $2 }' `
                      if [ "$PNT2" = "no" ]; then
                         echo " Root Login is Disabled in SSH PASSED  !!!"
                                 echo " It Will Be Exception when Server Run as a GPFS Client and Server " 
 
 
 
 ### IgnoreRhosts 
 
if [  -n "$K1" -o "$K1" = "no" ] ; then
 
 echo " IgnoreRhosts !!!!!!!!!!! PASSED !!!!!!!!!! "
  else
 PTEST="no"
  echo " IgnoreRhosts !!!!!!!!!!!--------- FAILED-------------- !!!!!!!!!!  "
  fi
 
### RhostsAuthentication 
 
if [  -n "$K2" -o "$K2" = "no" ] ; then
 echo " RhostsAuthentication !!!!!!!!!!! PASSED !!!!!!!!!!   "
else
PTEST="no"
 echo "RhostsAuthentication  !!!!!!!!!!!--------- FAILED-------------- !!!!!!!!!! "
fi
 
##  RhostsRSAAuthentication
 
if [  -n "$K3" -o "$K3" = "no" ] ; then
 echo "RhostsRSAAuthentication !!!!!!!!!!! PASSED !!!!!!!!!!   "
else
PTEST="no"
 echo " RhostsRSAAuthentication!!!!!!!!!--------- FAILED-------------- !!!!!!!! "
fi
 
##HostbasedAuthentication
 
if [  -n "$K4" -o "$K4" = "no" ] ; then
 echo " HostbasedAuthentication        !!!!!! PASSED !!!!!!  "
else
PTEST="no"
 echo " HostbasedAuthentication !!!!!!--------- FAILED-------------- !!!!!   "
fi
 
##PermitEmptyPasswords
 
if [  -n "$K5" -o "$K5" = "no" ] ; then
 echo " PermitEmptyPasswords !!!!!!!! PASSED !!!!!!!!!  "
else
PTEST="no"
 echo " PermitEmptyPasswords !!!!!!--------- FAILED-------------- !!!!  "
 
fi
 
### Banner  
 
if [  -n "$K6" -a "$K6" = "/etc/issue.net" ] ; then
 echo " Banner /etc/issue.net  !!!!!!!! PASSED !!!!!!!!!    "
else
PTEST="no"
 echo "  Banner /etc/issue.net  !!!!!!--------- FAILED-------------- !! "
fi
 
 ### Use Pam ==
 
 if [  -n "$K7" -a "$K7" = "yes" ] ; then
 echo " UsePam Settings is Yes  !!!!!!!! PASSED !!!!!!!!!    "
else
PTEST="no"
 echo "  UsePAM yes  !!!!!!--------- FAILED-------------- !!  [[ Should be UsePAM yes ]] "
fi
 
 
 
  if [ "$PTEST" = "no" ] ; then
    echo " Overall SSHD Configuartion  !!!!!!--------- FAILED-------------- !!!!  "
    RSLT18="NO"
  else
   echo " OverALL SSHD configuration !!!!!!!!!!!!!!! PASSED  !!!!!!!!!!!  "
 RSLT18="YES"
fi
 
 
 
                       else
                         echo " Root Login is not Disabled in SSH !!!!!--------FAILED---- !!!!!!!! Exempt GPFS is Running "
                                 echo " It Will Be Exception when Server Run as a GPFS Client or Server " 
                                 
                                 if  /usr/lpp/mmfs/bin/mmgetstate -L  >/dev/null 2>/dev/null
                    then
                    echo " GPFS Clients or Server is Running ........ "
                    echo " It Will be Exception for PermitrootLogin YES as Per NEW IM 2009 "
                    RSLT18="YES"
 
                                        ###########  Code is repeted as per New IM 2009 Since it exists flow on logic
                                        ### IgnoreRhosts 
 
if [  -n "$K1" -o "$K1" = "no" ] ; then
 
 echo " IgnoreRhosts !!!!!!!!!!! PASSED !!!!!!!!!! "
  else
 PTEST="no"
  echo " IgnoreRhosts !!!!!!!!!!!--------- FAILED-------------- !!!!!!!!!!  "
  fi
 
### RhostsAuthentication 
 
if [  -n "$K2" -o "$K2" = "no" ] ; then
 echo " RhostsAuthentication !!!!!!!!!!! PASSED !!!!!!!!!!   "
else
PTEST="no"
 echo "RhostsAuthentication  !!!!!!!!!!!--------- FAILED-------------- !!!!!!!!!! "
fi
 
##  RhostsRSAAuthentication
 
if [  -n "$K3" -o "$K3" = "no" ] ; then
 echo "RhostsRSAAuthentication !!!!!!!!!!! PASSED !!!!!!!!!!   "
else
PTEST="no"
 echo " RhostsRSAAuthentication!!!!!!!!!--------- FAILED-------------- !!!!!!!! "
fi
 
##HostbasedAuthentication
 
if [  -n "$K4" -o "$K4" = "no" ] ; then
 echo " HostbasedAuthentication        !!!!!! PASSED !!!!!!  "
else
PTEST="no"
 echo " HostbasedAuthentication !!!!!!--------- FAILED-------------- !!!!!   "
fi
 
##PermitEmptyPasswords
 
if [  -n "$K5" -o "$K5" = "no" ] ; then
 echo " PermitEmptyPasswords !!!!!!!! PASSED !!!!!!!!!  "
else
PTEST="no"
 echo " PermitEmptyPasswords !!!!!!--------- FAILED-------------- !!!!  "
 
fi
 
### Banner  
 
if [  -n "$K6" -a "$K6" = "/etc/issue.net" ] ; then
 echo " Banner /etc/issue.net  !!!!!!!! PASSED !!!!!!!!!    "
else
PTEST="no"
 echo "  Banner /etc/issue.net  !!!!!!--------- FAILED-------------- !! "
fi
 
 
 
 
  if [  -n "$K7" -a "$K7" = "yes" ] ; then
 echo " UsePam Settings is Yes  !!!!!!!! PASSED !!!!!!!!!    "
else
 
 echo "  UsePAM yes  !!!!!!--------- FAILED-------------- !!  [[ Should be UsePAM yes ]] "
 PTEST="no"
fi
 
 
 
                    else
                    echo " GSD Non - Compliants for sshd configuration "
                     RSLT18="NO"
                                         PTEST="no"
                   fi
 
                         
                          if [ "$PTEST" = "no" ] ; then
               echo " Overall SSHD Configuartion  !!!!!!--------- FAILED-------------- !!!!  "
              RSLT18="NO"
               else
          echo " OverALL SSHD configuration !!!!!!!!!!!!!!! PASSED  !!!!!!!!!!!  "
               RSLT18="YES"
             fi
                         
                         
                      fi
                else
                echo " X11 Forwarding is not enabled !!!!!------------FAILED------------- !!!!!!!!"
                RSLT18="NO"
            fi
         
     else
         echo " Protocol 2 is Not Appear  !!!!!------------FAILED------------- !!!!!!!! "
          RSLT18="NO"
    fi

#######################################

echo " "
echo "__________________________________________________________________________________"
echo "Managing cron and at Jobs. "
echo "__________________________________________________________________________________"
echo " "
 
  echo " Checking the cron.allow and cron.deny presents in the system "
  if [ -f /etc/cron.allow -a -f /etc/cron.deny ] ; then
  echo " /etc/cron.allow /etc/cron.deny File FOUND PASSED !!! "
  RSLT19="YES"
  #RSLT136="YES"
  echo " /etc/cron.allow file ............"
  cat /etc/cron.allow
  echo " /etc/cron.deny file ............"
  cat /etc/cron.deny 
  else
  echo " /etc/cron.allow /etc/cron.deny File NOT FOUND!!!!!------------FAILED------------- !!!!!!!!!!! "
  RSLT19="NO"
  #RSLT136="NO"
  fi

#######################################

echo " "
echo "__________________________________________________________________________________"
echo "  Restrict permissions on crontab files. "
echo "__________________________________________________________________________________"
echo " "
KLM=`ls -l /etc/crontab  |awk '{ print $1$3$4 }'`
CHECKVARSPOOL=`ls -ld  /var/spool/cron | awk '{print $1$3$4}' `
if [ "$KLM" = "-r--------rootroot" -a "$CHECKVARSPOOL" = "drwx------rootroot" ] ; then
 echo " /etc/crontab ownership is root OK PASSED !!!"
 echo " /var/spool/cron directory permission is OK .. "
cd /var/spool/cron
    for file in `ls /var/spool/cron`
         do
    PERM=`ls -l $file |  awk '{print $1$3$4}' `
     if [ "$PERM" = "-rw-------rootroot" ]; then 
       echo " File Permisssion of $file is OK   $PERM   PASSED !!!"
      else
           echo " "
       echo " File Permission of $file is Not OK $PERM  !!!!!------------FAILED------------- !!!!!!!!!!"
           echo " File Permssion of $file in /var/spool/cron should be 600 and permission would be root:root "
           echo " "
       RKTEST1=2
      fi
         done
 
 else
 echo " /etc/crontab ownership is not root!!!!!------------FAILED------------- !!!!!!!!!!"
  echo " /var/spool/cron directory permssion Failed  !!!------------FAILED------------- !!!! "
 echo " Please  do [ chown root:root /etc/crontab ] [chmod 400 /etc/crontab] "
 echo " [ chown -R root:root /var/spool/cron ]  [ chmod ?R go-rwx /var/spool/cron ] "
 
 fi
 
 if [ "$RKTEST1" != "2" ] ; then
  echo " ALL Files in crontab  permisson Ristrictions is  OK  PASSED !!!!"
   RSLT20="YES"
  else
  echo " "
  echo " crontab permisson Ristrictions is  Not OK !!!!!------------FAILED------------- !!!!!!!!!! "
 
 echo " Please  Make  [ chown root:root /etc/crontab ] [chmod 400 /etc/crontab] "
 echo " [ chown ?R root:root /var/spool/cron ]  [ chmod ?R go-rwx /var/spool/cron ] "
  
     RSLT20="NO"
 
  fi

#######################################

echo " "
echo "__________________________________________________________________________________"
echo " Services in RHEL5 Should be Disabled  "
echo "__________________________________________________________________________________"
echo " "
 
for SER in firstboot    gpm     isdn    irqbalance      kdump   kudzu   mdmonitor       microcode ctl           portmap         rhnsd   rpcgssd       rpcidmapd       xfs     yum-updatesd    pcscd   readahead_early         readahead _later        haldaemon       bluetooth       hidd  autofs           avahi-daemon    cups    haldaemon       hidd    hplip   ip6tables       iptables        mcstrans        nfslock
do
 
if ps ax | grep -v grep | grep -w $SER >/dev/null
then
    echo "<<<<<<<<<<<< $SER >>>>>>>>> service -----------------------running, FAILED !!!  !!!"
   MKT=1
 
   else
 
if chkconfig --list $SER  2>/dev/null >/dev/null 
then
YK=`chkconfig --list $SER | awk '{print $5$7 }'`
   if [ "$YK" = "3:off5:off" ] ; then
   echo "$SER Service is stoppped !!!! PASSED !!! "
   else
   echo " $SER Service is ==================== Runnning FAILED !!!!!!!!!!!!!! please do chkconfig $SER off "
   MKT=1
   fi
 else
 echo " $SER is not installed .......... PASSED !!!"
 fi
 
fi
 
 done
 
 CHKBL=`grep net-pf-31  /etc/modprobe.conf  | awk '{print $2}'`
 
 if [ "$CHKBL" = "net-pf-31" -a  "$MKT" = "1" ] ; then
 echo " "
 echo " Please Check ALL Services which Need to stopped but it is running  Please check FAILED !!!!!!"
 echo " Please check net-pf-31   in /etc/modprobe.conf "
 RSLT21="NO"
 else
 echo " ALL Services are stopped !!! PASSED !!!! "
 RSLT21="YES"
 fi

#######################################

echo " "
echo "__________________________________________________________________________________"
echo " Clear the page file at shut down "
echo "__________________________________________________________________________________"
echo " "

echo " Linux Systems by default clears page file at shutdown"
RSLT22="YES"

#######################################

echo " "
echo "__________________________________________________________________________________"
echo "Berkeley Remote Access Commands (rlogin, rsh, rcp) should not be allowed. "
echo "__________________________________________________________________________________"
echo ""
echo " The RSH is mandatory requirement of CRM and OSI where RAC , CRM & Financial Application "
echo " "
 
if [ -f /etc/xinetd.d/rlogin ] ; then
       echo " rlogin exist in the SYSTEM !!! "
        MKL=`grep disable /etc/xinetd.d/rlogin  | awk -F= '{ print $2 }'`
           if [ "$MKL" = " no" ] ; then
              echo " rlogin is enabled on the system !!!!!!------------FAILED------------- !!!!!! "
               RSLT23="NO"
              
            else
               echo " rlogin is not enabled on the system  PASSED !!!" 
                
                RSLT23="YES"
                JKT=5
            fi
  else
   echo " rlogin is not installed on the system  PASSED !!!!"
 RSLT23="YES"
 fi
 
   
   if [ -f /etc/xinetd.d/rsh ] ; then
       echo " rsh exist in the SYSTEM !!! "
        MKL=`grep disable /etc/xinetd.d/rsh  | awk -F= '{ print $2 }'`
           if [ "$MKL" = " no" ] ; then
              echo " rsh is enabled on the system !!!!!!!------------FAILED------------- !!!!!!!!!! "
                RSLT23="NO"
              
            else
               echo " rsh is not enabled on the system  PASSED !!!" 
               if [ "$JKT" = "5" ]; then  
                RSLT23="YES"
               else
               echo " rlogin is enabled on the system so overall  gsd checked is !!!!!------------FAILED------------- !!!!!!!!!"
                RSLT23="NO" 
               fi 
            
            fi
  else
   echo " rsh is not installed on the system  PASSED !!!!"
 RSLT23="YES"
 fi

#######################################

echo " "
echo "__________________________________________________________________________________"
echo "Remote Execution Daemon (rexd) must be disabled. "
echo "__________________________________________________________________________________"
echo ""
 
 SERVICE="rexd"
if ps ax | grep -v grep | grep $SERVICE >/dev/null
then
    echo "$SERVICE service running, !!!!!------------FAILED------------- !!!!!!!! !!!"
    RSLT24="NO"
else
  JKLM1=`chkconfig --list  |grep -w rexd | awk '{ print $5 $7 }'`
  if [ -n "$JKLM1" ] ; then
 
   if [  "$JKLM1" = "3:off5:off" ] ; then
   echo " rexd  is Disabled   PASSED !!!"
   RSLT24="YES"
   else
   echo " rexd Is not Disabled !!!!!------------FAILED------------- !!!!!!!!!!"
   RSLT24="NO"
  
    fi
   else
    echo " rexd IS NOT INSTALLED !!! PASSED !!!!!!!"
     RSLT24="YES"
    fi
     
 fi

#######################################

echo " "
echo "__________________________________________________________________________________"
echo "chkconfig lpd off "
echo "__________________________________________________________________________________"
echo ""
 
         SERVICE="lpd"
if ps ax | grep -v grep | grep $SERVICE >/dev/null
then
    echo "$SERVICE service running,!!!!!------------FAILED------------- !!!!!!!!!!"
    RSLT25a="NO"
else
  JKLM1=`chkconfig --list  |grep -w lpd | awk '{ print $5 $7 }' | head -1`
  if [ -n "$JKLM1" ] ; then
 
   if [  "$JKLM1" = "3:off5:off" ] ; then
   echo " lpd  is Disabled   PASSED !!!"
   RSLT25a="YES"
   else
   echo " lpd Is not Disabled !!!!!------------FAILED------------- !!!!!!!!!!"
   RSLT25a="NO"
  
    fi
   else
    echo " lpd IS NOT INSTALLED !!! PASSED !!!!!!!"
     RSLT25a="YES"
    fi
     
 fi
 
echo " "
echo "__________________________________________________________________________________"
echo "chkconfig cups off "
echo "__________________________________________________________________________________"
echo ""
 
 
         SERVICE="cupsd"
if ps ax | grep -v grep | grep -w $SERVICE >/dev/null
then
    echo "$SERVICE service running,!!!!!------------FAILED------------- !!!!! !!!"
    RSLT25b="NO"
else
  JKLM1=`chkconfig --list |grep -w cups | awk  ' $1 != "cups-config-daemon" && $2 != "cups-lpd:" { print $5 $7 }'|head -1 `
  if [ -n "$JKLM1" ] ; then
 
   if [  "$JKLM1" = "3:off5:off" ] ; then
   echo " cups  is Disabled   PASSED !!!"
   RSLT25b="YES"
   else
   echo "  cups Is not Disabled !!!!!------------FAILED------------- !!!!!!!!!!"
   RSLT25b="NO"
  
    fi
   else
    echo " cups  IS NOT INSTALLED !!! PASSED !!!!!!!"
     RSLT25b="YES"
    fi
     
 fi

if [ $RSLT25a = "YES" -a $RSLT25b = "YES" ] ; then
  RSLT25="YES"
else
  RSLT25="NO"
fi

#######################################

echo " "
echo "__________________________________________________________________________________"
echo "Post Office Protocol (POP) System must be disabled "
echo "__________________________________________________________________________________"
echo ""
 
 
         SERVICE="pop3d"
if ps ax | grep -v grep | grep $SERVICE >/dev/null
then
    echo "$SERVICE service running, !!!!!------------FAILED------------- !!!!!!!!  !!!"
    RSLT26="NO"
else
  JKLM1=`chkconfig --list  |grep -w pop3d | awk '{ print $5 $7 }' | head -1`
  if [ -n "$JKLM1" ] ; then
 
   if [  "$JKLM1" = "3:off5:off" ] ; then
   echo " pop  is Disabled   PASSED !!!"
   RSLT26="YES"
   else
   echo "  pop Is not Disabled !!!!!------------FAILED------------- !!!!!!!!!!"
   RSLT26="NO"
  
    fi
   else
    echo " pop  IS NOT INSTALLED !!! PASSED !!!!!!!"
     RSLT26="YES"
    fi
     
 fi

#######################################

echo " "
echo "__________________________________________________________________________________"
echo "IMAP must be disabled completely "
echo "__________________________________________________________________________________"
echo ""
 
 
         SERVICE="imapd"
if ps ax | grep -v grep | grep $SERVICE >/dev/null
then
    echo "$SERVICE service running, !!!!!------------FAILED------------- !!!!!!!!!!!"
    RSLT27="NO"
else
  JKLM1=`chkconfig --list  |grep -w imap | awk '{ print $5 $7 }'`
  if [ -n "$JKLM1" ] ; then
 
   if [  "$JKLM1" = "3:off5:off" ] ; then
   echo " imap is Disabled   PASSED !!!"
   RSLT27="YES"
   else
   echo "  imap Is not Disabled !!!!!------------FAILED------------- !!!!!!!!!!"
   RSLT27="NO"
  
    fi
   else
    echo " imap  IS NOT INSTALLED !!! PASSED !!!!!!!"
     RSLT27="YES"
    fi
     
 fi

#######################################

echo " "
echo "__________________________________________________________________________________"
echo "Net News Transfer Protocol (NNTP) System must be disabled "
echo "__________________________________________________________________________________"
echo ""
         SERVICE="nntp"
if ps ax | grep -v grep | grep $SERVICE >/dev/null
then
    echo "$SERVICE service running,!!!!!------------FAILED------------- !!!!!!!!  !!!"
    RSLT28="NO"
else
  JKLM1=`chkconfig --list  |grep -w $SERVICE  | awk '{ print $5 $7 }'`
  if [ -n "$JKLM1" ] ; then
 
   if [  "$JKLM1" = "3:off5:off" ] ; then
   echo " nntp  is Disabled   PASSED !!!"
   RSLT28="YES"
   else
   echo "  nntp Is not Disabled !!!!!------------FAILED------------- !!!!!!!!!!"
   RSLT28="NO"
  
    fi
   else
    echo " nntp  IS NOT INSTALLED !!! PASSED !!!!!!!"
     RSLT28="YES"
    fi
     
 fi

#######################################

echo " "
echo "__________________________________________________________________________________"
echo "NNTP authentication and identification "
echo "__________________________________________________________________________________"
echo ""
         SERVICE="nntp"
if ps ax | grep -v grep | grep -w $SERVICE >/dev/null
then
 for name in `echo $PATH | tr ':' '\n'`
 do  
  if [ -x $name/authinfo ] ; then  
   echo "Authentication Command Exist"
   RSLT29="YES"
  else 
   echo "Not exists"
   RSLT29="NO"
  fi 
 done
else
 RSLT29="YES"
fi

#######################################

echo " "
echo "__________________________________________________________________________________"
echo "DNS server should be turned off. "
echo "__________________________________________________________________________________"
echo ""
 
MT=`hostname`
MU=` echo $MT | grep -i dns `
 
if [ -n "$MU" ] ; then
echo " As per Hostname this Server is Operating as DNS Server"
else
echo " As per Hostname this Server is not Operating as DNS Server"
FALSE_VAL1="yes"
fi
 
 
if  ps  ax | grep -v grep | grep -w -i squid >/dev/null
 then
 echo " squid Service is running hence Server is Running as Proxy Server and requires DNS "
 else
echo " squid Service is not running hence Server is not Running as Proxy Server and does not require DNS "
 FALSE_VAL2="yes"
 fi
 
 
 
         SERVICE="named"
if ps ax | grep -v grep | grep -w $SERVICE >/dev/null
then
    echo "$SERVICE dns  service running, !!!!!------------FAILED------------- !!!!!!!! !!!"
        echo " It will be Treated as Exception if Server is  running as a Proxy and DNS Server "
 
        if [ "$FALSE_VAL1" = "yes" ] ; then
    RSLT30="NO"
        else
          RSLT30="YES"
        fi 
 
else
  JKLM1=`chkconfig --list  |grep -w $SERVICE  | awk '{ print $5 $7 }'`
  if [ -n "$JKLM1" ] ; then
 
   if [  "$JKLM1" = "3:off5:off" ] ; then
   echo "  dns  is Disabled   PASSED !!!"
   RSLT30="YES"
   else
   
   echo "   dns Is not Disabled!!!!!------------FAILED------------- !!!!!!!!!"
   echo " It will be Treated as Exception if Server is  running as a Proxy and DNS Server "  
           if [ "$FALSE_VAL2" = "yes" ] ; then
    RSLT30="NO"
        else
          RSLT30="YES"
        fi  
 
    fi
   else
    echo " dns   IS NOT INSTALLED !!! PASSED !!!!!!!"
     RSLT30="YES"
    fi
     
 fi

#######################################

echo " "
echo "__________________________________________________________________________________"
echo "Sendmail server should be disabled completely "
echo "__________________________________________________________________________________"
echo ""
 SERVICE="sendmail"
  JKLM1=`chkconfig --list  |grep -w $SERVICE  | awk '{ print $5 $7 }'`
  if [ -n "$JKLM1" ] ; then
 
   if [  "$JKLM1" = "3:off5:off" ] ; then
   echo " sendmail  is Disabled   PASSED !!!"
   RSLT31="YES"
   else
   echo "  sendmail Is not Disabled !!!!!------------FAILED------------- !!!!!!!!!!!"
   RSLT31="NO"
  
    fi
   else
    echo " sendmail  IS NOT INSTALLED !!! PASSED !!!!!!!"
     RSLT31="YES"
    fi

#######################################

echo " "
echo "__________________________________________________________________________________"
echo "SMB should be turned off completely. "
echo "__________________________________________________________________________________"
echo ""
 
         SERVICE="smbd"
if ps ax | grep -v grep | grep $SERVICE >/dev/null
then
    echo "$SERVICE   service running, FAILED !!!  !!!"
    RSLT32="NO"
else
  JKLM1=`chkconfig --list  |grep -w smb  | awk '{ print $5 $7 }'`
  if [ -n "$JKLM1" ] ; then
 
   if [  "$JKLM1" = "3:off5:off" ] ; then
   echo "  SMB  is Disabled   PASSED !!!"
   RSLT32="YES"
   else
   echo "   SMB Is not Disabled !!!!!------------FAILED------------- !!!!!!!!!!"
   RSLT32="NO"
  
    fi
   else
    echo " SAMBA   IS NOT INSTALLED !!! PASSED !!!!!!!"
     RSLT32="YES"
    fi
     
 fi

#######################################

echo " "
echo "__________________________________________________________________________________"
echo "BOOTP/DHCP should be disabled completely "
echo "__________________________________________________________________________________"
echo ""
  
         SERVICE="dhcpd"
if ps ax | grep -v grep | grep $SERVICE >/dev/null
then
    echo "$SERVICE dns  service running,!!!!!------------FAILED------------- !!!!!!!!!!!"
    RSLT33="NO"
else
  JKLM1=`chkconfig --list  |grep -w $SERVICE  | awk '{ print $5 $7 }'`
  if [ -n "$JKLM1" ] ; then
 
   if [  "$JKLM1" = "3:off5:off" ] ; then
   echo "  dhcp is Disabled   PASSED !!!"
   RSLT33="YES"
   else
   echo "   dhcp Is not Disabled !!!!!------------FAILED------------- !!!!!!!!!"
   RSLT33="NO"
  
    fi
   else
    echo " dhcp  IS NOT INSTALLED !!! PASSED !!!!!!!"
     RSLT33="YES"
    fi
     
 fi

#######################################

echo " "
echo "__________________________________________________________________________________"
echo "All services except ones to support business requirements must be disabled all servers."
echo "__________________________________________________________________________________"
echo ""
 
cd /etc/xinetd.d
for FILE in chargen chargen-udp cups-lpd daytime daytime-udp eklogin finger gssftp imap imaps ipop2 ipop3 krb5-telnet klogin kshell ktalk ntalk pop3s rexec rlogin rsh rsync servers services sgi_fam shell talk telnet tftp time time-udp  wu-ftpd echo
do
if chkconfig --list   $FILE  >   /dev/null 2>&1 
 then
JKLM1=`chkconfig --list   $FILE  | awk '{ print $2 }' | sed -s 's/0://g'`
   if [  "$JKLM1" = "off" ] ; then
   echo "  $FILE is Disabled   PASSED !!!!!!!!!!!!!"
   else
   echo "   $FILE Is not Disabled !!!!!------------FAILED------------- !!!!!!!!!!"
  JK31="NO"
  
    fi
 
else
echo " $FILE is not installed  !!! PASSED!!!!!!!!!!!!!"
fi
done
if [ "$JK31" = "NO" ] ; then
  RSLT34="NO"
 echo " ALL service is not Disabled -----!!!!!------------FAILED-------------!!!!!  "
else
 echo " ALL service is Disabled PASSED !!!!!!!!!!!!!!!!!!!  "
 RSLT34="YES"
fi

#######################################

echo "_________________________________________________________________________________"
echo "   Clock Synchronization NTP          "
echo "__________________________________________________________________________________"
echo " "
echo " "
echo " "
 
 
 
 
   
   MKST2=`grep -w  "ignore" /etc/ntp.conf | grep -v \#  | awk '{print $1 $2 $3}' `
   MKST3=`grep server /etc/ntp.conf | grep -v \# | grep -v pool.ntp.org | awk '{print $2}' `
   MKST4=`grep 255.255.255.0  /etc/ntp.conf | grep -v \# | awk '{ print $1 $2 $3 $4 $5 $6 $7 }' `
 
   if [ "$MKST2" = "restrictdefaultignore" -a "$MKST3" = "10.14.168.13"  -a "$MKST4" = "restrict10.14.168.13mask255.255.255.0nomodifynotrapnoquery" ] ; then
     
     echo " All Setting in /etc/ntp.conf is OK -- !!!!!!!! PASSED !!!!!!!!!!!! " 
       
         echo " Checking the NTPD Daemon running or not !!!!!! "
 
         if ps -ef | grep -v grep | grep -w ntpd  2>/dev/null 
 
         then
         echo " NTPD Process is Running OK and $MKST3 is configured as NTP Server.  --------- PASSED !!!!!!!!"
         echo " `ntpq -p ` "
         echo " "
         echo " Overall Testing is !!!!!!!!!PASSED !!!!!!!!!!!!!! "
         echo " "
         RSLT35="YES"
         else
         echo " NTPD Daemon is not Running --!!!!!!!!!FAILED !!!!!!!!!!!! "
         RSLT35="NO"
 
         fi
       
   else
     
   echo " Entry not Found !!!!!!!!  for  /etc/ntp.conf  !!!!!!!FAILED !!!!!!!!!! "
   echo " "
   echo "  "
   echo " Please Add given Value  to /etc/ntp.conf to make it Compliant  and Start the ntpd Daemons " 
   echo " Make it Hash  for Other Ntp server entry i.e 0.pool.ntp.org "
   echo " server 10.14.168.13 "
   echo " restrict 10.14.168.13 mask 255.255.255.0 nomodify notrap noquery "
   echo " restrict default ignore " 
   RSLT35="NO"
 
   fi

#######################################

echo " "
echo "__________________________________________________________________________________"
echo "chkconfig nfs off. "
echo "__________________________________________________________________________________"
echo " "
 
SERVICE="nfs"
if ps ax | grep -v grep | grep -w  $SERVICE >/dev/null
then
    echo "$SERVICE service running,!!!!!------------FAILED------------- !!!!!!!!!!!! Only Encrypted NFS is ALLOWED !!! "
        echo " Checking the NFS Whether it is running in encrpted mode  or not "
        CHECKIP=`hostname -i`
        VALOFNFS=` cat /etc/exports | grep  -v $CHECKIP | grep -Ev 'localhost|127.0.0.1'| grep -v \#  `
 
        if [ -n "$VALOFNFS" ] ; then
        echo "No ENCRYPTED NFS IS Configured !! .........  FAILED !!!!!!!!!!  "
    RSLT36a="NO"
        else
        RSLT36a="YES"
        echo "ENCRYPTED NFS IS Configured !! .........  PASSED !!!!!!!!!!  "
        fi
 
else
  JKLM1=`chkconfig --list  |grep -w nfs | awk '{ print $5 $7 }'`
  if [ -n "$JKLM1" ] ; then
 
   if [  "$JKLM1" = "3:off5:off" ] ; then
   echo " NFS  is Disabled   PASSED !!!"
   RSLT36a="YES"
   else
        CHECKIP=`hostname -i`
        VALOFNFS=` cat /etc/exports | grep  -v $CHECKIP | grep -Ev 'localhost|127.0.0.1'| grep -v \#  `
 
        if [ -n "$VALOFNFS" ] ; then
        echo "No ENCRYPTED NFS IS Configured !! .........  FAILED !!!!!!!!!! it is in Enable state!!! FAILED !!!  "
    RSLT36a="NO"
        else
        RSLT36a="YES"
        echo "ENCRYPTED NFS IS Configured !! .........  PASSED !!!!!!!!!!  "
   echo " NFS Is not Disabled [ Informational ] !!!!!-----------PASSED-------- !!!!!!!! Using Encryptions! GSD Compliants"
 
        fi
    
    fi
   else
    echo " NFS IS NOT INSTALLED !!! PASSED !!!!!!!"
     RSLT36a="YES"
    fi
     
 fi

##

echo " "
echo "__________________________________________________________________________________"
echo "chkconfig nfslock off. "
echo "__________________________________________________________________________________"
echo " "
SERVICE="nfslock"
if ps ax | grep -v grep | grep -w $SERVICE >/dev/null
then
    echo "$SERVICE service running,!!!!!---FAILED ---!!!!!!!!!!! Only Encrypted NFS is ALLOWED !!!"
 
    RSLT36b="NO"
else
  JKLM1=`chkconfig --list  |grep -w nfslock | awk '{ print $5 $7 }'`
  if [ -n "$JKLM1" ] ; then
 
   if [  "$JKLM1" = "3:off5:off" ] ; then
   echo " NFSLOCK  is Disabled   PASSED !!!"
   RSLT36b="YES"
   else
   echo " NFSLOCK Is not Disabled !!!!!------------FAILED------------- !!!!!!"
   RSLT36b="NO"
  
    fi
   else
    echo " NFSLOCK IS NOT INSTALLED !!! PASSED !!!!!!!"
     RSLT36b="YES"
    fi
     
 fi
 
 
echo " "
echo "__________________________________________________________________________________"
echo "chkconfig autofs off. "
echo "__________________________________________________________________________________"
echo " "
SERVICE="autofs"
if ps ax | grep -v grep | grep -w  $SERVICE >/dev/null
then
    echo "$SERVICE service running, !!!!!------------FAILED------------- !!!!!!!!  !!!"
    RSLT36c="NO"
else
  JKLM1=`chkconfig --list  |grep -w autofs | awk '{ print $5 $7 }'`
  if [ -n "$JKLM1" ] ; then
 
   if [  "$JKLM1" = "3:off5:off" ] ; then
   echo " AUTOFS  is Disabled   PASSED !!!"
   RSLT36c="YES"
   else
   echo " AUTOFS Is not Disabled !!!!!------------FAILED------------- !!!!!!!!!!"
   RSLT36c="NO"
  
    fi
   else
    echo " AUTOFS IS NOT INSTALLED !!! PASSED !!!!!!!"
     RSLT36c="YES"
    fi
     
 fi

 if [ $RSLT36a = "YES" -a $RSLT36b = "YES" -a $RSLT36c = "YES" ] ; then
   RSLT36="YES"
 else
   RSLT36="NO"
 fi

#######################################

echo " "
echo "__________________________________________________________________________________"
echo "Chekcing permission of NFS Configuration files. "
echo "__________________________________________________________________________________"
echo " "
if [  -f /etc/exports ] ; then
echo " NFS Configuration files  Exists in Server "
CHECKPEREXPORTS=`ls -l /etc/exports |awk '{print $1$3$4}'`
 
if [ "$CHECKPEREXPORTS" = "-rw-------rootroot" -o "$CHECKPEREXPORTS" = "-rw-r--r--rootroot" ] ; then
echo " "
echo " File Permission of NFS is OK PASSED !!!!!!!!!!!!  "
RSLT37="YES"
else
echo " "
echo " File Permission of NFS is not OK !!!!!!!!!!!!!! FAILED !!!!  "
echo " Change the permission of /etc/exports should be 644 or minimal and ownership would be root:root"
echo " "
RSLT37="NO"
fi
else
echo " NFS Configuration files is not Exists on the system !!!  PASSED !!! "
RSLT37="YES"
fi

#######################################

echo " "
echo "__________________________________________________________________________________"
echo "/etc/exports "
echo "__________________________________________________________________________________"
echo " "

if ps ax | grep -v grep | grep -w  $SERVICE >/dev/null
then
 if [ -f /etc/exports ] ; then
  RSLT38="YES"
 else
  RSLT38="NO"
 fi
else
 RSLT38="YES"
fi

#######################################


echo " "
echo "__________________________________________________________________________________"
echo "Exporting NFS confidential data "
echo "__________________________________________________________________________________"
echo " "

if ps ax | grep -v grep | grep -w  $SERVICE >/dev/null
then
  if [ -f /etc/exports ] ; then
   chk=$(awk '{print $2}' /etc/exports)
   chk1=`cut -f 1 -d( chk`
   chk2=`cut -f 2 -d( chk`
   if [ -n $chk1 -a -n $chk2 ] ; then
    RSLT28="YES"
   else
    RSLT39="N0"
   fi
  else
   RSLT39="N0"
  fi
else
 RSLT39="YES"
fi

#######################################

echo " "
echo "__________________________________________________________________________________"
echo " FTP should be disabled. If necessary, a secure system like SFTP or SCP should be used. "
echo "__________________________________________________________________________________"
echo " "
 
SERVICE="vsftpd"
if ps ax | grep -v grep | grep $SERVICE >/dev/null
then
    echo "$SERVICE service running,!!!!!------------FAILED------------- !!!!!!!!!"
    RSLT40="NO"
else
  JKLM1=`chkconfig --list | grep vsftpd | awk '{ print $5 $7 }'`
   if [ -n "$JKLM1" ] ; then
 
   if [ "$JKLM1" = "3:off5:off" ] ; then
   echo " Ftp is Disabled   PASSED !!!"
   RSLT40="YES"
   else
   echo " FTP Is not Disabled !!!!!------------FAILED------------- !!!!!!!!!"
   RSLT40="NO"
   fi
   else
   echo "  vsftpd  IS NOT INSTALLED !!! PASSED !!!!!! "
    RSLT40="YES"
  fi 
 fi

 ######################################

 SERVICE="vsftpd"
if ps ax | grep -v grep | grep $SERVICE >/dev/null
then
    echo "$SERVICE service running, FAILED !!!  !!!"
    CHK40="NO"
else
 
 echo " $SERVICE is Not Running on server PASSED !!!!"
 if [ -f /etc/vsftpd/vsftpd.conf ] ; then
  # JKLM=`grep anonymous_enable /etc/vsftpd/vsftpd.conf | awk -F= '{ print $2 }'`
   JKLM=` grep anonymous_enable /etc/vsftpd/vsftpd.conf  | sed -e 's/ //g' | awk -F= '{print $2}' `
     if [ "$JKLM" == "NO" ] ; then
      echo " Anonymous FTP is Disabled !!!!  PASSED !!!!"
      grep anonymous_enable /etc/vsftpd/vsftpd.conf
      CHK40="YES"
      else
      echo ""
      grep anonymous_enable /etc/vsftpd/vsftpd.conf
 
      echo " Anonoymoys FTP is Not Disabled !!!!!!!!------------FAILED------------- !!!!!!!!!!"
      CHK40="NO"
      fi
    else
    echo " VSFTPD is not installed PASSED !!!!!!!!!!!!!!!!!!"
     CHK40="YES"
    fi
     CHK40="YES"
 fi

 if [ $CHK40 = "YES" ] ; then
   RSLT41="YES"
   RSLT42="YES"
   RSLT43="YES"
   RSLT44="YES"
   RSLT45="YES"
   RSLT46="YES"
   RSLT47="YES"
   RSLT48="YES"
else
   RSLT41="NO"
   RSLT42="NO"
   RSLT43="NO"
   RSLT44="NO"
   RSLT45="NO"
   RSLT46="NO"
   RSLT47="NO"
   RSLT48="NO"
fi

#######################################

echo " "
echo "__________________________________________________________________________________"
echo "TFTP access control"
echo "__________________________________________________________________________________"
echo " "
SERVICE="tftpd"
if ps ax | grep -v grep | grep -w $SERVICE >/dev/null
 then
    echo "TFTP IS INSTALLED !!!"
    arg=`grep "server_args" /etc/xinetd.d/tftp | awk -F= '{print $2}' | awk '{print $1}'`
    if [ $arg = "-s" ] ; then
     RSLT49="YES"
    else
     RSLT49="NO"
    fi
else
    echo "TFTP IS NOT INSTALLED !!! NOT APPLICABLE !!!!!!!"
    RSLT49="YES"
fi


#######################################


echo " "
echo "__________________________________________________________________________________"
echo "Directories enabled for TFTP access"
echo "__________________________________________________________________________________"
echo " "
SERVICE="tftpd"
if ps ax | grep -v grep | grep -w $SERVICE >/dev/null
 then
    arg=`grep "server_args" /etc/xinetd.d/tftp | awk -F= '{print $2}' | awk '{print $2}'`
    if [ -n $arg ] ; then
     echo "$arg Directories enabled for TFTP access !!!"
     RSLT50="YES"
    else
     RSLT50="NO"
    fi
  else
   RSLT50="YES"
fi

#######################################

echo " "
echo "__________________________________________________________________________________"
echo "SNMP should be disabled on production servers.. "
echo "__________________________________________________________________________________"
echo ""
 
 
         SERVICE="snmpd"
if ps ax | grep -v grep | grep $SERVICE >/dev/null
then
    echo "$SERVICE dns  service running, !!!!!------------FAILED------------- !!!!!!!!!!"
    RSLT51="NO"
else
  JKLM1=`chkconfig --list  $SERVICE 2>/dev/null  | awk '{ print $5 $7 }'`
  if [ -n "$JKLM1" ] ; then
 
   if [  "$JKLM1" = "3:off5:off" ] ; then
   echo "  snmp  is Disabled   PASSED !!!"
   RSLT51="YES"
   else
   echo "   snmp Is not Disabled !!!!!------------FAILED------------- !!!!!!!!!!"
   RSLT51="NO"
  
    fi
   else
    echo " snmp   IS NOT INSTALLED !!! PASSED !!!!!!!"
     RSLT51="YES"
    fi
     
 fi

########################################

echo "_________________________________________________________________________________"
echo "   Enable tcp syncookies to prevent syn flooding          "
echo "__________________________________________________________________________________"
echo " "
echo " "
chk=$(cat /proc/sys/net/ipv4/tcp_syncookies)
if [ $chk -eq 1 ] ; then
  RSLT52="YES"
else
  RSLT52="NO"
fi


#######################################

echo "_________________________________________________________________________________"
echo "   Turn off ICMP broadcasts          "
echo "__________________________________________________________________________________"
echo " "
echo " "
chk=$(cat /proc/sys/net/ipv4/icmp_echo_ignore_broadcasts)
if [ $chk -eq 1 ] ; then
  RSLT53="YES"
else
  RSLT53="NO"
fi

#######################################

echo "_________________________________________________________________________________"
echo "   Disable ICMP Redirect Acceptance          "
echo "__________________________________________________________________________________"
echo " "
echo " "
chk=$(cat /proc/sys/net/ipv4/conf/all/accept_redirects)
#chk=$(cat /proc/sys/net/ipv4/icmp_echo_ignore_broadcasts)
if [ $chk -eq 0 ] ; then
  RSLT54="YES"
else
  RSLT54="NO"
fi

#######################################

echo " "
echo "__________________________________________________________________________________"
echo "relevant parameters in /etc/samba/smb.conf "
echo "__________________________________________________________________________________"
echo ""
 
         SERVICE="smbd"
if ps ax | grep -v grep | grep $SERVICE >/dev/null
then
    echo "$SERVICE   service running, FAILED !!!  !!!"
    AA1=`grep -i Security /etc/samba/smb.conf 2>/dev/null  | awk -F= '{print $2}'`
    AA2=`grep -i Workgroup /etc/samba/smb.conf 2>/dev/null  | awk -F= '{print $2}'`
    AA3=`grep -i "guest ok" /etc/samba/smb.conf 2>/dev/null   | awk -F= '{print $2}'`
    AA4=`grep -i "Invalid users" /etc/samba/smb.conf 2>/dev/null  | awk -F= '{print $2}'`
    AA5=`grep -i "Client lanman auth" /etc/samba/smb.conf 2>/dev/null  | awk -F= '{print $2}'`
    AA6=`grep -i "client ntlmv2 auth" /etc/samba/smb.conf 2>/dev/null   | awk -F= '{print $2}'`
    AA7=`grep -i "hosts allow" /etc/samba/smb.conf 2>/dev/null  | awk -F= '{print $2}'`

    if [ $AA1="user" -a $AA2="MYGROUP" -a $AA3="no" -a $AA4="root@wheel" -a $AA5="no" -a $AA6="yes" ] ; then
     RSLT55="YES"
    else
     RSLT55="NO"
    fi
else
    echo "$SERVICE   service not running, PASSED !!!  !!!"
    RSLT55="YES"  
fi

#######################################

echo " "
echo "__________________________________________________________________________________"
echo "Xinetd Disabling xinetd"
echo "__________________________________________________________________________________"
echo ""

cd /etc/xinetd.d
for FILE in chargen daytime discard  echo finger systat who netstat chargen chargen-udp cups-lpd daytime daytime-udp eklogin finger gssftp imap imaps ipop2 ipop3 krb5-telnet klogin kshell ktalk ntalk pop3s rexec rlogin rsh rsync servers services sgi_fam shell talk telnet tftp time time-udp  wu-ftpd echo bootps chargen daytime discard echo finger netstat pcnfsd rstatd ruserd rwalld sprayd tftp
do
if chkconfig --list   $FILE  >   /dev/null 2>&1 
 then
JKLM1=`chkconfig --list   $FILE  | awk '{ print $2 }' | sed -s 's/0://g'`
   if [  "$JKLM1" = "off" ] ; then
   echo "  $FILE is Disabled   PASSED !!!!!!!!!!!!!"
   else
   echo "   $FILE Is not Disabled !!!!!------------FAILED------------- !!!!!!!!!!"
  JK31="YES"
  
    fi
 
else
echo " $FILE is not installed  !!! PASSED!!!!!!!!!!!!!"
fi
done
if [ "$JK31" = "NO" ] ; then
  out56="YES"
 echo " ALL service is not Disabled -----!!!!!------------FAILED-------------!!!!!  "
else
 echo " ALL service is Disabled PASSED !!!!!!!!!!!!!!!!!!!  "
 out56="YES"
fi

  if [ $out56 = "YES" ] ; then
    SERVICE="xinetd"
    JKLM1=`chkconfig --list  |grep -w $SERVICE  | awk '{ print $5 $7 }'`
    if [ -n "$JKLM1" ] ; then
 
         if [  "$JKLM1" = "3:off5:off" ] ; then
           echo " xinetd   is Disabled  !!!!!!!!!!! PASSED !!!!!!!!!!!!!!!!!"
           RSLT56="YES"
         else
           echo "  xinetd Is not Disabled Should be Treated GSD  COMPLIANCE As per the GSD  !!!!"
           RSLT56="YES"
        fi
    else
      echo " xinetd  IS NOT INSTALLED !!! PASSED !!!!!!!"
      RSLT56="YES"
    fi
  else
   echo "  xinetd Is not Disabled Should be Treated GSD  COMPLIANCE As per the GSD  !!!!"
   RSLT56="YES"
  fi

#######################################

echo " "
echo "__________________________________________________________________________________"
echo "Configure xinetd Access Control"
echo "__________________________________________________________________________________"
echo ""
 
if [ -f /etc/xinetd.conf ] ; then
echo " xinted.conf  file Found on the server "
 
 HJK=`grep  -w only_from  /etc/xinetd.conf`
 if [ -n "$HJK" ] ; then
  echo " Xinetd Aceess control is enabled  !! PASSED !!!"
   RSLT57="YES"
  else
  echo "  Xinetd Aceess control is DISABLED  !!!!!!! FAILED  !!!!!!!!!!"
  RSLT57="NO"
  fi
 
else
 
echo " xinetd is not installed !!!!!!!!!!!!!  PASSED "
 RSLT57="YES"
fi

#######################################

echo " "
echo "__________________________________________________________________________________"
echo "Disable standard Services"
echo "__________________________________________________________________________________"
echo ""
 
   test="0"

for FILE in apmd canna FreeWnn gpm hpoj innd irda isdn kdcrotate lvs mars-new oki4daemon privoxy rstatd rusersd rwalld rwhod spamassassin wine nfs nfslock autofs ypbind ypserv yppasswdd portmap smb netfs lpd apache httpd tux snmpd named postgresql mysqld webmin kudzu squid cups ip6tables iptables pcmcia bluetooth, mDNSResponder   firstboot gpm  isdn   irqbalance   kdump  kudzu  mdmonitor   microcodectl   portmap  rhnsd  rpcgssd   rpcidmapd  xfs yum-updatesd pcscd   readahead_early  readahead_later  haldaemon  bluetooth hidd  autofs avahi-daemon   cups  haldaemon  hplip  ip6tables  iptables  mcstrans  nfslock bluetooth

do

 JKLM1=`chkconfig --list  $FILE 2>/dev/null | awk '{ print $5 $7 }'  2>/dev/null`
 if [ -n "$JKLM1" ] ; then

   if [  "$JKLM1" = "3:off5:off" ] ; then
     echo "  $FILE is Disabled  !!!!!!!!!!! PASSED !!!"
   else
     echo " $FILE  Is not Disabled ------------------ FAILED !!!!---------------Please do chkconfig $FILE off "
     test="1"
   fi
 else
   echo " $FILE IS NOT INSTALLED !!! PASSED !!!!!!!-----"

 fi

done

if [ $test = "1" ] ; then
  RSLT58="NO"
  else
  RSLT58="YES"
fi  

   echo " Note -:;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; "
   echo " "
   echo " Please Check The Above Logs for Running Services Should be  disabled Except for Bussiness needs "
   echo " Also Should be  Treated  as Compliace !!! As per The GSD ... "
   echo " "
   echo " "

#######################################

echo "__________________________________________________________________________________"
echo "Lock the daemon-user accounts related to servers"
echo "__________________________________________________________________________________"
echo ""
 
#cat /etc/passwd | awk -F: '$3 < 500 { print $1 "---" $7 }' | egrep -v  'sync|shutdown|root|halt' >KLTEST
cat /etc/passwd | awk -F: '$3 < 500 { print $1 "+++" $7 }' | egrep -v  'root' >KLTEST
 
for i in `cat KLTEST`
 do
  MPTEST=`echo $i  | awk -F+ '{ print $4 }'`
   if [ "$MPTEST" = "/sbin/nologin" -o "$MPTEST" = "/bin/false" ] ; then
    echo " $i  Daemon User Should  Locked  $MPTEST  ------!!!PASSED !!!!!!!!!"
    else
    echo " $i     User Not Locked !!!! ----------------FAILED-----------------!!!!"
    FAILTEST="OK"
    fi
   
   done
   rm -f KLTEST
if [ "$FAILTEST" = "OK" ] ;then
echo " "
echo "OverAll testing of Daemon User test is !!!!!!!!!!--------------------------FAILED-------------------!!!!!!!!!!!!!!!!!!!!!"
RSLT59="NO"
else
echo " "
echo  "Overall testing of Daemon User test is !!!!!!!!!!--------------------------PASSED-------------------!!!!!!!!!!!!!!!!!!!!!"
RSLT59="YES"
fi

#######################################

echo "__________________________________________________________________________________"
echo "Enable TCP SYN Cookie Protection"
echo "__________________________________________________________________________________"
echo ""
VAL2=`cat /proc/sys/net/ipv4/tcp_syncookies`
CHEKVALTCPSYN=`cat /etc/rc.local  | grep tcp_syncookies | grep -v \# `
if [ "$VAL2" = "1"  -a -n "$CHEKVALTCPSYN"  ] ; then
echo " TCP SYN Cookie Protection is Enabled !!!!!!!!!!!!PASSED !!!!!!!!!!!!!!!!!"
RSLT60="YES"
else
echo " TCP SYN Cookie Protection is Disabled !!!!!!!!!!!!------------FAILED------------- !!!!!!!!!!!!!!!!!"
echo " Please add [ echo 1 > /proc/sys/net/ipv4/tcp_syncookies]  in /etc/rc.local and execute it  " 
RSLT60="NO"
 
fi

#######################################

echo "__________________________________________________________________________________"
echo "XDMCP should not be allowed over unencrypted sessions."
echo "__________________________________________________________________________________"
echo ""
MKTEST=`grep X11Forwarding  /etc/ssh/sshd_config  | grep -v \# | awk '{ print $2 }'`
MKTEST5=`netstat -an | grep  -w 177 | awk '{print $5 }' | tail -1 `
if [ "$MKTEST" = "yes" -a -z "$MKTEST5" ] ; then
echo " XDMCP is allowed in encypted session !!!!!!!!!!!!PASSED !!!!!!!!!!"
RSLT61="YES"
else
echo "  XDMCP is not  allowed in encypted session !!!!!!!!------------FAILED------------- !!!!!! "
RSLT61="NO"
fi

#######################################

echo "__________________________________________________________________________________"
echo "Prevent X Server From Listening On Port 6000/tcp."
echo "__________________________________________________________________________________"
echo ""
 
if [ -f /etc/X11/xdm/Xservers  ] ; then
 MKTEST3=`grep :0   /etc/X11/xdm/Xservers  | grep -v \# | awk '{ print $4 }'`
 CHKPERM=`ls -l /etc/X11/xdm/Xservers | awk '{print $1$3$4}'`
 
    if [ "$MKTEST3"  = "-nolisten" -a "$CHKPERM" = "-r--r--r--rootroot"  ]; then
      echo "    /etc/X11/xdm/Xservers  nolisten entry found and Permission is OK "
        else
         echo " /etc/X11/xdm/Xservers  nolisten entry  NOT  found and Permission is FAILED !!!!!!! "
     echo " please [add :0 local /usr/X11R6/bin/X -nolisten tcp] in /etc/X11/gdm/gdm.conf  files "
         echo " "
         echo " Please give permission to 444 and ownership root:root of /etc/X11/xdm/Xservers "
         echo " "
           HJT="no"
         fi
         
 
 
       if [ -f  /etc/X11/gdm/gdm.conf  ] ; then
       MYTEST4=`grep :0  /etc/X11/gdm/gdm.conf  | grep -v \# | awk '{ print $4 }'`
           CHKPERGDM=`ls -l /etc/X11/gdm/gdm.conf | awk '{print $1$3$4}'`
            if [ "$MYTEST4"  = "-nolisten"  -a "$CHKPERGDM" = "-rw-r--r--rootroot" ] ; then
            echo  "  /etc/X11/gdm/gdm.conf   nolisten Entry  as well Permission-----------PASSED ----------------- "
 
              else
             HJT="no"
               echo  " /etc/X11/gdm/gdm.conf   nolisten Entry ------!!!!!!!!!! FAILED !!!!!!!!!----------------- "
                           echo " please [add :0 local /usr/X11R6/bin/X -nolisten tcp] in /etc/X11/gdm/gdm.conf  files "
                           echo " also check the permission  /etc/X11/gdm/gdm.conf to 644 and  ownership root:root "
              fi
   
         else
        echo " /etc/X11/gdm/gdm.conf  file not found !!!!!!!!!!!!!!!!!!  ---------PASSED --------  "
          fi
 
if [ -f  /etc/X11/xinit/xserverrc  ] ; then
MYTEST5=`grep :0  /etc/X11/xinit/xserverrc  | grep -v \# | awk '{ print $4 }'`
CHKPERMXSER=`ls -l /etc/X11/xinit/xserverrc`
 
            if [ "$MYTEST5"  = "-nolisten"  -a "CHKPERMXSER" = "-rwxr-xr-xrootroot " ] ; then
            echo  " /etc/X11/xinit/xserverrc   nolisten Entry -----------PASSED ----------------- "
              else
             HJT="no"
               echo  " /etc/X11/xinit/xserverrc   nolisten Entry ------!!!!!!!!!! FAILED !!!!!!!!!---------- "
                             echo " please [add :0 local /usr/X11R6/bin/X -nolisten tcp] in  /etc/X11/xinit/xserverrc  files "
                                 echo " also check the permission 755 and ownership to  root:root "
              fi
            
else
echo " /etc/X11/xinit/xserverrc  file not Found !!!!!!! -------  PASSED _----------------- "
fi
 
 
 if [ "$MKTEST3" = "-nolisten" -a "$HJT" != "no" ] ; then
  echo  "  /etc/X11/xdm/Xservers    nolisten Entry -----------PASSED ----------------- "
  echo " Prevent X Server From Listening On Port 6000/tcp  -- !!!!!!PASSED !!!!!!!!!!!!!!!!!"
 
  KAM=`cat /etc/services | grep -w 6000 | grep -v \#`
  KAM2=` netstat -an | grep -w 6000 | awk '{print $6}' `
   if [ -n "$KAM" -a -n "$KAM2" ] ; then
    echo " x11 6000/tcp X # the X Window System  not commented !!!!!!!!!!  FAILED !!!!!!!!!" 
        echo " X server should not listent on 6000 port .... Please check "
    echo " $KAM2  6000 Port is Listenning for  X ............ "
 
     RSLT62="NO"
   else
   echo "    x11 6000/tcp X # the X Window System  commented !!!!!!!!!PASSED !!!!!!!!! "
   echo "  6000 Port is not Listen in System .............   PASSED !!!!!!!!!!!!!!!"
   RSLT62="YES"
   fi
 
  else
  echo " "
  echo " echo /etc/X11/xdm/Xservers    nolisten Entry -----------FAILED ----------------- "
  echo " please [add :0 local /usr/X11R6/bin/X -nolisten tcp] in  /etc/X11/xdm/Xservers  files "
  echo " also check the permission to 444 and ownership to root:root "
  echo " "
  echo " Over ALL Prevent X Server From Listening On Port 6000/tcp -!!!------------FAILED------------- !!"
  RSLT62="NO"
 
  fi
 else
 echo " No File Found ....  /etc/X11/xdm/Xservers  Xserver is not installed  -------PASSED _------------ "
RSLT62="YES"
fi

#######################################

echo "__________________________________________________________________________________"
echo "Disable GUI login"
echo "__________________________________________________________________________________"
echo ""
LMTEST=`grep initdefault /etc/inittab | grep -v \# | awk -F: '{ print $2 }' `
if [ "$LMTEST" = "3" ] ; then 
echo " Server is Running in RUNLEVEL 3   -------PASSED ------------------- "
RSLT63="YES"
else
echo " Server is Running in RUNLEVEL $LMTEST !!------------FAILED------------- !!! "
RSLT63="NO"
fi

#######################################

echo "__________________________________________________________________________________"
echo "Disable X Font Server if possible.."
echo "__________________________________________________________________________________"
echo ""
 SERVICE="xfs"
  JKLM1=`chkconfig --list  |grep -w $SERVICE  | awk '{ print $5 $7 }'`
  if [ -n "$JKLM1" ] ; then
 
   if [  "$JKLM1" = "3:off5:off" ] ; then
   echo " $SERVICE  is Disabled  !!!!! PASSED !!!"
   RSLT64="YES"
   else
   echo "  $SERVICE Is not Disabled Should be Treated GSD !!!!!! COMPLIANCE !!!!!!As per the GSD  !!!!"
   echo "  Justify The Bussiness Requirements  " 
   RSLT64="YES"
  
    fi
   else
    echo " $SERVICE  IS NOT INSTALLED  !!! PASSED !!!!!!!"
     RSLT64="YES"
    fi

#######################################

echo "__________________________________________________________________________________"
echo "Screensaver-password based Locking of inactive GUI sessions.."
echo "__________________________________________________________________________________"
echo ""
 
LMTEST=`grep initdefault /etc/inittab | grep -v \# | awk -F: '{ print $2 }' `
if [ "$LMTEST" = "3" ] ; then 
echo " Server is Running in RUNLEVEL 3   -------PASSED ------------------- "
echo " NO Need To set the Password for GUI SESSION  !!!!!! "
RSLT65="NA"
else
echo " Server is Running in RUNLEVEL $LMTEST !!------------FAILED------------- !!! "
RSLT65="NA"
fi

#######################################

echo "__________________________________________________________________________________"
echo "X-server access control."
echo "__________________________________________________________________________________"
echo ""
OSLEVEL=`cat /etc/redhat-release  | awk '{ print $7}'`
if [ "$OSLEVEL" = "3" ] ; then 
 
if [ -f /usr/bin/X11/xhost ] ; then 
echo " File Found .......   /usr/bin/X11/xhost  " 
RKTEST=`ls -ltr  /usr/bin/X11/xhost | awk '{ print $1 }'`
echo " checking the xhost command permission .."
if [ "$RKTEST" = "-rwxr--r--" -o "$RKTEST" = "-rwx------" ] ; then
 
echo " $RKTEST  - xhost  X-server access control  !!!!!!!!!!!PASSED ----------------!!!!!!!!!!! "
RSLT66="YES"
else
echo " $RKTEST  - xhost -- X-server access control  !!------------FAILED------------- !!!!!  "
echo " Please change the permussion /usr/X11R6/bin/xhost  to 744 or less make it 700 "
RSLT66="NO"
fi
else
echo " File Not Found ..... /usr/bin/X11/xhost  _------------PASSED ----------------"
RSLT66="YES"
 
fi
 
else  ##oslevel
 
if [ -f /usr/X11R6/bin/xhost  ] ; then 
echo " File Found .......   /usr/X11R6/bin/xhost   " 
RKTEST=`ls -ltr  /usr/X11R6/bin/xhost  | awk '{ print $1 }'`
echo " checking the xhost command permission .."
if [ "$RKTEST" = "-rwxr--r--" -o "$RKTEST" = "-rwx------" ] ; then
 
echo " $RKTEST  - xhost  X-server access control  !!!!!!!!!!!PASSED ----------------!!!!!!!!!!! "
RSLT66="YES"
else
echo " $RKTEST  - xhost -- X-server access control  !!------------FAILED------------- !!!!!  "
echo " Please change the permussion /usr/X11R6/bin/xhost  to 700 "
RSLT66="NO"
fi
else
echo " File Not Found .....  /usr/X11R6/bin/xhost  -----------PASSED ----------------"
RSLT66="YES"
 
fi
 
fi  ##oslevel

#######################################

echo "__________________________________________________________________________________"
echo "Preventing Unauthorized Monitoring of Remote X Server"
echo "__________________________________________________________________________________"
echo ""
echo " checking the  xwd and xwud command permission " 
 
OSLEVEL=`cat /etc/redhat-release  | awk '{ print $7}'`
 
 if [ "$OSLEVEL" = "3" ] ; then 
 
 
 
if [ -f  /usr/bin/X11/xwud  -a /usr/bin/X11/xwd ] ; then 
echo " /usr/bin/X11/xwud  File Found ............."
echo " "
RKTEST=`ls -ltr  /usr/bin/X11/xwud | awk '{ print $1 }'`
if [ "$RKTEST" = "-rwx------" ] ; then
echo " $RKTEST  - xwud  X-server access control  !!!!!!!!!!!PASSED ----------------!!!!!!!!!!! "
else
echo " $RKTEST  - xwud -- X-server access control  !!------------FAILED------------- !!!!!  "
echo " Please change the permission /usr/X11R6/bin/xwd , /usr/X11R6/bin/xwud   to 700 "
RSLT67="NO"
fi
 
echo " checking the  xwd and xwud command permission " 
RKTEST=`ls -ltr  /usr/bin/X11/xwd | awk '{ print $1 }'`
if [ "$RKTEST" = "-rwx------" ] ; then
echo " $RKTEST  - xwd  X-server access control  !!!!!!!!!!!PASSED ----------------!!!!!!!!!!! "
RSLT67="YES"
else
echo " $RKTEST  - xwd -- X-server access control  !!------------FAILED------------- !!!!!  "
echo " Please change the permission /usr/bin/X11/xwd    to 700 "
RSLT67="NO"
fi
 
else
echo " /usr/bin/X11/xwud  /usr/bin/X11/xwd  File Not Found .... PASSED !!!!!!!!!!!!!!!!"
RSLT67="YES"
 
fi
 
 
else  ###OSLEVEL
 
 
if [ -f   /usr/X11R6/bin/xwud   -a /usr/X11R6/bin/xwd ] ; then 
echo "  /usr/X11R6/bin/xwud  /usr/X11R6/bin/xwd   File Found .............RHEL4 ......"
echo " "
RKTEST=`ls -ltr  /usr/X11R6/bin/xwud  | awk '{ print $1 }'`
if [ "$RKTEST" = "-rwx------" ] ; then
echo " $RKTEST  - xwud  X-server access control  !!!!!!!!!!!PASSED ----------------!!!!!!!!!!! "
else
echo " $RKTEST  - xwud -- X-server access control  !!------------FAILED------------- !!!!!  "
echo " Please change the permission /usr/X11R6/bin/xwd , /usr/X11R6/bin/xwud   to 700 "
RSLT67="NO"
fi
 
echo " checking the  xwd and xwud command permission " 
RKTEST=`ls -ltr  /usr/X11R6/bin/xwd  | awk '{ print $1 }'`
if [ "$RKTEST" = "-rwx------" ] ; then
echo " $RKTEST  - xwd  X-server access control  !!!!!!!!!!!PASSED ----------------!!!!!!!!!!! "
RSLT67="YES"
else
echo " $RKTEST  - xwd -- X-server access control  !!------------FAILED------------- !!!!!  "
echo " Please change the permission /usr/X11R6/bin/xwd    to 700 "
RSLT67="NO"
fi
 
else
echo " /usr/X11R6/bin/xwud   /usr/X11R6/bin/xwd  File Not Found .... PASSED !!!!!!!!!!!!!!!!"
RSLT67="YES"
 
fi
 
fi   ###OSLEVEL

#######################################

echo "__________________________________________________________________________________"
echo "/.rhosts and /.netrc Should not be allowed to exist on "
echo "__________________________________________________________________________________"
echo ""
 
 for home in `cat /etc/passwd | awk -F: '$3>=500 && $3 <=6000 {print $6}'`
 do
   find $home -maxdepth 1  \( -name ".rhosts" -o -name ".netrc"  \) >> netrc-list
#   find /root   \( -name ".rhosts" -o -name ".netrc" \) >> netrc-list
 done
 
 
   VAL4=`cat netrc-list | wc -l `
 
    if [ "$VAL4" -eq "0" ] ; then
    echo " /.rhosts and /.netrc  not exists on server  !!!!!!!!!!!!!PASSED !!!!!!!!!!! "
    RSLT68="YES"
    else
    echo "  /.rhosts and /.netrc  exists on server !!!------------FAILED------------- !!!!!!!! "
    echo " File Found ..........  Please check !!!!!!!!!!!! "
    echo " "
    cat  netrc-list
    rm -f  netrc-list
    RSLT68="NO"
    fi
rm -f netrc-list
 
####################################### 
 
echo "__________________________________________________________________________________"
echo "/etc/hosts.equiv Must not be used "
echo "__________________________________________________________________________________"
echo ""
if [ -f  /etc/hosts.equiv ] ; then 
RSLT69="NO"
echo " /etc/hosts.equiv Exists on server   !!!------------FAILED------------- !!!!! "
cat /etc/hosts.equiv 
else
 echo "  /etc/hosts.equiv not  Exists on server  !!!!!!!!!!!!!PASSED !!!!!!!!!!  "
RSLT69="YES"
fi

#######################################

echo "__________________________________________________________________________________"
echo "/etc/pam.d/rlogin    /etc/pam.d/rsh  "
echo "__________________________________________________________________________________"
echo ""

if [ -f  /etc/pam.d/rlogin ] ; then 
 
echo " /etc/pam.d/rlogin  Exists on server   ..........."
echo " checking the entry (auth     sufficient   pam_rhosts_auth.so no_hosts_equiv )  in rlogin file "
MKTEST8=`grep "pam_rhosts_auth.so"  /etc/pam.d/rlogin | awk '{ print $4 }' `
 
if [ -n "$MKTEST8" -a "$MKTEST8" = "no_hosts_equiv" ] ; then
 
echo "auth  sufficient  pam_rhosts_auth.so no_hosts_equiv  --Line found  !!!!!!!!!!!!!PASSED !!!!!!!!!! "
RSLT70a="YES"
 
else
echo " auth  sufficient  pam_rhosts_auth.so no_hosts_equiv -Line not Found  !------------FAILED------------- !!"
RSLT70a="NO"
fi
 
else
 echo "  /etc/pam.d/rlogin  not  Exists on server  !!!!!!!!!!!!PASSED !!!!!!!!!  "
RSLT70a="YES"
 
fi
 
 
echo "__________________________________________________________________________________"
echo "/etc/pam.d/rsh  file exists and there is a /lib/security/pam_rhosts_auth.so  "
echo "__________________________________________________________________________________"
echo ""
 
if [ -f  /etc/pam.d/rsh ] ; then 
 
echo " /etc/pam.d/rsh    Exists on server   ..........."
echo " checking the entry (auth     sufficient   pam_rhosts_auth.so no_hosts_equiv )  in rsh file "
MKTEST8=`grep "pam_rhosts_auth.so"  /etc/pam.d/rsh | awk '{ print $4 }' `
if [ -n "$MKTEST8" -a "$MKTEST8" = "no_hosts_equiv" ] ; then
echo "auth  sufficient  pam_rhosts_auth.so no_hosts_equiv  --Line found  !!!!!!!!!!!!!PASSED !!!!!!!!!! "
RSLT70b="YES"
 
else
echo " auth  sufficient  pam_rhosts_auth.so no_hosts_equiv -Line not Found  !------------FAILED------------- !!"
RSLT70b="NO"
fi
 
else
 echo "  /etc/pam.d/rsh  not  Exists on server  !!!!!!!!!!!!PASSED !!!!!!!!!  "
RSLT70b="YES"
 
fi


if [ $RSLT70a = "YES" -a $RSLT70b = "YES" ] ; then
 RSLT70="YES"
else
 RSLT70="NO"
fi

#######################################

echo "__________________________________________________________________________________"
echo "Network Information Services (NIS) should be disabled completely "
echo "__________________________________________________________________________________"
echo ""

 
         SERVICE="yppasswd"
if ps ax | grep -v grep | grep $SERVICE >/dev/null
then
    echo "$SERVICE   service running, !!!!!------------FAILED------------- !!!!!!!! !!!"
    RSLT71a="NO"
else
  JKLM1=`chkconfig --list  |grep -w $SERVICE  | awk '{ print $5 $7 }'`
  if [ -n "$JKLM1" ] ; then
 
   if [  "$JKLM1" = "3:off5:off" ] ; then
   echo "  $SERVICE  is Disabled   $JKLM1 -------!!!!!!!! PASSED !!!!!!!!!!"
   RSLT71a="YES"
   else
   echo "   $SERVICE Is not Disabled!!!!!  $JKLM1 ------------FAILED------------- !!!!!!!!!"
   RSLT71a="NO"
  
    fi
   else
    echo " $SERVICE   IS NOT INSTALLED !!! PASSED !!!!!!!"
     RSLT71a="YES"
    fi
     
 fi
 
 
#echo "__________________________________________________________________________________"
#echo "ypserv  daemon  should be disabled "
#echo "__________________________________________________________________________________"
#echo ""
 
         SERVICE="ypserv"
if ps ax | grep -v grep | grep $SERVICE >/dev/null
then
    echo "$SERVICE   service running, !!!!!------------FAILED------------- !!!!!!!! !!!"
    RSLT71b="NO"
else
  JKLM1=`chkconfig --list  |grep -w $SERVICE  | awk '{ print $5 $7 }'`
  if [ -n "$JKLM1" ] ; then
 
   if [  "$JKLM1" = "3:off5:off" ] ; then
   echo "  $SERVICE  is Disabled  $JKLM1 -------!!!!!!!! PASSED !!!"
   RSLT71b="YES"
   else
   echo "   $SERVICE Is not Disabled!!!!!  $JKLM1 ------------FAILED------------- !!!!!!!!!"
   RSLT71b="NO"
  
    fi
   else
    echo " $SERVICE   IS NOT INSTALLED !!! PASSED !!!!!!!"
     RSLT71b="YES"
    fi
     
 fi


if [ $RSLT71a = "YES" -a $RSLT71b = "YES" ] ; then
  RSLT71="YES"
else
  RSLT71="NO"
fi

#######################################

echo "__________________________________________________________________________________"
echo "Yppasswd daemon  should be disabled "
echo "__________________________________________________________________________________"
echo ""
 
         SERVICE="yppasswd"
if ps ax | grep -v grep | grep $SERVICE >/dev/null
then
    echo "$SERVICE   service running, !!!!!------------FAILED------------- !!!!!!!! !!!"
    RSLT72="NO"
else
  JKLM1=`chkconfig --list  |grep -w $SERVICE  | awk '{ print $5 $7 }'`
  if [ -n "$JKLM1" ] ; then
 
   if [  "$JKLM1" = "3:off5:off" ] ; then
   echo "  $SERVICE  is Disabled   $JKLM1 -------!!!!!!!! PASSED !!!!!!!!!!"
   RSLT72="YES"
   else
   echo "   $SERVICE Is not Disabled!!!!!  $JKLM1 ------------FAILED------------- !!!!!!!!!"
   RSLT72="NO"
  
    fi
   else
    echo " $SERVICE   IS NOT INSTALLED !!! PASSED !!!!!!!"
     RSLT72="YES"
    fi
     
 fi

#######################################

echo "__________________________________________________________________________________"
echo "NIS Maps "
echo "__________________________________________________________________________________"
echo ""
if [ $RSLT71 = "YES" ] ; then
  RSLT73="YES"
else
  for line in `grep ^+ /etc/passwd`
  do
   if [ `awk -F: '{print $2}'` !=  "x" ] ; then
    RSLT73="YES"
   else
    RSLT73="NO"
   fi
  done
 RSLT73="NO"
fi

#######################################

echo "__________________________________________________________________________________"
echo "NIS+ Maps "
echo "__________________________________________________________________________________"
echo ""

if [ $RSLT71 = "YES" ] ; then
  RSLT74="YES"
else
  for line in `grep ^+ /etc/passwd`
  do
   if [ `awk -F: '{print $2}'` !=  "x" ] ; then
    RSLT74="YES"
   else
    RSLT74="NO"
   fi
  done
 RSLT74="NO"
fi

echo "#####################################################################################################################"

echo "__________________________________________________________________________________"
echo "Enable cryptographic support which is an integral part of the operating systems, subsystems and applications  "
echo "__________________________________________________________________________________"
echo "This is a Recomendation Only. Encryption Settings are as per Server Usage and Application Requirement"
echo "Checking status of SSHD which is encrypted Service for OS"
echo "#####################################################################################################################"

echo "__________________________________________________________________________________"
echo "Secret-key (also known as symmetric or shared key) cryptographic keys "
echo "__________________________________________________________________________________"
echo "This is a Recomendation Only. Encryption Settings are as per Server Usage and Application Requirement" 
echo "Checking status of SSHD which is encrypted Service for OS"
echo "#####################################################################################################################"

echo "__________________________________________________________________________________"
echo "Public/Private key pairs (also known as asymmetric or dual keys) cryptographic keys"
echo "__________________________________________________________________________________"
echo "This is a Recomendation Only. Encryption Settings are as per Server Usage and Application Requirement"
echo "Checking status of SSHD which is encrypted Service for OS"
echo "#####################################################################################################################"

echo "__________________________________________________________________________________"
echo "Cryptographic Keys for transmitting Confidential Data over Public Networks"
echo "__________________________________________________________________________________"
echo "This is a Recomendation Only. Encryption Settings are as per Server Usage and Application Requirement"
echo "Checking status of SSHD which is encrypted Service for OS"
echo "#####################################################################################################################"

 
 SERVICE="sshd"
if ps ax | grep -v grep | grep $SERVICE >/dev/null
then
    echo "$SERVICE   service running, !!!! PASSED !!!!!!! !!!"
   
 
   JKLM1=`chkconfig --list  |grep -w $SERVICE  | awk '{ print $5 $7 }'`
  if [ -n "$JKLM1" ] ; then
 
   if [  "$JKLM1" = "3:off5:off" ] ; then
   echo "  $SERVICE  is Disabled  $JKLM1 -- ------------FAILED------------- !!!!"
 
    RSLT75="NO"
      RSLT76="NO"
       RSLT77="NO"
        RSLT78="NO"
         RSLT79="NO"
   else
   echo "   $SERVICE Is not Disabled!!!!!  $JKLM1 -!!!! PASSED !!!!!!! !!!!!!!!"
    RSLT75="YES"
      RSLT76="YES"
       RSLT77="YES"
        RSLT78="YES"
         RSLT79="YES"
  
    fi
   else
    echo " $SERVICE   IS NOT INSTALLED !!------------FAILED------------- !!!!"
     RSLT75="NO"
      RSLT76="NO"
       RSLT77="NO"
        RSLT78="NO"
         RSLT79="NO"
    fi
 
 
 
 
   else
   echo "$SERVICE is not Running  !!!!!!!!!!---------FAILED------------- !!!!!!!!!!  " 
   RSLT75="NO"
      RSLT76="NO"
       RSLT77="NO"
        RSLT78="NO"
         RSLT79="NO"
 fi

#######################################

echo "___________________________________________________________________________________________"
echo "Login success  or failure  *.info;mail.none;news.none;authpriv.none;cron.none /var/log/messages "
echo "___________________________________________________________________________________________"
echo ""
MKY1=`grep "\*\.info;mail.none;news.none;authpriv.none;cron.none"  /etc/syslog.conf  | grep -v \# | awk '{ print $2 }'`
MKY2=`grep "authpriv\.\*" /etc/syslog.conf  | grep -v \# | awk '{ print $2 }'`
if [ "$MKY1" = "/var/log/messages" -a "$MKY2" = "/var/log/secure" ] ; then
echo " Login success  or failure  *.info;mail.none;news.none;authpriv.none;cron.none /var/log/messages  setting Present in /etc/syslog.conf !!!!!!!PASSED!!!!!!!!!!"
echo " Line Found ... *.info;mail.none;news.none;authpriv.none;cron.none /var/log/messages "
echo " Line Fount ... authpriv.* /var/log/secure "
RSLT80="YES"
else
echo " No Entry Found or Entry Disable Please Check !! !!!!---------FAILED------------- !!!!!!!"
echo " Line not Found ..*.info;mail.none;news.none;authpriv.none;cron.none /var/log/messages  Please add"
echo " Line not Found .. authpriv.* /var/log/secure "
echo " $MKY1  --- CHECK for MESSAGES   "
echo " $MKY2  --- CHECK for authpriv.* "
RSLT80="NO"
fi

#######################################

echo "__________________________________________________________________________________"
echo " Messages to be captured.   for RHEL5    /etc/syslog.conf  Please Add Below Entry !!! "
echo "__________________________________________________________________________________"
echo " Check For > *.info;mail.none;news.none;authpriv.none;cron.none;auth.*       /var/log/messages "
echo " Check For > authpriv.*                                                     /var/log/secure  "
echo " Check For > kern.*                                                         /var/log/kernel "
echo " Check For > daemon.*                                                      /var/log/daemon.log "
echo " Check For > syslog.*                                                      /var/log/syslog    "
echo " Check For > lpr,news,uucp,local0,local1,local2,local3,local4,local5,local6.*      /var/log/unused.log  "
echo "__________________________________________________________________________________"
 
GY1=` grep -w "auth\.\*" /etc/syslog.conf | grep "cron.none" |awk '{print $2}'`  ###/var/log/meesages
GY2=`grep  "authpriv\.\*"  /etc/syslog.conf | grep -v \#  | awk '{print $2}' `  ## /var/log/secure
GY3=`grep  "kern\.\*" /etc/syslog.conf | grep -v \# | awk '{print $2}' `     ###  /var/log/kernel
GY4=`grep "daemon\.\*"   /etc/syslog.conf  |grep -v \#|  awk '{print $2}'`   ###  /var/log/daemon 
GY5=`grep "syslog\.\*"  /etc/syslog.conf  |grep -v \# |  awk '{print $2}'  `  ###/var/log/syslog 
GY6=`grep local0 /etc/syslog.conf  |grep -v \# |  awk '{print $2}'  `   ###  /var/log/unused.log 
 
if [ -n "$GY1" ] ; then
 echo " syslog setting Found for /var/log/messages   " 
 if [ -f /var/log/messages ] ; then
 echo " File Found .... OK ----------  !!PASSED !!! " 
 else
 echo " File Not Found ..-----  FAILED --......."
 NKTEST="no"
 
 fi
else
echo " No setting Found .. for /var/log/messages .. ---------  FAILED --- "
NKTEST="no"
fi
 
if [ -n "$GY2" ] ; then
 echo " syslog setting Found for /var/log/secure    " 
 if [ -f /var/log/secure  ] ; then
 echo " File Found .... OK ----------  !!PASSED !!! " 
 else
 echo " File Not Found ..-----  FAILED --......."
 NKTEST="no"
 
 fi
else
echo " No setting Found .. for /var/log/secure  .. ---------  FAILED --- "
NKTEST="no"
fi
 
if [ -n "$GY3" ] ; then
 echo " syslog setting Found for /var/log/kernel   " 
 if [ -f /var/log/kernel -o -f /var/log/kernel ] ; then
 echo " File Found .... OK ----------  !!PASSED !!! " 
 else
 echo " File Not Found ....-----  FAILED --....."
 NKTEST="no"
 
 fi
else
echo " No setting Found .. for /var/log/kernel.. ---------  FAILED --- "
NKTEST="no"
fi
 
if [ -n "$GY4" ] ; then
 echo " syslog setting Found for /var/log/loginlog     " 
 if [ -f /var/log/loginlog  ] ; then
 echo " File Found .... OK ----------  !!PASSED !!! " 
 else
 echo " File Not Found ....-----  FAILED --....."
 NKTEST="no"
 
 fi
else
echo " No setting Found .. for /var/log/loginlog .. ---------  FAILED --- "
NKTEST="no"
fi
 
if [ -n "$GY5" ] ; then
 echo " syslog setting Found for /var/log/syslog    " 
 if [ -f /var/log/syslog ] ; then
 echo " File Found .... OK ----------  !!PASSED !!! " 
 else
 echo " File Not Found ....-----  FAILED --....."
 NKTEST="no"
 
 fi
else
echo " No setting Found .. for /var/log/syslog .. ---------  FAILED --- "
NKTEST="no"
fi
 
if [ -n "$GY6" ] ; then
 echo " syslog setting Found for /var/log/unused.log  " 
 if [ -f /var/log/unused.log ] ; then
 echo " File Found .... OK ----------  !!PASSED !!! " 
 else
 echo " File Not Found ........-----  FAILED --."
 NKTEST="no"
 
 fi
else
echo " No setting Found .. for /var/log/unused.log .. ---------  FAILED --- "
NKTEST="no"
fi
 
 
if [ "$NKTEST" = "no" ] ; then
echo " OverAll Testing is !!!!----------!!!!!!!FAILED !!!!---------!!!!!!!!   " 
RSLT81="NO"
else
echo " Overall TESTING is !!PASSED !!! !!!!!!!!!!!!!!!!!!! "
RSLT81="YES"
fi

#######################################

echo "__________________________________________________________________________________"
echo "  Confirm Permissions On System Log Files   "
echo "__________________________________________________________________________________"
echo ""
 
 
 
for i in `cat /etc/syslog.conf | grep -v \# |  grep log | sed -e 's/-//g' | awk '{print $2}'` 
do
if [ -f $i ] ; then
PERMCH=`ls -l $i | awk '{print $1$3$4}'`
 
  if [ "$PERMCH" = "-rw-------rootroot" ] ; then
  echo " $i ==> Permission & Ownership is OK PASSED !!! "
  else
  echo " $i ==> Permission & Ownership is FAILED !!! shoule be root:root and 600 "
  OMTEST="NO"
  fi
  
else
echo " $i file not Found !!!"
fi
 
done
 
 
 
 
for  FIL in ` ls  /var/log/boot.log* /var/log/cron* /var/log/dmesg*  /var/log/ksyms*  2>/dev/null`
do
if [ -f $FIL ]; then
KM1=`ls -l  $FIL | awk '{ print $1  }' `
KM2=`ls -l $FIL | awk '{ print $3 $4 }'`
 
if [ "$KM1" = "-rw-------" -a "$KM2" = "rootroot" ] ; then
echo "$FIL --$KM1 --$KM2    Permission is OK  !!!---------------PASSED !!!!!!!!!!!!!!!"
else
echo " "
echo  "$FIL --$KM1 --$KM2     !!!!!!!!-----------FAILED----------- !!!!!!!!!!!!!"
echo " it should be 600 and root:root $FIL "
OMTEST="NO"
fi
else
echo " Directory found == $i "
fi
done
 
 
for  FIL in ` ls   /var/log/httpd/*   /var/log/maillog*  /var/log/messages*  /var/log/news/*  /var/log/pgsql  /var/log/rpmpkgs*  /var/log/samba/*   /var/log/sa/*  /var/log/scrollkeeper.log secure* spooler*  /var/log/squid/*  /var/log/vbox/* 2>/dev/null `
do
if [ -f $FIL ] ; then
KM1=`ls -l  $FIL | awk '{ print $1  }' `
KM2=`ls -l $FIL | awk '{ print $3 $4 }'`
 
if [ "$KM1" = "-rw-------" -a "$KM2" = "rootroot" -o  "$KM1" = "-rw-r-----"  -o "$KM1" = "-rw-------" ] ; then
echo "$FIL ==> $KM1 ==> $KM2    Permission is OK  !!!---------------PASSED !!!!!!!!!!!!!!!"
else
echo " "
echo  "$FIL ==> $KM1 ==> $KM2     !!!!!!!!-----------FAILED----------- !!!!!!!!!!!!!"
OMTEST="NO"
fi
 
else
echo " Directory Found ......$FIL "
 
fi
done
 
for  FIL in ` ls  /var/log/gdm/* /var/log/httpd/* /var/log/news/* /var/log/samba/*  2>/dev/null  `
do
 
if [ -f $FIL ] ; then 
 
KM1=`ls -l  $FIL | awk '{ print $1  }' `
KM2=`ls -l $FIL | awk '{ print $3 $4 }'`
 
if [ "$KM1" = "-rw-------" -a "$KM2" = "rootroot" -o "$KM1" = "-rw-r-----" ] ; then
echo "$FIL==>$KM1 ==> $KM2    Permission is OK  !!!---------------PASSED !!!!!!!!!!!!!!!"
else
echo " "
echo  "$FIL ==> $KM1 ==> $KM2     !!!!!!!!-----------FAILED----------- !!!!!!!!!!!!!"
OMTEST="NO"
fi
else
echo  " Directory Found ..... $FIL "
fi 
 
done
 
for  FIL in `  ls    /var/log/kernel  /var/log/syslog  /var/log/loginlog  2>/dev/null `
do
KM1=`ls -l  $FIL | awk '{ print $1  }' `
KM2=`ls -l $FIL | awk '{ print $3 $4 }'`
 
if [ "$KM1" = "-rw-------" -a "$KM2" = "rootroot" ] ; then
echo "$FIL ==> $KM1 ==> $KM2    Permission is OK  !!!---------------PASSED !!!!!!!!!!!!!!!"
else
echo " "
echo  "$FIL ==> $KM1 ==>$KM2     !!!!!!!!-----------FAILED----------- !!!!!!!!!!!!!"
OMTEST="NO"
fi
done
 
 
for  FIL in ` ls   /var/log/wtmp  2>/dev/null `
do
KM1=`ls -l  $FIL | awk '{ print $1  }' `
KM2=`ls -l $FIL | awk '{ print $3 $4 }'`
 
if [ "$KM1" = "-rw-rw-r--"  -o "$KM1" = "-rw-r----" -a "$KM2" = "rootutmp" ] ; then
echo "$FIL ==> $KM1 ==> $KM2    Permission is OK  !!!---------------PASSED !!!!!!!!!!!!!!!"
else 
echo " "
echo  "$FIL ==> $KM1 ==> $KM2     !!!!!!!!-----------FAILED----------- !!!!!!!!!!!!!"
echo " "
OMTEST="NO"
fi
done
 
for  FIL in ` ls   /var/log/squid/* /var/log/pgsql/*  2>/dev/null `
do
KM1=`ls -l  $FIL | awk '{ print $1  }' `
KM2=`ls -l $FIL | awk '{ print $3 $4 }'`
 
if [ "$KM1" = "-rw-r--r--"  -o "$KM1" = "-rw-r----" -a "$KM2" = "squidsquid" -o  "$KM2" = "postgrespostgres" ] ; then
echo "$FIL ==> $KM1 ==> $KM2    Permission is OK  !!!---------------PASSED !!!!!!!!!!!!!!!"
else 
echo " "
echo  "$FIL ==> $KM1 ==> $KM2     !!!!!!!!-----------FAILED----------- !!!!!!!!!!!!!"
echo " "
OMTEST="NO"
fi
done
 
 
 
if [ "$OMTEST" = "NO" ] ; then
echo ""
echo " Over ALL Test is  !!!!!!-----------FAILED----------- !!!!!!!!!  "
echo " it should be 600 and ownerhip would be root.root "
 RSLT82="NO"
else
echo ""
echo " Over ALL Test is  !!!!!!--------PASSED ---------- !!!!!!!!!  "
 RSLT82="YES"
fi

#######################################

echo "__________________________________________________________________________________"
echo "/var/log/wtmp  must exist "
echo "__________________________________________________________________________________"
echo ""
if [ -f /var/log/wtmp ] ; then
   echo " File Found  OK !! !!!!!!!!!!!!!PASSED !!!!!!!!!!!!! "
    RSLT83="YES"
   else
   echo " File Not Found   !!!!---------FAILED------------- !!!!! "
   RSLT83="NO"
 fi
 
#######################################

echo "__________________________________________________________________________________"
echo "/var/log/messages  must exist "
echo "__________________________________________________________________________________"
echo ""
if [ -f /var/log/messages   ] ; then
   echo " /var/log/messages File Found  OK !! !!!!!!!!!!!!!PASSED !!!!!!!!!!!!! "
    RSLT84="YES"
   else
   echo " /var/log/messages  File Not Found   !!!!---------FAILED------------- !!!!! "
   RSLT84="NO"
 fi
 
#######################################

echo "__________________________________________________________________________________"
echo "/var/log/faillog  must exists  "
echo "__________________________________________________________________________________"
echo ""
if [ -f /var/log/faillog ] ; then
   echo " /var/log/faillog  File Found  OK !! !!!!!!!!!!!!!PASSED !!!!!!!!!!!!! "
    RSLT85="YES"
   else
   echo " /var/log/faillog File Not Found   !!!!---------FAILED------------- !!!!! "
   RSLT85="NO"
 fi

#######################################

echo "__________________________________________________________________________________"
echo "/var/log/secure  must exists "
echo "__________________________________________________________________________________"
echo ""
if [ -f  /var/log/secure ] ; then
   echo "  /var/log/secure File Found  OK !! !!!!!!!!!!!!!PASSED !!!!!!!!!!!!! "
    RSLT86a="YES"
   else
   echo " /var/log/secure  File Not Found   !!!!---------FAILED------------- !!!!! "
   RSLT86a="NO"
 fi
 
echo "__________________________________________________________________________________"
echo "/var/log/auth.log  /var/log/secure any one should be present "
echo "__________________________________________________________________________________"
echo ""
 
if [ -f  /var/log/secure ] ; then
   echo "  /var/log/secure or auth.log File Found  OK !! !!!!!!!!!!!!!PASSED !!!!!!!!!!!!! "
    RSLT86b="YES"
   else
   echo "  /var/log/secure  or auth.log File Not Found   !!!!---------FAILED------------- !!!!! "
   RSLT86b="NO"
 fi

if [ $RSLT86a = "YES" -o $RSLT86b = "YES" ] ; then
  RSLT86="YES"
else
  RSLT86="NO"
fi

#######################################

echo "__________________________________________________________________________________"
echo "/var/log/kernel "
echo "__________________________________________________________________________________"
echo ""

file1="/var/log/kernel"
for i in $file1
do
if [ -f $i -a $(stat -c %U:%G $i) = "root:root" -a $(stat -c %A $i) = "-rw-------" ] ; then
   RSLT87a="YES"
else
   RSLT87a="NO"
fi
done

file2="/var/log/kernel"
for i in $file2
do
if [ -f $i -a $(stat -c %U:%G $i) = "root:root" -a $(stat -c %A $i) = "-rw-------" ] ; then
   RSLT87b="YES"
else
   RSLT87b="NO"
fi
done 

if [ $RSLT87a = "YES" -o $RSLT87b = "YES" ] ; then
 RSLT87="YES"
else
 RSLT87="NO"
fi

#######################################
 
echo "__________________________________________________________________________________"
echo "/var/log/loginlog "
echo "__________________________________________________________________________________"
echo ""

file1="/var/log/loginlog"
for i in $file1
do
if [ -f $i -a $(stat -c %U:%G $i) = "root:root" -a $(stat -c %A $i) = "-rw-------" ]; then
   RSLT88a="YES"
else
   RSLT88a="NO"
fi
done

file2="/var/log/daemon.log"
for i in $file2
do
if [ -f $i -a $(stat -c %U:%G $i) = "root:root" -a $(stat -c %A $i) = "-rw-------" ]; then
   RSLT88b="YES"
else
   RSLT88b="NO"
fi
done

if [ $RSLT88a = "YES" -o $RSLT88b = "YES" ] ; then
 RSLT88="YES"
else
 RSLT88="NO"
fi

#######################################

echo "__________________________________________________________________________________"
echo "/var/log/syslog "
echo "__________________________________________________________________________________"
echo ""

file1="/var/log/syslog"
for i in $file1
do
if [ -f $i -a $(stat -c %U:%G $i) = "root:root" -a $(stat -c %A $i) = "-rw-------" ]; then
   RSLT89a="YES"
else
   RSLT89a="NO"
fi
done

file2="/var/log/syslog"
for i in $file2
do
if [ -f $i -a $(stat -c %U:%G $i) = "root:root" -a $(stat -c %A $i) = "-rw-------" ]; then
   RSLT89b="YES"
else
   RSLT89b="NO"
fi
done

if [ $RSLT89a = "YES" -o $RSLT89b = "YES" ] ; then
 RSLT89="YES"
else
 RSLT89="NO"
fi

#######################################

echo "__________________________________________________________________________________"
echo "Turn On Additional Logging For FTP Daemon "
echo "__________________________________________________________________________________"
echo ""
 
 
 
 
if [ -f  /etc/vsftpd/vsftpd.conf  ] ; then 
 
SERVICE="vsftpd" 
 JKLM1=`chkconfig --list  |grep -w $SERVICE  | awk '{ print $5 $7 }'`
  if [ -n "$JKLM1" ] ; then
 
     echo " vsftp service status .. $JKLM1 "
    PK1=`grep xferlog_std_format /etc/vsftpd/vsftpd.conf | grep -v \#|awk -F= '{ print $2 }'` 
    PK2=`grep log_ftp_protocol  /etc/vsftpd/vsftpd.conf | grep -v \# | awk -F= '{ print $2 }'`
    if [ -n "$PK1" ] ; then
       if [ "$PK1" = "YES" ] ; then
        echo " xferlog_std_format =YES is enabled on vsftpd----PASSED----- "
        else
        echo " xferlog_std_format =YES is Disabled on vsftpd !!!!!!!!-----------FAILED----------- !!!!!!!!!!! "
       TKTEST=1
        fi
    else
    echo "  xferlog_std_format =YES is not added on in /etc/vsftpd/vsftpd.conf !!!-----------FAILED----------- !!!"
     TKTEST=1
    fi
 
       if [ -n "$PK2" ] ; then
       if [ "$PK2" = "YES" ] ; then
        echo " log_ftp_protocol=YES is enabled on vsftpd ----PASSED----- "
        else
        echo " log_ftp_protocol=YES is DISABLED on vsftpd !!!!!!!!-----------FAILED----------- !!!!!!!!!!! "
         TKTEST=1
        fi
     else
      echo " log_ftp_protocol=YES is not added on in /etc/vsftpd/vsftpd.conf !!!-----------FAILED----------- !!!"
        TKTEST=1 
    fi
 
 
   else
    echo " $SERVICE   IS NOT INSTALLED !!! PASSED !!!!!!!"
     TKTEST=2
    fi
     
 
 
 
 
elif [ -f /etc/xinetd.d/wu-ftpd ] ; then
echo " Wu-Ftp  is Installed on the server   "  
 
SERVICE="wu-ftp" 
 JKLM1=`chkconfig --list  |grep -w $SERVICE  | awk '{ print $5 $7 }'`
  if [ -n "$JKLM1" ] ; then
 
     echo " vsftp service status .. $JKLM1 "
    PK1=`grep xferlog_std_format /etc/xinetd.d/wu-ftpd | grep -v \#|awk -F= '{ print $2 }'` 
    PK2=`grep log_ftp_protocol /etc/xinetd.d/wu-ftpd | grep -v \# | awk -F= '{ print $2 }'`
    if [ -n "$PK1" ] ; then
       if [ "$PK1" = "YES" ] ; then
        echo " xferlog_std_format =YES is enabled on  wu-ftp----PASSED----- "
        else
        echo " xferlog_std_format =YES is Disabled on wu-ftp !!!!!!!!-----------FAILED----------- !!!!!!!!!!! "
       TKTEST=1
        fi
    else
    echo "  xferlog_std_format =YES is not added on in /etc/xinetd.d/wu-ftpd !!!-----------FAILED----------- !!!"
     TKTEST=1
    fi
 
       if [ -n "$PK2" ] ; then
       if [ "$PK2" = "YES" ] ; then
        echo " log_ftp_protocol=YES is enabled on wu-ftp ----PASSED----- "
        else
        echo " log_ftp_protocol=YES is DISABLED on wu-ftp !!!!!!!!-----------FAILED----------- !!!!!!!!!!! "
         TKTEST=1
        fi
     else
      echo " log_ftp_protocol=YES is not added on in /etc/xinetd.d/wu-ftpd !!!-----------FAILED----------- !!!"
        TKTEST=1 
    fi
 
 
   else
    echo " $SERVICE   IS NOT INSTALLED !!! PASSED !!!!!!!"
     TKTEST=2
    fi
     
 
else
   
    echo " ftp service  is not Installed .................  "
 
fi
 
 
 
 
 if [ "$TKTEST" = "1" ] ; then
 echo " OverALL test is   !!!!!!!-----------FAILED----------- !!!!!!!!!!!!-"
  RSLT90="NO"
 else
 echo " OverALL test is OK   !!!!!!!-----------PASSED----------- !!!!!!!!!!!!! "
 RSLT90="YES"
 fi

#######################################

echo "__________________________________________________________________________________"
echo "  Confirm Permissions On System Log Files   "
echo "__________________________________________________________________________________"
echo ""
 
 
 
for i in `cat /etc/syslog.conf | grep -v \# |  grep log | sed -e 's/-//g' | awk '{print $2}'` 
do
if [ -f $i ] ; then
PERMCH=`ls -l $i | awk '{print $1$3$4}'`
 
  if [ "$PERMCH" = "-rw-------rootroot" ] ; then
  echo " $i ==> Permission & Ownership is OK PASSED !!! "
  else
  echo " $i ==> Permission & Ownership is FAILED !!! shoule be root:root and 600 "
  OMTEST="NO"
  fi
  
else
echo " $i file not Found !!!"
fi
 
done
 
 
 
 
for  FIL in ` ls  /var/log/boot.log* /var/log/cron* /var/log/dmesg*  /var/log/ksyms*  2>/dev/null`
do
if [ -f $FIL ]; then
KM1=`ls -l  $FIL | awk '{ print $1  }' `
KM2=`ls -l $FIL | awk '{ print $3 $4 }'`
 
if [ "$KM1" = "-rw-------" -a "$KM2" = "rootroot" ] ; then
echo "$FIL --$KM1 --$KM2    Permission is OK  !!!---------------PASSED !!!!!!!!!!!!!!!"
else
echo " "
echo  "$FIL --$KM1 --$KM2     !!!!!!!!-----------FAILED----------- !!!!!!!!!!!!!"
echo " it should be 600 and root:root $FIL "
OMTEST="NO"
fi
else
echo " Directory found == $i "
fi
done
 
 
for  FIL in ` ls   /var/log/httpd/*   /var/log/maillog*  /var/log/messages*  /var/log/news/*  /var/log/pgsql  /var/log/rpmpkgs*  /var/log/samba/*   /var/log/sa/*  /var/log/scrollkeeper.log secure* spooler*  /var/log/squid/*  /var/log/vbox/* 2>/dev/null `
do
if [ -f $FIL ] ; then
KM1=`ls -l  $FIL | awk '{ print $1  }' `
KM2=`ls -l $FIL | awk '{ print $3 $4 }'`
 
if [ "$KM1" = "-rw-------" -a "$KM2" = "rootroot" -o  "$KM1" = "-rw-r-----"  -o "$KM1" = "-rw-------" ] ; then
echo "$FIL ==> $KM1 ==> $KM2    Permission is OK  !!!---------------PASSED !!!!!!!!!!!!!!!"
else
echo " "
echo  "$FIL ==> $KM1 ==> $KM2     !!!!!!!!-----------FAILED----------- !!!!!!!!!!!!!"
OMTEST="NO"
fi
 
else
echo " Directory Found ......$FIL "
 
fi
done
 
for  FIL in ` ls  /var/log/gdm/* /var/log/httpd/* /var/log/news/* /var/log/samba/*  2>/dev/null  `
do
 
if [ -f $FIL ] ; then 
 
KM1=`ls -l  $FIL | awk '{ print $1  }' `
KM2=`ls -l $FIL | awk '{ print $3 $4 }'`
 
if [ "$KM1" = "-rw-------" -a "$KM2" = "rootroot" -o "$KM1" = "-rw-r-----" ] ; then
echo "$FIL==>$KM1 ==> $KM2    Permission is OK  !!!---------------PASSED !!!!!!!!!!!!!!!"
else
echo " "
echo  "$FIL ==> $KM1 ==> $KM2     !!!!!!!!-----------FAILED----------- !!!!!!!!!!!!!"
OMTEST="NO"
fi
else
echo  " Directory Found ..... $FIL "
fi 
 
done
 
for  FIL in `  ls    /var/log/kernel  /var/log/syslog  /var/log/loginlog  2>/dev/null `
do
KM1=`ls -l  $FIL | awk '{ print $1  }' `
KM2=`ls -l $FIL | awk '{ print $3 $4 }'`
 
if [ "$KM1" = "-rw-------" -a "$KM2" = "rootroot" ] ; then
echo "$FIL ==> $KM1 ==> $KM2    Permission is OK  !!!---------------PASSED !!!!!!!!!!!!!!!"
else
echo " "
echo  "$FIL ==> $KM1 ==>$KM2     !!!!!!!!-----------FAILED----------- !!!!!!!!!!!!!"
OMTEST="NO"
fi
done
 
 
for  FIL in ` ls   /var/log/wtmp  2>/dev/null `
do
KM1=`ls -l  $FIL | awk '{ print $1  }' `
KM2=`ls -l $FIL | awk '{ print $3 $4 }'`
 
if [ "$KM1" = "-rw-rw-r--"  -o "$KM1" = "-rw-r----" -a "$KM2" = "rootutmp" ] ; then
echo "$FIL ==> $KM1 ==> $KM2    Permission is OK  !!!---------------PASSED !!!!!!!!!!!!!!!"
else 
echo " "
echo  "$FIL ==> $KM1 ==> $KM2     !!!!!!!!-----------FAILED----------- !!!!!!!!!!!!!"
echo " "
OMTEST="NO"
fi
done
 
for  FIL in ` ls   /var/log/squid/* /var/log/pgsql/*  2>/dev/null `
do
KM1=`ls -l  $FIL | awk '{ print $1  }' `
KM2=`ls -l $FIL | awk '{ print $3 $4 }'`
 
if [ "$KM1" = "-rw-r--r--"  -o "$KM1" = "-rw-r----" -a "$KM2" = "squidsquid" -o  "$KM2" = "postgrespostgres" ] ; then
echo "$FIL ==> $KM1 ==> $KM2    Permission is OK  !!!---------------PASSED !!!!!!!!!!!!!!!"
else 
echo " "
echo  "$FIL ==> $KM1 ==> $KM2     !!!!!!!!-----------FAILED----------- !!!!!!!!!!!!!"
echo " "
OMTEST="NO"
fi
done
 
 
 
if [ "$OMTEST" = "NO" ] ; then
echo ""
echo " Over ALL Test is  !!!!!!-----------FAILED----------- !!!!!!!!!  "
echo " it should be 600 and ownerhip would be root.root "
 RSLT91="NO"
else
echo ""
echo " Over ALL Test is  !!!!!!--------PASSED ---------- !!!!!!!!!  "
 RSLT91="YES"
fi

#######################################

echo "__________________________________________________________________________________"
echo "Confirm Permissions On System Log Files Configure syslogd to Send Logs to a Remote LogHost "
echo "__________________________________________________________________________________"
echo ""

MNT=`grep "@"  /etc/syslog.conf  | awk '{print $2 }'`
if [ -n "$MNT" ] ; then
echo " Loghost is Configured in SYSLOGD .............  ----------PASSED ----------------"
echo " Loghost is $MNT "
RSLT92="YES"
else
echo " Logs not forwarded to Syslog Server....--FAILED---  " 
RSLT92="NO" 
fi

#######################################

echo "__________________________________________________________________________________"
echo "Log Archiving at least 60 days "
echo "__________________________________________________________________________________"
echo ""
echo " We kept all our Logs in  DCTOOL Server and also stored in Syslog Server for ALL Linux Servers !!!!!!!PASSED !!!!!!"
RSLT93="YES"

#######################################

echo "__________________________________________________________________________________"
echo "PASS_MAX_DAYS  Checking 45 Days in /etc/shadow for all normal users"
echo "__________________________________________________________________________________"
echo ""
JKM=`grep PASS_MAX_DAYS /etc/login.defs | grep -v \# | awk '{ print $2 }'`
if [ "$JKM" = "45" ] ; then
echo "PASS_MAX_DAYS -- $JKM is set in /etc/login.defs  -------------PASSED !!!!!!!!!!!!!!!!"
echo " cheking all User's password for expiration settings .............. "
 
 
###2009-Oct-09 Review updated 
 
for UN in `cat /etc/passwd | awk  -F: ' $3>499 && $3< 5000{print $1}'`
do
CHKVAL=`grep -w $UN /etc/shadow|awk -F: '{print $5 }'`
if [ "$CHKVAL" = "45" ]; then
echo "$UN Password verification of 45 days settings is OK in /etc/shadow .. PASSED "
else
echo "$UN Password verification of 45 days settings is Wrong in /etc/shadow !!.. FAILED !! Please Make it 45 days by chage -M command "
MYCJK=1
fi
done
 
if [ "$MYCJK" = "1" ] ; then
echo " Overall All Users Password verification of 45 days Max DAYS Settings is FAILED as per /etc/shadow !!!! "
RSLT94="NO"
else
echo " Overall All Users Password verification of 45 days Max DAYS Settings is PASSED as per /etc/shadow !!!! "
RSLT94="YES"
fi
 
else
echo "PASS_MAX_DAYS -- $JKM  ---------is set in /etc/login.defs !!!-----------FAILED----------- !!!!! "
RSLT94="NO"
fi
echo "Password age to be set in /etc/login.defs as well as by chage -M command to reflect in /etc/shadow"

#######################################

echo "__________________________________________________________________________________"
echo "PASS_MIN_LEN"
echo " password required /lib/security/\$ISA/pam_cracklib.so  retry=3 minlen=8 dcredit=0 ucredit=0 lcredit=0 ocredit=0     " 
echo "__________________________________________________________________________________"
echo ""
JKM=`   grep PASS_MIN_LEN /etc/login.defs | grep -v \# | awk '{ print $2 }'    `
JKM5=`  grep retry /etc/pam.d/system-auth | grep -v \# | awk '{ print $4}'    `
JKM6=`  grep  minlen /etc/pam.d/system-auth | grep -v \# | awk '{ print $5}'  `
JKM7=`  grep dcredit /etc/pam.d/system-auth | grep -v \# | awk '{ print $6}'  `
JKM8=`  grep ucredit /etc/pam.d/system-auth | grep -v \# | awk '{ print $7}'  `
JKM9=`  grep lcredit /etc/pam.d/system-auth | grep -v \# | awk '{ print $8}'  `
JKM10=` grep ocredit /etc/pam.d/system-auth | grep -v \# | awk '{ print $9}'  `
JKM11=`grep pam_passwdqc.so /etc/pam.d/system-auth | grep -v ^# | awk '{print $4}' | awk -F, '{print $5}'`

if [ "$JKM" = "8" -a "$JKM5" = "retry=3" -a  "$JKM6" = "minlen=8"  -a "$JKM7" = "dcredit=0" -a  "$JKM8" = "ucredit=0"  -a "$JKM9" = "lcredit=0" -a  "$JKM10" = "ocredit=0" ] ; then
echo " entry exists for  PASS_MIN_LEN in login.defs  and minlen in  /etc/pam.d/system-auth  -------------PASSED !!!!!!!!!!!!!!!!"
RSLT95="YES"
else
echo "entry  not exists for PASS_MIN_LEN in  login.defs  or minlen in  /etc/pam.d/system-auth !!!-----------FAILED----------- !!!!! "
echo " "
 
grep  minlen /etc/pam.d/system-auth | grep -v \# | awk '{ print $5}' 
 
echo " password required /lib/security/\$ISA/pam_cracklib.so  retry=3 minlen=8 dcredit=0 ucredit=0 lcredit=0 ocredit=0     " 
echo " Make sure above entry exists  on server PASS_MIN_LEN in /etc/login.defs and /etc/pam.d/system-auth "
RSLT95="NO"
fi

if [ "$JKM11" = "8" ] ; then
echo " Entry Exists for pam_passwdqc.so in /etc/pam.d/system-auth"]
else
echo " Entry not Exists for pam_passwdqc.so in /etc/pam.d/system-auth"]
fi

#######################################

echo "__________________________________________________________________________________"
echo " Minimum Password Age ,Field 4 of /etc/shadow must be 1 for all userids "
echo "__________________________________________________________________________________"
echo " "
echo " Check With All normal User with chage command   "
BNM=`grep PASS_MIN_DAYS /etc/login.defs | grep  -v \#  | awk '{print $2}'`
if [ "$BNM" = "1" ] ; then
echo " PASS_MIN_DAYS 1 Settings PASSED  !!!!!!!!!!! "
else
echo "   PASS_MIN_DAYS 1 Settings FAILED   !!!!!!!!!!! "
echo " "
echo " Please add PASS_MIN_DAYS 1  at /etc/login.defs "
NJ="yes"
fi
 
for i in `cat /etc/passwd | awk -F:  ' $3>=500 && $3 < 5000 {print $1 }'  ` 
 do
 
 JUI=`cat /etc/shadow | grep -w $i | awk -F: '{print $4}'`
 
 
 if [ "$JUI" = "1" ]; then
 echo " $i -------------  PASSWORD MIN Days = $JUI   -----------------  PASSSED --------------------- "
 else
 echo " $i -------------- PASSWORD MIN Days = $JUI   !!!!!-----------------  FAILED   ------------------ !!!! "
 NJ="yes"
 fi
 done
 
if [ "$NJ" = "yes" ] ; then
echo " Overall testing of PASS_MIN_DAYS 1  is !!!!!!!!!!---------FAILED  -------- .!!!!!!!!!!!! "
RSLT96="NO"
 
else
echo " Over All Testing of PASS_MIN_DAYS 1 is  -------------- PASSED -------------------------  "
echo " Please run  chage -m 1 <<User Name >> to set the minimum days "
RSLT96="YES"
echo " "
fi

#######################################

echo "__________________________________________________________________________________"
echo "/etc/security/opasswd must exists 0600 permissions"
echo "__________________________________________________________________________________"
echo ""
 
if [ -f /etc/pam.d/system-auth ] ; then
 
MYVAl1=`grep remember /etc/pam.d/system-auth |awk '{print $4 }' | awk -F= '{print $2}'|tail -1 `
else
echo " file /etc/pam.d/system-auth not found ......"
fi
 
 if [ -f /etc/security/opasswd ] ; then
  echo " File Found !!!  -------PASSED -------------"
  if [ "`ls -l /etc/security/opasswd | awk '{ print $1 }'`" = "-rw-------" ] ; then
   echo " Permission is OK  ----------PASSED --------"
     if [ "$MYVAl1" = "4" ] ; then
      echo "  remember entry exists in /etc/pam.d/system-auth-------PASSED ------- "
       RSLT97="YES"
      else
      echo "  remember entry not  exists in /etc/pam.d/system-auth---FAILED-------------"
          echo " Please add [ password required /lib/security/pam_unix.so remember=4 use_authtok md5 ]  in /etc/pam.d/system-auth"
       RSLT97="NO"
     fi
   else
   echo "Permission is Wrong  --FAILED--------------"
        echo " Please add [ password required /lib/security/pam_unix.so remember=4 use_authtok md5 ]  in /etc/pam.d/system-auth"
 
   RSLT97="NO"
  fi
 
   else
  echo " FILE NOT FOUND  ------------------------!!! FAILED !!!!_-----------------"
  echo " Please create a files   /etc/security/opasswd "
  RSLT97="NO"
  fi

#######################################

echo "__________________________________________________________________________________"
echo "Immediately expire new and manually reset passwords for new accunt"
echo "__________________________________________________________________________________"
echo ""
 
echo " It is Done By L1 Team So Treated As Compliant !!!!!!!!!  PASSED !!!!!!!!! "
 RSLT98="YES"

#######################################

echo "__________________________________________________________________________________"
echo "Lockout Accounts After 3 Failures"
echo "__________________________________________________________________________________"
echo ""
 
 
MKL=""
MKL=`grep  -w deny /etc/pam.d/system-auth  | awk '{print $5 }' | grep -v sense | awk -F= '{print $2}'`
MKL8=` grep onerr /etc/pam.d/system-auth | awk '{print $5 }'| grep -v sense`
if [ "$MKL" = "3" ] ; then
echo " Lockout Accounts After 5 Failures Entry Found !!!!!!!!  --------------!!PASSED---------------!!! "
echo " Login Attemps set in /etc/pam.d/system-auth is $MKL "
echo " Line   auth  required  /lib/security/\$ISA/pam_tally.so onerr=fail no_magic_root  present " 
 
grep  -w deny /etc/pam.d/system-auth | grep -v user
RSLT99="YES"
else
echo " Lockout Accounts After 3 Failures Entry Not Found !!!!! -------------FAILED --------------------"
echo " Login Attemps set in /etc/pam.d/system-auth is $MKL "
echo " Please add [account     required      /lib/security/\$ISA/pam_tally.so deny=5 reset no_magic_root] "
echo " Line   auth  required  /lib/security/\$ISA/pam_tally.so onerr=fail no_magic_root should be present " 
echo " Current settings ========`grep  -w deny /etc/pam.d/system-auth | grep -v user` "
grep onerr /etc/pam.d/system-auth | awk '{print $5 }'
 
RSLT99="NO"
 
fi

#######################################

echo "__________________________________________________________________________________"
echo "    Each uid must only be used once "
echo "__________________________________________________________________________________"
echo ""
 
 VAL7=`cat /etc/passwd | awk -F: '{print $3}' | sort -n  |uniq -d`
  
  if  [ -n "$VAL7" ]; then
    echo " Duplicate UID 's found-       ---------FAILED ---------!!! "
    echo "Duplicate ---UID ---> $VAL7 " 
    RSLT100="NO"
   else
 
   echo " NO Duplicate ID Found ...  OK !! PASSED  !!!!!!!!! ... "
    RSLT100="YES"
 
   fi

#######################################

echo "__________________________________________________________________________________"
echo "Default UID Disabled/Changed "
echo "__________________________________________________________________________________"
echo ""
FAILTEST=""
cat /etc/passwd | awk -F: '$3 < 500 { print $1 "+++" $7 }' | egrep -v  'root' >KLTEST
for i in `cat KLTEST`
 do
  MPTEST=`echo $i  | awk -F+ '{ print $4 }'`
   if [ "$MPTEST" = "/sbin/nologin" -o "$MPTEST" = "/bin/false" ] ; then
    echo " $i  Daemon User Should  Locked  $MPTEST  ------!!!PASSED !!!!!!!!!"
    else
    echo " $i     User Not Locked !!!! ----------------FAILED-----------------!!!!"
    FAILTEST="OK"
    fi
   
   done
 
echo "checking the  Daemon User  Locked or Not ...........  " 
 
for i in `cat KLTEST | awk -F- '{print $1 }' `
do
grep -w  $i /etc/shadow  >>lockeduser
done
 
for nnnk  in `cat lockeduser | awk -F: '{print $2 "-" $1}'`
do
GTJ=`echo "$nnnk" | awk -F- '{print $1 }' `
if [ "$GTJ" = "!!" -o "$GTJ" = "!" -o "$GTJ" = "!*" ] ;then
echo " Accounts  $nnnk is Locked ...................  PASSED "
else
echo " Account  $nnnk  is Unlocked ----------------------FAILED ----------- "
 FAILTEST="OK"
fi
 
done
 
 
   rm -f lockeduser
   rm -f KLTEST
if [ "$FAILTEST" = "OK" ] ;then
echo " "
RSLT101="NO"
echo "OverAll testing of Daemon User test is !!!!!!!!!!--------------------------FAILED-------------------!!!!!!!!!!!!!!!!!!!!!"
 
else
echo " "
 echo  "Overall testing of Daemon User test is !!!!!!!!!!--------------------------PASSED-------------------!!!!!!!!!!!!!!!!!!!!!"
RSLT101="YES"
 
fi
 
#######################################

echo "__________________________________________________________________________________"
echo "Guest/demo id Disabled "
echo "__________________________________________________________________________________"
echo ""

echo "Manual identification of Guest/Demo ID should be done as per policy"
RSLT102="NA"

#######################################

echo "__________________________________________________________________________________"
echo "Lock the daemon-user accounts related to servers"
echo "__________________________________________________________________________________"
echo ""
 
#cat /etc/passwd | awk -F: '$3 < 500 { print $1 "---" $7 }' | egrep -v  'sync|shutdown|root|halt' >KLTEST
cat /etc/passwd | awk -F: '$3 < 500 { print $1 "+++" $7 }' | egrep -v  'root' >KLTEST
 
for i in `cat KLTEST`
 do
  MPTEST=`echo $i  | awk -F+ '{ print $4 }'`
   if [ "$MPTEST" = "/sbin/nologin" -o "$MPTEST" = "/bin/false" ] ; then
    echo " $i  Daemon User Should  Locked  $MPTEST  ------!!!PASSED !!!!!!!!!"
    else
    echo " $i     User Not Locked !!!! ----------------FAILED-----------------!!!!"
    FAILTEST="OK"
    fi
   
   done
   rm -f KLTEST
if [ "$FAILTEST" = "OK" ] ;then
echo " "
echo "OverAll testing of Daemon User test is !!!!!!!!!!--------------------------FAILED-------------------!!!!!!!!!!!!!!!!!!!!!"
RSLT103="NO"
else
echo " "
echo  "Overall testing of Daemon User test is !!!!!!!!!!--------------------------PASSED-------------------!!!!!!!!!!!!!!!!!!!!!"
RSLT103="YES"
fi

#######################################

echo "__________________________________________________________________________________"
echo "/etc/ftpusers or /etc/vsftpd.ftpusers"
echo "__________________________________________________________________________________"
echo ""
 SERVICE="vsftpd"
 JKLM1=`chkconfig --list  $SERVICE 2>/dev/null  | awk '{ print $5 $7 }' `
  if [ -n "$JKLM1" ] ; then
    echo " $SERVICE is installed ..........."
      if [ -f /etc/ftpusers ] ; then
          CHECKVALUE=`cat /etc/ftpusers | grep -v \#  | wc -l`
          
          if [ $CHECKVALUE -gt 0 ] ; then
          
      echo " FIle Found  /etc/ftpusers  on the system  ...!!!!!PASSED !!!!!!!!!. and user id exist "
      echo "  Users  who should not have ftp access are added in this file  "
      
           else
 
           echo " No User id exits !! FAILED Please add root user into /etc/ftpusers file "
           MYTTT=1
           fi
           
            else
       echo " None of the File Found !!!!!!!!!! ----------FAILED---------- !!!!!!!!!!!!"
 
       MYTTT=1
       fi
           
           if [ -f /etc/vsftpd.ftpusers ] ; then
           CHECKVALUE=`cat /etc/vsftpd.ftpusers | grep -v \#  | wc -l`
          
          if [ $CHECKVALUE -gt 0 ] ; then
          
      echo " FIle Found /etc/vsftpd.ftpusers  on the system  ...!!!!!PASSED !!!!!!!!!. and user id exist "
      echo "  Users  who should not have ftp access are added in this file  "
       RSLT104="YES"
           else
             MYTTT=1
           echo " No User id exits !! FAILED Please add root user into /etc/vsftpd.ftpusers  file "
           fi
           
       else
       echo " None of the File Found !!!!!!!!!! ----------FAILED---------- !!!!!!!!!!!!"
 
          MYTTT=1
       fi
           
          
          if  [ "$MYTTT" = "1" ] ; then
          echo ""
          echo "OverALL Testing of ftpuser id existence  FAILED !!!! $RSLT104 "
             RSLT104="NO"
          else
             RSLT104="YES"
                 echo "OverALL Testing of ftpuser id existence  PASSED !!!! $RSLT104  "
          fi
         
   else
   echo "$SERVICE is NOT INSTALLED on the System -- !!!!!!!!!PASSED !!!!!!!!!!!!!! "
   RSLT104="YES"
  fi

#######################################

echo "__________________________________________________________________________________"
echo "    /etc/passwd containing the passwords and Locked account  "
echo "__________________________________________________________________________________"
echo ""
  for CN in  `cat /etc/passwd | awk -F: ' $3==0 || $3>=500 && $3 <=60000 { print $1 ":" $2}'`
   do
   JKL=`echo $CN | awk -F: '{ print $2 }'`
   if [ "$JKL" = "x" ] ; then
   echo " USER -- $CN encrypted password is set !! !!!!!!PASSED !!!!!!!! "
   
    
   else
   echo " encrypted password is  not set !!!!!!!!!! ----FAILED --- !!!!!!!!!!!!!!!! $CN "
   TKL=2
   fi
   done
 
 echo " For Locked Account We do this Activity on Every Months ...."
 RSLT105="YES"  ########this Point is added it was missing ..
 
   

#######################################

echo "__________________________________________________________________________________"
echo "    /etc/passwd containing the passwords and Locked account  "
echo "__________________________________________________________________________________"
echo ""
  for CN in  `cat /etc/passwd | awk -F: '$3==0 || $3>=500 && $3 <=60000 { print $1 ":" $2}'`
   do
   JKL=`echo $CN | awk -F: '{ print $2 }'`
   if [ "$JKL" = "x" ] ; then
   echo " USER -- $CN encrypted password is set !! !!!!!!PASSED !!!!!!!! "
   
    
   else
   echo " encrypted password is  not set !!!!!!!!!! ----FAILED --- !!!!!!!!!!!!!!!! $CN "
   TKL=2
   fi
   done
 
 echo " For Locked Account We do this Activity on Every Months ...."
 RSLT106="YES"  ########this Point is added it was missing ..
 

#######################################

echo "__________________________________________________________________________________"
echo "Rules file used by PAM(Pluggable Authentication Modules) to control system authentication"
echo "__________________________________________________________________________________"
echo ""
mypam=`cat /etc/pam.d/system-auth | grep pam_listfile.so | grep -v \#  | awk '{print $1$2$3$4$5}'`
if [ -n "$mypam" -a "$mypam" = 'authrequiredpam_listfile.soitem=usersense=deny'  ] ; then
echo " pam_listfile.so configuration found at PAM  ----- PASSED !!!!!!!!!  "
  if [ -f /etc/security/user  ] ; then
  echo " /etc/security/user file FOUND On Server !!!!!!!!!!  " 
   permuser=`ls -l /etc/security/user | awk '{print $1}'`
   if [ "$permuser" = "-rw-------" ] ; then
   echo " Permission is OK $permuser /etc/security/user PASSED !!!!!! "
   RSLT107="YES"
  else
  echo " Permission is NOT OK FAILED !!!!!!! "
  echo " Note Permission would be 600 /etc/security/user "
  RSLT107="NO"
  fi
  
  else
  echo " /etc/security/user file NOT  FOUND On Server !!!!!!!!!!  "
   RSLT107="NO"
  fi
 
else
echo "pam_listfile.so configuration NOT found at PAM  ----- FAILED !!!!!!!!!  " 
echo "Note Please add below Line in /etc/pam.d/system-auth  file "
echo " auth  required  /lib/security/$ISA/pam_listfile.so item=user sense=deny file=/etc/security/user  onerr=succeed "
echo " create the file /etc/security/user and  permssion should be  600 "
 RSLT107="NO"
fi

#######################################

echo "__________________________________________________________________________________"
echo "/etc/security/opasswd must exists 0600 permissions"
echo "__________________________________________________________________________________"
echo ""
 
if [ -f /etc/pam.d/system-auth ] ; then
 
MYVAl1=`grep remember /etc/pam.d/system-auth |awk '{print $4 }' | awk -F= '{print $2}'|tail -1 `
else
echo " file /etc/pam.d/system-auth not found ......"
fi
 
 if [ -f /etc/security/opasswd ] ; then
  echo " File Found !!!  -------PASSED -------------"
  if [ "`ls -l /etc/security/opasswd | awk '{ print $1 }'`" = "-rw-------" ] ; then
   echo " Permission is OK  ----------PASSED --------"
     if [ "$MYVAl1" = "4" ] ; then
      echo "  remember entry exists in /etc/pam.d/system-auth-------PASSED ------- "
       RSLT108="YES"
      else
      echo "  remember entry not  exists in /etc/pam.d/system-auth---FAILED-------------"
          echo " Please add [ password required /lib/security/pam_unix.so remember=4 use_authtok md5 ]  in /etc/pam.d/system-auth"
       RSLT108="NO"
     fi
   else
   echo "Permission is Wrong  --FAILED--------------"
        echo " Please add [ password required /lib/security/pam_unix.so remember=4 use_authtok md5 ]  in /etc/pam.d/system-auth"
 
   RSLT108="NO"
  fi
 
   else
  echo " FILE NOT FOUND  ------------------------!!! FAILED !!!!_-----------------"
  echo " Please create a files   /etc/security/opasswd "
  RSLT108="NO"
  fi

#######################################

echo "__________________________________________________________________________________"
echo "/etc/ftpusers or /etc/vsftpd.ftpusers"
echo "__________________________________________________________________________________"
echo ""
 SERVICE="vsftpd"
 JKLM1=`chkconfig --list  $SERVICE 2>/dev/null  | awk '{ print $5 $7 }' `
  if [ -n "$JKLM1" ] ; then
    echo " $SERVICE is installed ..........."
      if [ -f /etc/ftpusers ] ; then
          CHECKVALUE=`cat /etc/ftpusers | grep -v \#  | wc -l`
          
          if [ $CHECKVALUE -gt 0 ] ; then
          
      echo " FIle Found  /etc/ftpusers  on the system  ...!!!!!PASSED !!!!!!!!!. and user id exist "
      echo "  Users  who should not have ftp access are added in this file  "
      
           else
 
           echo " No User id exits !! FAILED Please add root user into /etc/ftpusers file "
           MYTTT=1
           fi
           
            else
       echo " None of the File Found !!!!!!!!!! ----------FAILED---------- !!!!!!!!!!!!"
 
       MYTTT=1
       fi
           
           if [ -f /etc/vsftpd.ftpusers ] ; then
           CHECKVALUE=`cat /etc/vsftpd.ftpusers | grep -v \#  | wc -l`
          
          if [ $CHECKVALUE -gt 0 ] ; then
          
      echo " FIle Found /etc/vsftpd.ftpusers  on the system  ...!!!!!PASSED !!!!!!!!!. and user id exist "
      echo "  Users  who should not have ftp access are added in this file  "
       RSLT109="YES"
           else
             MYTTT=1
           echo " No User id exits !! FAILED Please add root user into /etc/vsftpd.ftpusers  file "
           fi
           
       else
       echo " None of the File Found !!!!!!!!!! ----------FAILED---------- !!!!!!!!!!!!"
 
          MYTTT=1
       fi
           
          
          if  [ "$MYTTT" = "1" ] ; then
          echo ""
          echo "OverALL Testing of ftpuser id existence  FAILED !!!! $RSLT109 "
             RSLT109="NO"
          else
             RSLT109="YES"
                 echo "OverALL Testing of ftpuser id existence  PASSED !!!! $RSLT109  "
          fi
         
   else
   echo "$SERVICE is NOT INSTALLED on the System -- !!!!!!!!!PASSED !!!!!!!!!!!!!! "
   RSLT109="YES"
  fi

#######################################


echo "__________________________________________________________________________________"
echo "UsePAM yes in /etc/ssh/sshd_config"
echo "__________________________________________________________________________________"
echo ""

K7=`grep UsePAM /etc/ssh/sshd_config | grep -v \# | awk '{print $2}' | sed -e 's/ //g'`   #### 2009-Oct-Checked 
if [  -n "$K7" -a "$K7" = "yes" ] ; then
 echo " UsePam Settings is Yes  !!!!!!!! PASSED !!!!!!!!!    "
 RSLT110="YES"
else
RSLT110="NO"
 echo "  UsePAM yes  !!!!!!--------- FAILED-------------- !!  [[ Should be UsePAM yes ]] "
fi

#######################################

echo "__________________________________________________________________________________"
echo "/.rhosts Should not be allowed to exist on "
echo "__________________________________________________________________________________"
echo ""
 
 for home in `cat /etc/passwd | awk -F: '$3>=500 && $3 <=6000 {print $6}'`
 do
   #find $home   \( -name ".rhosts" -o -name ".netrc"  \) >> netrc-list
#   find /root   \( -name ".rhosts" -o -name ".netrc" \) >> netrc-list
 
  if [  -f  $home/.rhosts  -o  -f  $home/.netrc ] ; then
        echo " .rhost  or .netrc File Found ...  !!!!!!FAILED !!!! ..  --PATH = $home  "
   MAG="no"
  else
  echo "  .netrc  or  .rhosts File not Found ... !!!!!!!PASSED !!!!! PATH = $home "
  
  fi
 done
 
    if [ "$MAG" != "no" ] ; then
    echo " /.rhosts and /.netrc  not exists on server  !!!!!!!!!!!!!PASSED !!!!!!!!!!! "
    RSLT111="YES"
    else
    echo "  /.rhosts and /.netrc  exists on server !!!------------FAILED------------- !!!!!!!! "
    echo " File Found ..........  Please check !!!!!!!!!!!! "
    echo " "
   
    RSLT111="NO"
    fi

#######################################

echo "__________________________________________________________________________________"
echo "/.netrc Should not be allowed to exist on "
echo "__________________________________________________________________________________"
echo ""
 
 for home in `cat /etc/passwd | awk -F: '$3>=500 && $3 <=6000 {print $6}'`
 do
   #find $home   \( -name ".rhosts" -o -name ".netrc"  \) >> netrc-list
#   find /root   \( -name ".rhosts" -o -name ".netrc" \) >> netrc-list
 
  if [  -f  $home/.rhosts  -o  -f  $home/.netrc ] ; then
        echo " .rhost  or .netrc File Found ...  !!!!!!FAILED !!!! ..  --PATH = $home  "
   MAG="no"
  else
  echo "  .netrc  or  .rhosts File not Found ... !!!!!!!PASSED !!!!! PATH = $home "
  
  fi
 done
 
    if [ "$MAG" != "no" ] ; then
    echo " /.rhosts and /.netrc  not exists on server  !!!!!!!!!!!!!PASSED !!!!!!!!!!! "
    RSLT112="YES"
    else
    echo "  /.rhosts and /.netrc  exists on server !!!------------FAILED------------- !!!!!!!! "
    echo " File Found ..........  Please check !!!!!!!!!!!! "
    echo " "
   
    RSLT112="NO"
    fi

#######################################

echo "__________________________________________________________________________________"
echo "    Verify passwd, shadow, and group File Permissions 644 passwd 400 shadow."
echo "__________________________________________________________________________________"
echo ""
 
val3=` ls -l /etc/passwd | awk '{ print $1$3$4 }' `
val4=`ls -l /etc/shadow | awk '{ print $1$3$4 }'`
val5=`ls -l /etc/group  | awk '{ print $1$3$4 }' `
val6=`ls -l /etc/gshadow| awk '{ print $1$3$4 }'`
if [ "$val3" = "-rw-r--r--rootroot" -a "$val4" = "-r--------rootroot"  -a  "$val5" = "-rw-r--r--rootroot" -a "$val6" = "-r--------rootroot"  ] ; then
   echo " passwd - $val3  and shadow and gshadow -$val4  permission is OK  !!!!!!!!PASSED !!!!!!!!!!!!!! "
    echo " group permisson $val5    PASSED  !!!!!!!!!!!!! "
 
    RSLT113="YES"
 else
 echo "  passwd - $val3  and shadow  -$val4  permission  .!!!!---------FAILED ---------!! "
 echo " group permisson $val5   "
 echo " Please do [ chown root:root /etc/passwd /etc/shadow /etc/group , chmod 644 passwd group ,chmod 400 shadow gshadow ] "
  RSLT113="NO"
 
 fi

 ######################################

 echo "__________________________________________________________________________________"
echo "    / must be r-x or more restrictive.   for others ."
echo "__________________________________________________________________________________"
echo ""
 
 val3=`ls -ld  / | awk '{ print $1 }' `
 
if [ "$val3" = "drwxr-xr-x" ] ; then
   echo " / - $val3    permission is OK  !!!!!!!!PASSED !!!!!!!!!!!!!! "
    RSLT114="YES"
 else
 echo "  / - $val3   permission  .!!!!---------FAILED ---------!! "
  RSLT114="NO"
 fi

#######################################

echo "__________________________________________________________________________________"
echo "     /usr must be r-x or more restrictive. for others"
echo "__________________________________________________________________________________"
echo ""
 
 val3=`ls -ld  /usr | awk '{ print $1 }' `
 
if [ "$val3" = "drwxr-xr-x" ] ; then
   echo " /usr - $val3    permission is OK  !!!!!!!!PASSED !!!!!!!!!!!!!! "
    RSLT115="YES"
 else
 echo "  /usr - $val3   permission  .!!!!---------FAILED ---------!! "
  RSLT115="NO"
 fi
 
#######################################

echo "__________________________________________________________________________________"
echo "     /var  must be r-x or more restrictive. for others "
echo "__________________________________________________________________________________"
echo ""
 
 val3=`ls -ld  /var | awk '{ print $1 }' `
 
if [ "$val3" = "drwxr-xr-x" ] ; then
   echo " /var - $val3    permission is OK  !!!!!!!!PASSED !!!!!!!!!!!!!! "
    RSLT116="YES"
 else
 echo "  /var - $val3   permission  .!!!!---------FAILED ---------!! "
  RSLT116="NO"
 fi

#######################################

echo "__________________________________________________________________________________"
echo "     /var/log  must be r-x or more restrictive. for others "
echo "__________________________________________________________________________________"
echo ""
 
 val3=`ls -ld  /var/log | awk '{ print $1 }' `
 
if [ "$val3" = "drwxr-xr-x" ] ; then
   echo " /var/log - $val3    permission is OK  !!!!!!!!PASSED !!!!!!!!!!!!!! "
    RSLT117="YES"
 else
 echo "  /var - $val3   permission  .!!!!---------FAILED ---------!!  [[ should be 755 /var/log ]] "
  RSLT117="NO"
 fi

#######################################

echo "__________________________________________________________________________________"
echo "    /tmp  rwxrwxrwt(1777)   "
echo "__________________________________________________________________________________"
echo ""
 
 val3=`ls -ld  /tmp | awk '{ print $1 }' `
 
if [ "$val3" = "drwxrwxrwt" ] ; then
   echo " /tmp - $val3    permission is OK  !!!!!!!!PASSED !!!!!!!!!!!!!! "
    RSLT118="YES"
 else
 echo "  /tmp- $val3   permission  .!!!!---------FAILED ---------!! "
  RSLT118="NO"
 fi
 
####################################### 
 
echo "__________________________________________________________________________________"
echo " /etc/snmpd/snmpd.conf Permissions must be 0640 or more restrictive if the file exists   "
echo "__________________________________________________________________________________"
echo ""
 
 if [ -f /etc/snmpd/snmpd.conf ] ; then
 echo " snmpd.conf file Found !!!!!!!!!  "
 
 val3=`ls -l  /etc/snmpd/snmpd.conf | awk '{ print $1 }' `
 
if [ "$val3" = "-rw-r-----" ] ; then
   echo " /tmp - $val3    permission is OK  !!!!!!!!PASSED !!!!!!!!!!!!!! "
    RSLT119="YES"
 else
 echo "  /tmp- $val3   permission  .!!!!---------FAILED ---------!! "
  RSLT119="NO"
 fi
 
else
 echo " File Not Found !!!!!!!!!!!!!!!!  ----------PASSED-----------------"
  RSLT119="YES"
 fi

#######################################

echo "__________________________________________________________________________________"
echo "World-Writable Directories Should Have Their Sticky Bit Set   "
echo "__________________________________________________________________________________"
echo ""
echo  " This is Quarterly Activity and We do in every 3 Months So no need to do  this Actvity in GSD .."
echo " Listing all World-Writable Directories without Sticky Bit Set"
#for PART in `awk '($3 == "ext2" || $3 == "ext3") { print $2 }' /etc/fstab`;#
for PART in /home /etc /bin /usr /usr/etc  /boot
do 
echo "Listing World-Writable Directories in Partition $PART "
find $PART -xdev -type d \( -perm -0002 -a ! -perm -1000 \) -print 
done 
RSLT120="YES"
#######################################

echo "__________________________________________________________________________________"
echo "Find Unauthorized World-Writable Files  "
echo "__________________________________________________________________________________"
echo ""
echo " "
echo  " This is Quarterly Activity and We do in every 3 Months So no need to do  this Actvity in GSD .."
echo " "
echo " Listing all Unauthorised World-Writable Files "

#for PART in `grep -v ^# /etc/fstab | awk '($6 != "0") {print $2}'`;#
for PART in /home /etc /bin /usr /usr/etc /var /boot
do
echo "Listing Unauthorised WW Files in Partition $PART"
find $PART -xdev -type f \( -perm -0002 -a ! -perm -1000 \) -print 
done 
RSLT121="YES"
 
#######################################

echo "__________________________________________________________________________________"
echo "Find Unauthorized SUID/SGID System Executables"
echo "__________________________________________________________________________________"
echo ""
echo " "
echo  " This is Quarterly Activity and We do in every 3 Months So no need to do  this Actvity in GSD .."
echo " "
echo " Listing all Unauthorized SUID/SGID System Executables"
#for PART in `grep -v ^# /etc/fstab | awk '($6 != "0") {print $2}'`;#
for PART in /home /etc /bin /usr /usr/etc /var /boot
do
echo "Listing Unauthorised SUID/SGID in Partition $PART"
find $PART \( -perm -04000 -o -perm -02000 \) -type f -xdev -print 
done 
RSLT122="YES"

#######################################

echo "__________________________________________________________________________________"
echo "Find All Unowned Files"
echo "__________________________________________________________________________________"
echo ""
echo " "
echo  " This is Quarterly Activity and We do in every 3 Months So no need to do  this Actvity in GSD .."
echo " "
echo "Listing All Unowned Files "
#for PART in `grep -v ^# /etc/fstab | awk '($6 != "0") {print $2}'`;#
for PART in /home /etc /bin /usr /usr/etc /var /boot
do
echo " Listing Unowned Files in Partition $PART"
find $PART -nouser -o -nogroup -print 
done
RSLT123="YES"

#######################################

echo "__________________________________________________________________________________"
echo "Disable USB Devices (AKA Hotplugger)"
echo "__________________________________________________________________________________"
echo ""
CHKUSB=`cat /etc/modprobe.conf  |  grep -v \# | grep usb-storage`
if [ -n "$CHKUSB" -a -f /lib/modules/2.6.18-164.el5xen/kernel/drivers/usb/storage/usb-storage.ko ] ; then
echo " Please rename or delete /lib/modules/2.6.18-164.el5xen/kernel/drivers/usb/storage/usb-storage.ko "
echo  " please remove usb-storage from /etc/modprobe.conf "
RSLT124="NO"
else
echo " Disable USB Device (AKA Hotplugger )   PASSED !!!"
echo "  Rest This Disabled  Through BIOS ... PASSED !!!"
RSLT124="YES"
fi

#######################################

echo "__________________________________________________________________________________"
echo "Socket,piped,block,character,symbolic link files may be world writeable"
echo "__________________________________________________________________________________"
echo ""

echo "Exception to OSRs (exempt from OSR requirements)...as per IM"

  RSLT125="YES"

#######################################

echo "__________________________________________________________________________________"
echo "Protecting Resources - User Resources"
echo "__________________________________________________________________________________"
echo ""

RSLT126="YES"

#######################################

echo "__________________________________________________________________________________"
echo " HOME  Default Permissions at time of creation: 700 or 750 . Must be owned by userid)"
echo "__________________________________________________________________________________"
echo ""
 
for i in `cat /etc/passwd | awk -F: '$3 >=500 &&  $3 <= 6000 {print $6}'`
do
 
if  [ -d $i ] ; then
 
if ls -ld $i | grep -v  \>   > /dev/null 
then 
 
MKL=`ls -ld $i | awk '{ print $1 }' 2>/dev/null`
 
if [  "$MKL" = "drwx------" -o "$MKL" = "drwxr-x---" ] ; then
echo " Home Directory $i --- $MKL  ----------------PASSED---------- "
else
echo " Home Directory $i --- $MKL         !!!!!!!!! ------------FAILED -----------!!!!!!!!!!!"
FGH="NO"
fi
 
else
echo " This is not Directory   $i  "
MKN=`ls -ld $i | awk '{ print $NF }' 2>/dev/null`
MKL=`ls -ld $MKN | awk '{ print $1 }' 2>/dev/null`
 
if [ "$MKL" = "drwx------" -o "$MKL" = "drwxr-x---" ] ; then
echo " Home Directory $i --- $MKL  ----------------PASSED---------- "
else
echo " Home Directory $i --- $MKL         !!!!!!!!! ------------FAILED -----------!!!!!!!!!!!"
FGH="NO"
fi
 
 
fi
else
echo " $i does not Seeems like Directory it may does not exists "
fi
done
 
 
if [ "$FGH" = "NO" ] ; then
echo " OverALL Testing is FAILED !!!! ------------FAILED -----------!!!!!!! "
echo " Note -: Please Make the Home Directories Permission to 750 or 700 as applicable "
RSLT127="NO"
else
echo " OverALL Testing is PASSED  ----------------PASSED----------      "
RSLT127="YES"
fi

#######################################

echo "__________________________________________________________________________________"
echo "System id home directories"
echo "__________________________________________________________________________________"
echo ""
for j in `cat /etc/passwd  | awk  -F: ' $3 > 0 &&  $3 < 500   { print $0 }' | grep -v \/home`
 do
 JK=`echo $j | awk -F: '{ print $6 }' `
 ls -ld $JK  2>/dev/null
done
echo " OverALL Testing is PASSED  ----------------PASSED----------      "
RSLT128="YES"

#######################################

echo "__________________________________________________________________________________"
echo "Create Warnings for Network and Physical Access Service."
echo "__________________________________________________________________________________"
echo ""
if [ -f /etc/issue ] ; then
echo " File for Banner Found in /etc/issue !!!!!!!!  PASSED!!!!!!!!!!! "
 
echo " Checking the Ownership of   /etc/motd /etc/issue /etc/issue.net "
for nnn in  /etc/motd /etc/issue /etc/issue.net 
do
GTY=`ls -l  $nnn | awk '{print $1}'`
GTY1=` ls -l $nnn | awk '{print $3 $4 }' `
if [ "$GTY" = "-rw-r--r--" -a "$GTY1" = "rootroot" ] ; then
echo " $nnn ------- $GTY     ---  PASSED -------------- "
else
echo " $nnn  ------- $GTY    ---  FAILED  --------------  "
fi
done
 
RTY=`grep UNAUTHORIZED  /etc/issue | awk '{ print $1 }'`
if [ "$RTY" = "UNAUTHORIZED" ] ; then
echo " Warinings Found for Physical Access services ----------PASSED---------------"
RSLT129="YES"
cat /etc/issue
 
if [ -f /etc/issue.net ] ; then
echo " /etc/issue.net  file Found .............  PASSED ----------------- "
           RTY=`grep UNAUTHORIZED  /etc/issue | awk '{ print $1 }'`
           if [ "$RTY" = "UNAUTHORIZED" ] ; then
           echo " Warinings Found for Physical Access services ----------PASSED---------------"
           RSLT129="YES"
 
           else
            echo " /etc/issue.net Warning not Found --------!!!!!!!!!!!!   FAILED - -----!!!!! "
              RSLT129="NO"
           fi
                  
else       
 
echo " file not found /etc/issue.net ---------------!!!!!!!!!FAILED !!!!!!!!!!!!!!!"
fi
 
else
echo "  Warinings not Found for Physical Access services ----GSD-COMPLIANT ------------ "
echo " Warnings are added in /etc/motd "
RSLT129="NO"
cat /etc/issue
fi
 
else
echo " File for Banner  Not Found !!!!!!!!!! ------------FAILED -----------!!!!!!! "
RSLT129="N0"
 
fi

#######################################

echo "__________________________________________________________________________________"
echo "Protect Banner."
echo "__________________________________________________________________________________"
echo ""
if [ -f /etc/motd ] ; then
echo " File Found  for PROTECTIONS in /etc/motd !!!!!!!!  PASSED!!!!!!!!!!! "
 
echo " Checking the Ownership of   /etc/motd /etc/issue /etc/issue.net "
for nnn in  /etc/motd /etc/issue /etc/issue.net 
do
GTY=`ls -l  $nnn | awk '{print $1}'`
GTY1=` ls -l $nnn | awk '{print $3 $4 }' `
if [ "$GTY" = "-rw-r--r--" -a "$GTY1" = "rootroot" ] ; then
echo " $nnn ------- $GTY     ---  PASSED -------------- "
else
echo " $nnn  ------- $GTY    ---  FAILED  --------------  "
fi
done
 
 
 
RTY=`grep UNAUTHORIZED  /etc/motd | awk '{ print $1 }'`
RTY1=`grep WARNING  /etc/motd  | awk '{print $1}' | sed -e 's/!//g'`
if [ "$RTY" = "UNAUTHORIZED" -a "$RTY1" = "WARNING" ] ; then
echo " Warinings Found for Physical Access services ----------PASSED---------------"
RSLT130="YES"
cat /etc/motd
 
else
echo "  Warinings not Found for Physical Access services ------FAILED ------------ "
RSLT130="NO"
#
echo " File for Banner  Not Found !!!!!!!!!! ------------FAILED -----------!!!!!!! "
echo " Please add below information .. in /etc/motd and /etc/issue "
echo "                           !!!WARNING!!!                    "
echo " ############################################################################"
echo "  ACCESS TO THIS SYSTEM IS STRICTLY RESTRICTED TO AUTHORIZED PERSONS ONLY "
echo "UNAUTHORIZED ACCESS TO THIS SYSTEM IS NOT ALLOWED AND EVERY ACTIVITY IS MONITORED ON THIS "
echo "                                       SYSTEM.                                "
echo "###########################################################################################"
 
fi
 
else
echo " File for Banner  Not Found !!!!!!!!!! ------------FAILED -----------!!!!!!! "
echo " Please add below information .. in /etc/motd and /etc/issue "
echo "                           !!!WARNING!!!                    "
echo " ############################################################################"
echo "  ACCESS TO THIS SYSTEM IS STRICTLY RESTRICTED TO AUTHORIZED PERSONS ONLY "
echo "UNAUTHORIZED ACCESS TO THIS SYSTEM IS NOT ALLOWED AND EVERY ACTIVITY IS MONITORED ON THIS "
echo "                                       SYSTEM.                                "
echo "###########################################################################################"
RSLT130="N0"
 
fi

#######################################

echo "__________________________________________________________________________________"
echo "Create Warning for GUI Based Logins."
echo "__________________________________________________________________________________"
echo ""
if [ -f /etc/X11/xdm/Xresources ] ; then
  echo " File Found   /etc/X11/xdm/Xresources  .........."
  echo " Checking the Warning fot GUI Login .."
  DF=`cat /etc/X11/xdm/Xresources | grep "xlogin\*greeting"  | awk '{ print $2 }'`
  if [ "$DF" = "Welcome" ] ; then
  echo " Warning  is Found on the System ..------------PASSED----------- "
  else
  echo " Warning is not Found on The System !!!!!!!!!!!!! ----FAILED------- !!!!!!!!!!!!!!!! "
  tk="no"
  fi
 elif [ -f  /etc/X11/xdm/kdmrc  ] ; then
 
   echo " File Found  /etc/X11/xdm/kdmrc .........."
  echo " Checking the Warning fot GUI Login .."
  DF=`cat /etc/X11/xdm/kdmrc | grep GreetString | awk '{ print $1 }'`
  if [ "$DF" = "#GreetString" ] ; then
  echo " Warning  is Found on the System ..------------PASSED----------- "
  else
  echo " Warning is not Found on The System !!!!!!!!!!!!! ----FAILED------- !!!!!!!!!!!!!!!! "
    tk="no"
 
  fi
 
 elif [ -f /etc/X11/gdm/gdm.conf  ] ; then
   
   echo " File Found  /etc/X11/xdm/kdmrc .........."
  echo " Checking the Warning fot GUI Login .."
  DF=`cat  /etc/X11/gdm/gdm.conf  | grep  -w Greeter | grep -v  \#  | awk '{ print $2 }'`
  if [ "$DF" = "Welcome" ] ; then
  echo " Warning  is Found on the System ..------------PASSED----------- "
  else
  echo " Warning is not Found on The System !!!!!!!!!!!!! ----FAILED------- !!!!!!!!!!!!!!!! "
    tk="no"
 
  fi
else
echo " NO X11 Found !!!!!!!!!!!!!!  ---------PASSED --------------"
fi
 
 
if [ "$tk" = "no" ] ; then
echo " OverALL Test is FAILED --------- !!!!!! ----FAILED------- !!!!!! "
RSLT131="N0"
 
else
echo " OverALL Test is PASSED  --------- !!!!!! ----PASSED ------ !!!!!! "
RSLT131="YES"
 
fi

#######################################

echo "__________________________________________________________________________________"
echo "Transmission."
echo "__________________________________________________________________________________"
echo ""
 SERVICE="sshd"
if ps ax | grep -v grep | grep $SERVICE >/dev/null
then
    echo "$SERVICE   service running, !!!!!---------PASSED----------- !!!!!!!! !!!"
    RSLT132="YES"
else
  JKLM1=`chkconfig --list  |grep -w $SERVICE  | awk '{ print $5 $7 }'`
  if [ -n "$JKLM1" ] ; then
 
   if [  "$JKLM1" = "3:off5:off" ] ; then
   echo "  $SERVICE  is Disabled  $JKLM1 -------!!!!!!!! FAILED !!!!!! !!!"
    RSLT132="NO"
   else
   echo "   $SERVICE Is not Disabled!!!!!   $JKLM1 ------------PASSED ------------ !!!!!!!!!"
   echo " SSHD Service Can be Used for Transmission "
    RSLT132="YES"
  
    fi
   else
    echo " $SERVICE   IS NOT INSTALLED !!! FAILED  !!!!!!!"
    RSLT132="NO"
    fi
     
 fi
 
####################################### 
 
echo "__________________________________________________________________________________"
echo "File/Database Storage  md5 shadow in password "
echo "__________________________________________________________________________________"
echo ""
 
valk=`cat /etc/pam.d/system-auth|grep password |grep pam_unix.so|grep md5|grep -v \# |grep shadow | awk '{print $6}'`
if [ "$valk" = "md5" ] ; then
echo " $valk Found  on the System  /etc/pam.d/system-auth !!!!!!!!!!!!  --PASSED !!!!!!!!!!!!!!!!!!!"
RSLT133="YES"
else
echo " $valk is not Found on /etc/pam.d/system-auth   ----------------FAILED  ------------ !!!!! "
RSLT133="NO"
fi
 
#######################################

echo "__________________________________________________________________________________"
echo "Storage of passwords "
echo "__________________________________________________________________________________"
echo ""
 
valk=`grep md5 /etc/pam.d/system-auth | awk '{ print $6 }'`
if [ "$valk" = "md5" ] ; then
echo " $valk Found  on the System  /etc/pam.d/system-auth !!!!!!!!!!!!  --PASSED !!!!!!!!!!!!!!!!!!!"
RSLT134="YES"
else
echo " $valk is not Found on /etc/pam.d/system-auth   ----------------FAILED  ------------ !!!!! "
RSLT134="NO"
fi

#######################################

echo "__________________________________________________________________________________"
echo "Protection of private keys "
echo "__________________________________________________________________________________"
echo ""
if [ "$RSLT88" = "YES" ] ; then
echo " Since UserHome Directory is 700 so Private Keys is Protected .. ----PASSED-------"
RSLT135="YES"
else
echo " UserHome Directory is not  700 so Private Keys is  not Protected -------------FAILED  ------------ "
RSLT135="NO"
fi

#######################################

echo "__________________________________________________________________________________"
echo "net.ipv4.tcp_max_syn_backlog = 4096   "
echo "__________________________________________________________________________________"
echo ""
VAN=""
 
VAN=`grep net.ipv4.tcp_max_syn_backlog /etc/sysctl.conf   | awk -F= '{print $2 }'`
if [ -n "$VAN" ] ; then 
 
if [ "$VAN" -eq "4096" ] ; then
echo " Entry Found on /etc/sysctl.conf $VAN ------------ PASSED ---------------"
RSLT136a="YES"
 
else
echo " Entry on /etc/sysctl.conf $VAN ------------ FAILED  ---------------"
RSLT136a="NO"
 
fi
else
echo " Value Not Found .......  ------------FAILED --------------"
RSLT136a="NO"
 
fi
 
## Code is updated as per oct-2009 
 
echo "__________________________________________________________________________________"
echo "net.ipv4.tcp_syncookies=1  "
echo "__________________________________________________________________________________"
echo ""
 
VAN=""
 
VAN=`grep net.ipv4.tcp_syncookies /etc/sysctl.conf   | awk -F= '{print $2 }'| tail -1`
if [ -n "$VAN" ] ; then 
if [ "$VAN" -eq "1" ] ; then
echo " Entry Found on /etc/sysctl.conf $VAN ------------ PASSED ---------------"
RSLT136b="YES"
 
else
echo " Entry on /etc/sysctl.conf $VAN ------------ FAILED  ---------------"
RSLT136b="NO"
 
fi
else
echo " Value Not Found .......  ------------FAILED --------------"
RSLT136b="NO"
 
fi
 
 
echo "__________________________________________________________________________________"
echo "net.ipv4.conf.all.rp_filter = 1  "
echo "__________________________________________________________________________________"
echo ""
VAN=""
 
VAN=` grep net.ipv4.conf.all.rp_filter /etc/sysctl.conf   | awk -F= '{print $2 }'`
if [ -n "$VAN" ] ; then 
 
if [   "$VAN" -eq "1" ] ; then
echo " Entry Found on /etc/sysctl.conf $VAN ------------ PASSED ---------------"
RSLT136c="YES"
 
else
echo " Entry on /etc/sysctl.conf $VAN ------------ FAILED  ---------------"
RSLT136c="NO"
fi
 
else
echo " Value Not Found .......  ------------FAILED --------------"
RSLT136c="NO"
 
fi
 
 
echo "__________________________________________________________________________________"
echo "net.ipv4.conf.all.accept_source_route = 0 "
echo "__________________________________________________________________________________"
echo ""
VAN=""
VAN=` grep net.ipv4.conf.all.accept_source_route /etc/sysctl.conf   | awk -F= '{print $2 }'`
if [ -n "$VAN" ] ; then 
 
if [ "$VAN" -eq  "0" ] ; then
echo " Entry Found on /etc/sysctl.conf $VAN ------------ PASSED ---------------"
RSLT136d="YES"
 
else
echo " Entry on /etc/sysctl.conf $VAN ------------ FAILED  ---------------"
RSLT136d="NO"
fi
else
echo " Value Not Found .......  ------------FAILED --------------"
RSLT136d="NO"
 
fi
 
 
echo "__________________________________________________________________________________"
echo "net.ipv4.conf.all.accept_redirects = 0"
echo "__________________________________________________________________________________"
echo ""
VAN=""
 
VAN=` grep net.ipv4.conf.all.accept_redirects /etc/sysctl.conf | tail -1   | awk -F= '{print $2 }'`
if [ -n "$VAN" ] ; then 
 
if [ "$VAN" -eq "0" ] ; then
echo " Entry Found on /etc/sysctl.conf $VAN ------------ PASSED ---------------"
RSLT136e="YES"
 
else
echo " Entry on /etc/sysctl.conf $VAN ------------ FAILED  ---------------"
echo " Please add net.ipv4.conf.all.accept_redirects = 0  in /etc/sysctl.conf "
 
RSLT136e="NO"
fi
else
echo " Value Not Found .......  ------------FAILED --------------"
echo " Please add net.ipv4.conf.all.accept_redirects = 0  in /etc/sysctl.conf "
RSLT136e="NO"
 
fi
 
echo "__________________________________________________________________________________"
echo "net.ipv4.conf.all.secure_redirects = 0"
echo "__________________________________________________________________________________"
echo ""
VAN=""
 
VAN=` grep net.ipv4.conf.all.secure_redirects /etc/sysctl.conf   | awk -F= '{print $2 }'`
if [ -n "$VAN" ] ; then 
 
if [ "$VAN" -eq  "0" ] ; then
echo " Entry Found on /etc/sysctl.conf $VAN ------------ PASSED ---------------"
RSLT136f="YES"
 
else
echo " Entry on /etc/sysctl.conf $VAN ------------ FAILED  ---------------"
RSLT136f="NO"
fi
else
echo " Value Not Found .......  ------------FAILED --------------"
RSLT136f="NO"
 
fi
 
 
echo "__________________________________________________________________________________"
echo "net.ipv4.conf.default.rp_filter = 1"
echo "__________________________________________________________________________________"
echo ""
VAN=""
 
VAN=` grep net.ipv4.conf.default.rp_filter /etc/sysctl.conf  |head -1 | awk -F= '{print $2 }'`
if [ -n "$VAN" ] ; then 
 
if [ "$VAN" -eq  "1" ] ; then
echo " Entry Found on /etc/sysctl.conf $VAN ------------ PASSED ---------------"
RSLT136g="YES"
 
else
echo " Entry on /etc/sysctl.conf $VAN ------------ FAILED  ---------------"
RSLT136g="NO"
fi
else
echo " Value Not Found .......  ------------FAILED --------------"
RSLT136g="NO"
 
fi
 
echo "__________________________________________________________________________________"
echo "net.ipv4.conf.default.accept_source_route = 0"
echo "__________________________________________________________________________________"
echo ""
VAN=""
 
VAN=` grep net.ipv4.conf.default.accept_source_route /etc/sysctl.conf   | awk -F= '{print $2 }' | head -1`
if [ -n "$VAN" ] ; then 
 
if [ "$VAN" -eq  "0" ] ; then
echo " Entry Found on /etc/sysctl.conf $VAN ------------ PASSED ---------------"
RSLT136h="YES"
 
else
echo " Entry on /etc/sysctl.conf $VAN ------------ FAILED  ---------------"
RSLT136h="NO"
fi
else
echo " Value Not Found .......  ------------FAILED --------------"
RSLT136h="NO"
 
fi
 
 
echo "__________________________________________________________________________________"
echo "net.ipv4.conf.default.accept_redirects = 0"
echo "__________________________________________________________________________________"
echo ""
VAN=""
 
VAN=` grep net.ipv4.conf.default.accept_redirects /etc/sysctl.conf   | awk -F= '{print $2 }'`
if [ -n "$VAN" ] ; then 
 
if [ "$VAN" -eq  "0" ] ; then
echo " Entry Found on /etc/sysctl.conf $VAN ------------ PASSED ---------------"
RSLT136i="YES"
 
else
echo " Entry on /etc/sysctl.conf $VAN ------------ FAILED  ---------------"
RSLT136i="NO"
fi
else
echo " Value Not Found .......  ------------FAILED --------------"
RSLT136i="NO"
 
fi
 
 
echo "__________________________________________________________________________________"
echo "net.ipv4.conf.default.secure_redirects = 0 "
echo "__________________________________________________________________________________"
echo ""
VAN=""
 
VAN=` grep net.ipv4.conf.default.secure_redirects /etc/sysctl.conf   | awk -F= '{print $2 }'`
if [ -n "$VAN" ] ; then 
 
if [ "$VAN" -eq  "0" ] ; then
echo " Entry Found on /etc/sysctl.conf $VAN ------------ PASSED ---------------"
RSLT136j="YES"
 
else
echo " Entry on /etc/sysctl.conf $VAN ------------ FAILED  ---------------"
RSLT136j="NO"
fi
else
echo " Value Not Found .......  ------------FAILED --------------"
RSLT136j="NO"
 
fi
 
 
echo "__________________________________________________________________________________"
echo "net.ipv4.icmp_echo_ignore_broadcasts = 1 "
echo "__________________________________________________________________________________"
echo ""
VAN=""
 
VAN=` grep net.ipv4.icmp_echo_ignore_broadcasts /etc/sysctl.conf |head -n 1  | awk -F= '{print $2 }'`
if [ -n "$VAN" ] ; then 
 
if [ "$VAN" -eq  "1" ] ; then
echo " Entry Found on /etc/sysctl.conf $VAN ------------ PASSED ---------------"
RSLT136k="YES"
 
else
echo " Entry on /etc/sysctl.conf $VAN ------------ FAILED  ---------------"
echo " please add net.ipv4.icmp_echo_ignore_broadcasts = 1 in /etc/sysctl.conf and execute sysctl -p  "
RSLT136k="NO"
fi
else
echo " Value Not Found .......  ------------FAILED --------------"
echo " please add net.ipv4.icmp_echo_ignore_broadcasts = 1 in /etc/sysctl.conf and execute sysctl -p  "
 
RSLT136k="NO"
 
fi
 
echo "__________________________________________________________________________________"
echo "net.ipv4.ip_forward = 0 "
echo "__________________________________________________________________________________"
echo ""
VAN=""
 
VAN=` grep net.ipv4.ip_forward /etc/sysctl.conf | head -n 1  | awk -F= '{print $2 }'`
if [ -n "$VAN" ] ; then 
 
if [ "$VAN" -eq "0" ] ; then
echo " Entry Found on /etc/sysctl.conf $VAN ------------ PASSED ---------------"
RSLT136l="YES"
 
else
echo " Entry on /etc/sysctl.conf $VAN ------------ FAILED  ---------------"
RSLT136l="NO"
fi
else
echo " Value Not Found .......  ------------FAILED --------------"
RSLT136l="NO"
 
fi
 
echo "__________________________________________________________________________________"
echo "net.ipv4.conf.all.send_redirects = 0"
echo "__________________________________________________________________________________"
echo ""
VAN=""
 
VAN=` grep net.ipv4.conf.all.send_redirects /etc/sysctl.conf |head -1    | awk -F= '{print $2 }'`
if [ -n "$VAN" ] ; then 
 
if [ "$VAN" -eq  "0" ] ; then
echo " Entry Found on /etc/sysctl.conf $VAN ------------ PASSED ---------------"
RSLT136m="YES"
 
else
echo " Entry on /etc/sysctl.conf $VAN ------------ FAILED  ---------------"
RSLT136m="NO"
fi
else
echo " Value Not Found .......  ------------FAILED --------------"
RSLT136m="NO"
 
fi
 
 
echo "__________________________________________________________________________________"
echo "net.ipv4.conf.default.send_redirects = 0 "
echo "__________________________________________________________________________________"
echo ""
VAN=""
 
VAN=` grep net.ipv4.conf.default.send_redirects /etc/sysctl.conf  |head -1  | awk -F= '{print $2 }'`
if [ -n "$VAN" ] ; then 
 
if [ "$VAN" -eq  "0" ] ; then
echo " Entry Found on /etc/sysctl.conf $VAN ------------ PASSED ---------------"
RSLT136n="YES"
 
else
echo " Entry on /etc/sysctl.conf $VAN ------------ FAILED  ---------------"
RSLT136n="NO"
fi
else
echo " Value Not Found .......  ------------FAILED --------------"
RSLT136n="NO"
 
fi
 
 
echo "__________________________________________________________________________________"
echo "net.ipv4.tcp_max_orphans = 256 "
echo "__________________________________________________________________________________"
echo ""
VAN=""
 
VAN=` grep net.ipv4.tcp_max_orphans  /etc/sysctl.conf   | awk -F= '{print $2 }'`
if [ -n "$VAN" ] ; then 
 
if [ "$VAN" -eq "256" ] ; then
echo " Entry Found on /etc/sysctl.conf $VAN ------------ PASSED ---------------"
grep net.ipv4.tcp_max_orphans  /etc/sysctl.conf 
RSLT136o="YES"
 
else
echo " Entry on /etc/sysctl.conf $VAN ------------ FAILED  ---------------"
RSLT136o="NO"
grep net.ipv4.tcp_max_orphans  /etc/sysctl.conf 
 
fi
else
echo " Value Not Found .......  ------------FAILED --------------"
RSLT136o="NO"
 
fi
 
 
echo "__________________________________________________________________________________"
echo "net.ipv4.conf.all.log_martians = 1 "
echo "__________________________________________________________________________________"
echo ""
VAN=""
 
VAN=` grep net.ipv4.conf.all.log_martians  /etc/sysctl.conf  |head -1 | awk -F= '{print $2 }'`
if [ -n "$VAN" ] ; then 
 
if [ "$VAN" -eq  "1" ] ; then
echo " Entry Found on /etc/sysctl.conf $VAN ------------ PASSED ---------------"
RSLT136p="YES"
 
else
echo " Entry on /etc/sysctl.conf $VAN ------------ FAILED  ---------------"
RSLT136p="NO"
fi
else
echo " Value Not Found .......  !!!!!------------FAILED --------------!!!!!!!!"
RSLT136p="NO"
 
fi


if [ $RSLT136a="YES" -a $RSLT136b="YES" -a $RSLT136c="YES" -a $RSLT136d="YES" -a $RSLT136e="YES" -a $RSLT136f="YES" -a $RSLT136g="YES" -a $RSLT136h="YES" -a $RSLT136i="YES" -a $RSLT136j="YES" -a $RSLT136k="YES" -a $RSLT136l="YES" -a $RSLT136m="YES" -a $RSLT136n="YES" -a $RSLT136o="YES" -a $RSLT136p="YES" ] ; then
 RSLT136="YES"
else
 RSLT136="NO"
fi

#######################################

echo "__________________________________________________________________________________"
echo "Block System Accounts "
echo "__________________________________________________________________________________"
echo ""
FAILTEST=""
cat /etc/passwd | awk -F: '$3 < 500 { print $1 "+++" $7 }' | egrep -v  'root' >KLTEST
for i in `cat KLTEST`
 do
  MPTEST=`echo $i  | awk -F+ '{ print $4 }'`
   if [ "$MPTEST" = "/sbin/nologin" -o "$MPTEST" = "/bin/false" ] ; then
    echo " $i  Daemon User Should  Locked  $MPTEST  ------!!!PASSED !!!!!!!!!"
    else
    echo " $i     User Not Locked !!!! ----------------FAILED-----------------!!!!"
    FAILTEST="OK"
    fi
   
   done
 
echo "checking the  Daemon User  Locked or Not ...........  " 
 
for i in `cat KLTEST | awk -F- '{print $1 }' `
do
grep -w  $i /etc/shadow  >>lockeduser
done
 
for nnnk  in `cat lockeduser | awk -F: '{print $2 "-" $1}'`
do
GTJ=`echo "$nnnk" | awk -F- '{print $1 }' `
if [ "$GTJ" = "!!" -o "$GTJ" = "!" -o "$GTJ" = "!*" ] ;then
echo " Accounts  $nnnk is Locked ...................  PASSED "
else
echo " Account  $nnnk  is Unlocked ----------------------FAILED ----------- "
 FAILTEST="OK"
fi
 
done
 
 
   rm -f lockeduser
   rm -f KLTEST
if [ "$FAILTEST" = "OK" ] ;then
echo " "
RSLT137="NO"
echo "OverAll testing of Daemon User test is !!!!!!!!!!--------------------------FAILED-------------------!!!!!!!!!!!!!!!!!!!!!"
 
else
echo " "
 echo  "Overall testing of Daemon User test is !!!!!!!!!!--------------------------PASSED-------------------!!!!!!!!!!!!!!!!!!!!!"
RSLT137="YES"
 
fi

#######################################

echo "__________________________________________________________________________________"
echo "Verify there are no accounts with empty password fields. "
echo "__________________________________________________________________________________"
echo ""
NK=""
 
NK=`cat /etc/shadow | awk -F: ' $2 == "" {print $0 }'`
if [ -n "$NK" ] ; then
echo " Empty Password Found ............!!!!!!------------FAILED ---------------!!!!!!!!"
echo $NK
RSLT138="NO"
else
echo " Empty Password NOT  Found ............!!!----------PASSED --------------!!!!!!!!!!!!!!"
RSLT138="YES"
fi

#######################################

echo "__________________________________________________________________________________"
echo "Verify No Legacy entries exist in passwd, shadow and group files. "
echo "__________________________________________________________________________________"
echo ""
NK=""
 
NK=`grep ^+: /etc/passwd /etc/shadow /etc/group`
if [ -n "$NK" ] ; then
echo "Legacy ?+? entries exist in passwd!------------FAILED ---------------!!!!!!!!"
echo $NK
RSLT139="NO"
else
echo " No Legacy ?+? entries exist in passwd.!!!----------PASSED --------------!!!!!!!!!!!!!!"
RSLT139="YES"
fi

#######################################

echo "__________________________________________________________________________________"
echo "Verify that No UID 0 accounts exist othre than root.. "
echo "__________________________________________________________________________________"
echo ""
NK=""
NK=`cat /etc/passwd | awk -F: ' $3 == 0 {print $1 }' | grep -v root`
if [ -n "$NK" ] ; then
echo " UID 0 accounts exist othre than root. !------------FAILED ---------------!!!!!!!!"
echo $NK
RSLT140="NO"
else
echo " No UID 0 accounts exist othre than root.!!!----------PASSED --------------!!!!!!!!!!!!!!"
RSLT140="YES"
fi

#######################################

echo "__________________________________________________________________________________"
echo "No '.' or Group/World-Writable Directory In Root's PATH. "
echo "__________________________________________________________________________________"
echo ""
echo $PATH | egrep '(^|:)(\.|:|$)' >>TT
find `echo $PATH | tr ':' ' '` -type d \( -perm -002 -o -perm -020 \)  -ls  >>TTT
TTU=`cat TTT | wc -l `
if [ "$TTU" -eq "0" ] ; then
echo " NO Group/World-Writable Directory In Root's path ------PASSED --------------..."
RSLT141="YES"
 
else
echo "FOUND ..Group/World-Writable Directory In Root's path-----------------  FAILED -------------"
RSLT141="NO"
 
fi
rm -f TT
rm  -f TTT

#######################################

echo "__________________________________________________________________________________"
echo " HOME  Default Permissions at time of creation: 700 or 750 . Must be owned by userid)"
echo "__________________________________________________________________________________"
echo ""
 
for i in `cat /etc/passwd | awk -F: '$3 >=500 &&  $3 <= 6000 {print $6}'`
do
 
if  [ -d $i ] ; then
 
if ls -ld $i | grep -v  \>   > /dev/null 
then 
 
MKL=`ls -ld $i | awk '{ print $1 }' 2>/dev/null`
 
if [  "$MKL" = "drwx------" -o "$MKL" = "drwxr-x---" ] ; then
echo " Home Directory $i --- $MKL  ----------------PASSED---------- "
else
echo " Home Directory $i --- $MKL         !!!!!!!!! ------------FAILED -----------!!!!!!!!!!!"
FGH="NO"
fi
 
else
echo " This is not Directory   $i  "
MKN=`ls -ld $i | awk '{ print $NF }' 2>/dev/null`
MKL=`ls -ld $MKN | awk '{ print $1 }' 2>/dev/null`
 
if [ "$MKL" = "drwx------" -o "$MKL" = "drwxr-x---" ] ; then
echo " Home Directory $i --- $MKL  ----------------PASSED---------- "
else
echo " Home Directory $i --- $MKL         !!!!!!!!! ------------FAILED -----------!!!!!!!!!!!"
FGH="NO"
fi
 
 
fi
else
echo " $i does not Seeems like Directory it may does not exists "
fi
done
 
 
if [ "$FGH" = "NO" ] ; then
echo " OverALL Testing is FAILED !!!! ------------FAILED -----------!!!!!!! "
echo " Note -: Please Make the Home Directories Permission to 750 or 700 as applicable "
RSLT142="NO"
else
echo " OverALL Testing is PASSED  ----------------PASSED----------      "
RSLT142="YES"
fi

#######################################

echo "__________________________________________________________________________________"
echo " No User Dot- Files Should Be World-Writable "
echo "__________________________________________________________________________________"
echo ""
 
for DIR in  `awk -F: '($3 >= 500 && $3 < 60000) {print $6 }' /etc/passwd`;
do
for FILE in $DIR/.[A-Za-z0-9]* ; do
if [ ! -h "$FILE" -a -f"$FILE" ]; then
 find $FILE   -perm -0002  ! -perm -1000 -type d >>HJU
fi
done
done
FGT=`cat   HJU | wc -l `
if [ "$FGT" = "0" ] ; then
echo " Word Writable Directory  Not Found !on User Home Directory  ------------PASSED ----------"
RSLT143="YES"
 
else
echo " Word Writable Directory Found !!! on User Home Directory .. -----------------FAILED ---------"
RSLT143="NO"
cat HJU
 
fi
rm -f HJU

#######################################

RSLT144="YES"

#######################################

echo "__________________________________________________________________________________"
echo "ROOT password must be assigned"
echo "__________________________________________________________________________________"
echo ""
RTY=`grep root /etc/shadow | awk -F: ' $2 == "" {print $2 }'`
if [ -n "$RTY" ] ; then
echo " Root Password is not Assigned !!!!!!!!!!!!!!  ----------FAILED-------------"
RSLT145="NO"  
else
echo " Root Password is Assined !!!!!!!!!!!!!!!!!!! --------PASSED -----------------"
RSLT145="YES"  
fi

#######################################

echo "__________________________________________________________________________________"
echo "/etc/pam.d/other"
echo "__________________________________________________________________________________"
echo ""
 
for i in `grep pam_deny /etc/pam.d/other | egrep 'account|auth'  | awk '{print $2}'`
  do
  if [ "$i" = "required" ] ; then
  echo " pam_deny entry Found on the /etc/pam.d/other --------------PASSED-------------- "
  else
  echo " pam_deny entry  not Found on the /etc/pam.d/other -----------FAILED------------  "
  yu="nn"
 
  fi
 done
 if [ "yu" = "nn" ] ; then
 echo " pam_deny Entry is wrong ......FAILED -------------- " 
   RSLT146="NO"  
 
 else
 echo " pam_deny Entry is ok --------------PASSED ------------------"
   RSLT146="YES"  
 
 fi

#######################################

echo "__________________________________________________________________________________"
echo "  /etc/ftpusers  root id must exists"
echo "__________________________________________________________________________________"
echo ""
 SERVICE="vsftpd"
 JKLM1=`chkconfig --list  |grep -w $SERVICE  | awk '{ print $5 $7 }'`
  if [ -n "$JKLM1" ] ; then
    echo " $SERVICE is installed ..........."
      if [ -f /etc/ftpusers -o -f /etc/vsftpd.ftpusers ] ; then
      echo " FIle Found  on the system  ...!!!!!PASSED !!!!!!!!!. "
       jkh=`grep root  /etc/vsftpd.ftpusers /etc/ftpusers 2>/dev/null | awk -F: '{print $2}'|tail -1`
       jkh2=`ls -l /etc/ftpusers | awk ' { print $3 $4 } ' `
         if [ "$jkh" = "root"  -a "$jkh2" = "rootroot" ] ; then
          echo " root Entry Found on  the System ---- PASSED !!!!!!!!!!!!!!!!"
          RSLT147="YES" 
           #####RSLT128="YES"
          else
           echo "root Entry NOT  Found on  the System ---- FAILED !!!!!!!!!!!!!!!"
           #####RSLT128="NO" 
            RSLT147="NO"
 
           fi
       else
       echo " None of the File Found !!!!!!!!!! ----------FAILED---------- !!!!!!!!!!!!"
 
       RSLT147="NO"
        ####RSLT128="NO"
       fi
   else
   echo "$SERVICE is NOT INSTALLED on the System -- !!!!!!!!!PASSED !!!!!!!!!!!!!! "
  RSLT147="YES"  
   ####RSLT128="YES"
  fi

#######################################

#echo "__________________________________________________________________________________"
#echo "Lockout Accounts After 5 Failures"
#echo "__________________________________________________________________________________"
#echo ""
 
 
#MKL=""
#MKL=`grep  -w deny /etc/pam.d/system-auth  | awk '{print $4 }' | awk -F= '{print $2 }'| grep -v -i user`
#MKL8=` grep onerr /etc/pam.d/system-auth | awk '{print $5 }'| grep -v sense`
#if [ "$MKL" = "3"  ] ; then
#echo " Lockout Accounts After 3 Failures Entry Found !!!!!!!!  --------------!!PASSED---------------!!! "
#echo " Lockout Attempts set in /etc/pam.d/system_auth is $MKL "
#echo " Line   auth  required  /lib/security/\$ISA/pam_tally.so onerr=fail no_magic_root  present " 
 
#grep  -w deny /etc/pam.d/system-auth | grep -v user
#RSLT148="YES"
#else
#echo " Lockout Accounts After 3 Failures Entry Not Found !!!!! -------------FAILED --------------------"
#echo " Lockout Attempts set in /etc/pam.d/system_auth is $MKL "
#echo " Please add [account     required      /lib/security/\$ISA/pam_tally.so deny=5 reset no_magic_root] "
#echo " Line   auth  required  /lib/security/\$ISA/pam_tally.so onerr=fail no_magic_root should be present " 
#echo " Current settings ========`grep  -w deny /etc/pam.d/system-auth | grep -v user` "
#grep onerr /etc/pam.d/system-auth | awk '{print $5 }'
 
#RSLT148="NO"
 
#fi

#######################################

echo "__________________________________________________________________________________"
echo "Restrict Root Logins to System Console"
echo "__________________________________________________________________________________"
echo ""
FGT=`cat /etc/securetty | grep tty | grep -v \# |wc -l`
NUMVC=`cat /etc/securetty | grep vc | grep -v \# |wc -l `
PERSYS=`ls -l /etc/securetty | awk '{print $1$3$4}'`
CHKCON=` cat /etc/securetty | grep console | grep -v \# |wc -l`
 
if [ "$FGT" -eq "6" -a "$PERSYS" = "-r--------rootroot"  -a  "$NUMVC" = "11"  -a "$CHKCON" = "1" ] ; then
echo " Login is Ristrict ok !!!!!!!!!-------PASSED------------!!!!!!!!"
RSLT149="YES"
 
else
echo " Root Login is Not Ristrict !!---------FAILED --------!!!!!!!!!!!!"
echo " Please check tty [ it should be 6 Enrtries]  vc [ 11 Entries ] console [1 ] permission =600  ownership root:root "
RSLT149="NO"
 
fi

#######################################

echo "__________________________________________________________________________________"
echo "Limit Access to the Root Account from su"
echo "__________________________________________________________________________________"
echo ""
 
ERT=`cat /etc/pam.d/su | grep pam_wheel.so | grep required  | grep -v \# | awk '{print $2}'`
if [ -n "$ERT" ] ; then
echo "pam_wheel is enabled on the system ............. ----------PASSED --------------"
echo " line auth required /lib/security/$ISA/pam_wheel.so use_uid should be there "
RSLT150="YES"
else
echo " pam_wheel is Enabled on the system ............. --------FAILED  ---------------"
echo " line auth required /lib/security/$ISA/pam_wheel.so use_uid should be there "
 
RSLT150="NO"
fi

####################################### 
 
echo "__________________________________________________________________________________"
echo "Webmin"
echo "__________________________________________________________________________________"
echo ""
 
 SERVICE="webmin"
if ps ax | grep -v grep | grep $SERVICE >/dev/null
then
   echo "$SERVICE service running, FAILED !!!  !!!"
  RSLT151="NO"
else
  JKLM1=`chkconfig --list  |grep -w $SERVICE  | awk '{ print $5 $7 }'`
  if [ -n "$JKLM1" ] ; then
 
   if [  "$JKLM1" = "3:off5:off" ] ; then
   echo " $SERVICE  is Disabled   PASSED !!!"
   RSLT151="YES"
   else
   echo "  $SERVICE Is not Disabled !!!!!------------FAILED------------- !!!!!!!!!!!"
     RSLT151="NO"
  
    fi
   else
    echo " $SERVICE   IS NOT INSTALLED      !!! PASSED !!!!!!!"
    RSLT151="YES"
    fi
     
 fi
 
#######################################
 
echo "__________________________________________________________________________________"
echo "Remove .rhosts Support In PAM Configuration Files"
echo "__________________________________________________________________________________"
echo ""
 
for ff in `ls /etc/pam.d/* ` ; do grep  rhost_auth  $ff  >>rrrrlist ; done
 
MKJ=`cat rrrrlist | wc -l `
 
if [ "$MKJ" -eq "0" ] ; then
echo " No rhost_auth File Found on the system ............ ------------ PASSED -----------" 
RSLT152="YES"
else
 
for FILE in `cat rrrrlist `
 
do
 
if [ -f  $FILE ] ; then
   
  echo " rhosts found ...     ---------  FAILED -------------- "
  RSLT152="NO"
  else
  echo " rhosts not found ... ------------ PASSED ------------ "
    RSLT152="YES"
 fi
done
 
fi

#######################################

echo "__________________________________________________________________________________"
echo "    /etc/passwd must not contain passwords  "
echo "__________________________________________________________________________________"
echo ""
  for CN in  `cat /etc/passwd | awk -F: '$3>=500 && $3 <=60000 { print $1 ":" $2}'`
   do
   JKL=`echo $CN | awk -F: '{ print $2 }'`
   if [ "$JKL" = "x" ] ; then
   echo " USER -- $CN encrypted password is set !! !!!!!!PASSED !!!!!!!! "
   
    
   else
   echo " encrypted password is  not set !!!!!!!!!! ----FAILED --- !!!!!!!!!!!!!!!! $CN "
   TKL=2
   fi
   done
 
 echo " For Locked Account We do this Activity on Every Months ...."
 
  if [ "$TKL" = "2" ] ; then
    echo " OverAll test is --------------FAILED ------------  "
    RSLT153="NO"
    else
    echo " OverAll test is --------------PASSED ------------"
     RSLT153="YES"
  fi 

#######################################

echo "__________________________________________________________________________________"
echo "    /etc/passwd file contains encrypted passwords and has permission bits set to 0640 or more restrictive  "
echo "__________________________________________________________________________________"
echo ""
  for CN in  `cat /etc/passwd | awk -F: '$3>=500 && $3 <=60000 { print $1 ":" $2}'`
   do
   JKL=`echo $CN | awk -F: '{ print $2 }'`
   if [ "$JKL" = "x" ] ; then
   echo " USER -- $CN encrypted password is set !! !!!!!!PASSED !!!!!!!! "
   
    
   else
   echo " encrypted password is  not set !!!!!!!!!! ----FAILED --- !!!!!!!!!!!!!!!! $CN "
   TKL=2
   fi
   done
 
 MKL=`ls -ld /etc/passwd | awk '{ print $1 }' 2>/dev/null`
 
if [ "$MKL" = "-rw-r--r--" ] ; then
echo " Permision bit OK --- $MKL  ----------------PASSED---------- "
FGH="YES"
else
echo " Permission bit not OK --- $MKL         !!!!!!!!! ------------FAILED -----------!!!!!!!!!!!"
FGH="NO"
fi

 echo " For Locked Account We do this Activity on Every Months ...."
 
  if [ "$TKL" = "2" -a $FGH = "NO" ] ; then
    echo " OverAll test is --------------FAILED ------------  "
    RSLT154="NO"
    else
    echo " OverAll test is --------------PASSED ------------"
     RSLT154="YES"
  fi 

#######################################

echo "__________________________________________________________________________________"
echo "    Files that have been stored into a writeable directory must be examined before being moved to a readable directory  "
echo "__________________________________________________________________________________"
echo ""

echo "Manually checked for confidential information, inappropriate materials, etc"
RSLT155="YES"

#######################################
#######################################
#######################################
#######################################


echo "    GSD-REPORT                                    FINAL  REPORT --- `hostname`                                            "
echo `/sbin/ifconfig  | grep 'inet addr:'| grep -v '127.0.0.1' | cut -d: -f2 | awk '{ print $1}' | grep ^10. | head -1`
echo `hostname`
echo    "$CHKPNT1       >>        $RSLT1  "
echo    "$CHKPNT2       >>        $RSLT2  "
echo    "$CHKPNT3       >>        $RSLT3  "
echo    "$CHKPNT4       >>        $RSLT4  "
echo    "$CHKPNT5       >>        $RSLT5  "
echo    "$CHKPNT6       >>        $RSLT6  "
echo    "$CHKPNT7       >>        $RSLT7  "
echo    "$CHKPNT8       >>        $RSLT8  "
echo    "$CHKPNT9       >>        $RSLT9  "
echo    "$CHKPNT10      >>        $RSLT10 "
echo    "$CHKPNT11      >>        $RSLT11 "
echo    "$CHKPNT12      >>        $RSLT12 "
echo    "$CHKPNT13      >>        $RSLT13 "
echo    "$CHKPNT14      >>        $RSLT14 "
echo    "$CHKPNT15      >>        $RSLT15 "
echo    "$CHKPNT16      >>        $RSLT16 "
echo    "$CHKPNT17      >>        $RSLT17 "
echo    "$CHKPNT18      >>        $RSLT18 "
echo    "$CHKPNT19      >>        $RSLT19 "
echo    "$CHKPNT20      >>        $RSLT20 "
echo    "$CHKPNT21      >>        $RSLT21 "
echo    "$CHKPNT22      >>        $RSLT22 "
echo    "$CHKPNT23      >>        $RSLT23 "
echo    "$CHKPNT24      >>        $RSLT24 "
echo    "$CHKPNT25      >>        $RSLT25 "
echo    "$CHKPNT26      >>        $RSLT26 "
echo    "$CHKPNT27      >>        $RSLT27 "
echo    "$CHKPNT28      >>        $RSLT28 "
echo    "$CHKPNT29      >>        $RSLT29 "
echo    "$CHKPNT30      >>        $RSLT30 "
echo    "$CHKPNT31      >>        $RSLT31 "
echo    "$CHKPNT32      >>        $RSLT32 "
echo    "$CHKPNT33      >>        $RSLT33 "
echo    "$CHKPNT34      >>        $RSLT34 "
echo    "$CHKPNT35      >>        $RSLT35 "
echo    "$CHKPNT36      >>        $RSLT36 "
echo    "$CHKPNT37      >>        $RSLT37 "
echo    "$CHKPNT38      >>        $RSLT38 "
echo    "$CHKPNT39      >>        $RSLT39 "
echo    "$CHKPNT40      >>        $RSLT40 "
echo    "$CHKPNT41      >>        $RSLT41 "
echo    "$CHKPNT42      >>        $RSLT42 "
echo    "$CHKPNT43      >>        $RSLT43 "
echo    "$CHKPNT44      >>        $RSLT44 "
echo    "$CHKPNT45      >>        $RSLT45 "
echo    "$CHKPNT46      >>        $RSLT46 "
echo    "$CHKPNT47      >>        $RSLT47 "
echo    "$CHKPNT48      >>        $RSLT48 "
echo    "$CHKPNT49      >>        $RSLT49 "
echo    "$CHKPNT50      >>        $RSLT50 "
echo    "$CHKPNT51      >>        $RSLT51 "
echo    "$CHKPNT52      >>        $RSLT52 "
echo    "$CHKPNT53      >>        $RSLT53 "
echo    "$CHKPNT54      >>        $RSLT54 "
echo    "$CHKPNT55      >>        $RSLT55 "
echo    "$CHKPNT56      >>        $RSLT56 "
echo    "$CHKPNT57      >>        $RSLT57 "
echo    "$CHKPNT58      >>        $RSLT58 "
echo    "$CHKPNT59      >>        $RSLT59 "
echo    "$CHKPNT60      >>        $RSLT60 "
echo    "$CHKPNT61      >>        $RSLT61 "
echo    "$CHKPNT62      >>        $RSLT62 "
echo    "$CHKPNT63      >>        $RSLT63 "
echo    "$CHKPNT64      >>        $RSLT64 "
echo    "$CHKPNT65      >>        $RSLT65 "
echo    "$CHKPNT66      >>        $RSLT66 "
echo    "$CHKPNT67      >>        $RSLT67 "
echo    "$CHKPNT68      >>        $RSLT68 "
echo    "$CHKPNT69      >>        $RSLT69 "
echo    "$CHKPNT70      >>        $RSLT70 "
echo    "$CHKPNT71      >>        $RSLT71 "
echo    "$CHKPNT72      >>        $RSLT72 "
echo    "$CHKPNT73      >>        $RSLT73 "
echo    "$CHKPNT74      >>        $RSLT74 "
echo    "$CHKPNT75      >>        $RSLT75 "
echo    "$CHKPNT76      >>        $RSLT76 "
echo    "$CHKPNT77      >>        $RSLT77 "
echo    "$CHKPNT78      >>        $RSLT78 "
echo    "$CHKPNT79      >>        $RSLT79 "
echo    "$CHKPNT80      >>        $RSLT80 "
echo    "$CHKPNT81      >>        $RSLT81 "
echo    "$CHKPNT82      >>        $RSLT82 "
echo    "$CHKPNT83      >>        $RSLT83 "
echo    "$CHKPNT84      >>        $RSLT84 "
echo    "$CHKPNT85      >>        $RSLT85 "
echo    "$CHKPNT86      >>        $RSLT86 "
echo    "$CHKPNT87      >>        $RSLT87 "
echo    "$CHKPNT88      >>        $RSLT88 "
echo    "$CHKPNT89      >>        $RSLT89 "
echo    "$CHKPNT90      >>        $RSLT90 "
echo    "$CHKPNT91      >>        $RSLT91 "
echo    "$CHKPNT92      >>        $RSLT92 "
echo    "$CHKPNT93      >>        $RSLT93 "
echo    "$CHKPNT94      >>        $RSLT94 "
echo    "$CHKPNT95      >>        $RSLT95 "
echo    "$CHKPNT96      >>        $RSLT96 "
echo    "$CHKPNT97      >>        $RSLT97 "
echo    "$CHKPNT98      >>        $RSLT98 "
echo    "$CHKPNT99      >>        $RSLT99 "
echo    "$CHKPNT100     >>        $RSLT100 "
echo    "$CHKPNT101     >>        $RSLT101 "
echo    "$CHKPNT102     >>        $RSLT102 "
echo    "$CHKPNT103     >>        $RSLT103 "
echo    "$CHKPNT104     >>        $RSLT104 "
echo    "$CHKPNT105     >>        $RSLT105 "
echo    "$CHKPNT106     >>        $RSLT106 "
echo    "$CHKPNT107     >>        $RSLT107 "
echo    "$CHKPNT108     >>        $RSLT108 "
echo    "$CHKPNT109     >>        $RSLT109 "
echo    "$CHKPNT110     >>        $RSLT110 "
echo    "$CHKPNT111     >>        $RSLT111 "
echo    "$CHKPNT112     >>        $RSLT112 "
echo    "$CHKPNT113     >>        $RSLT113 "
echo    "$CHKPNT114     >>        $RSLT114 "
echo    "$CHKPNT115     >>        $RSLT115 "
echo    "$CHKPNT116     >>        $RSLT116 "
echo    "$CHKPNT117     >>        $RSLT117 "
echo    "$CHKPNT118     >>        $RSLT118 "
echo    "$CHKPNT119     >>        $RSLT119 "
echo    "$CHKPNT120     >>        $RSLT120 "
echo    "$CHKPNT121     >>        $RSLT121 "
echo    "$CHKPNT122     >>        $RSLT122 "
echo    "$CHKPNT123     >>        $RSLT123 "
echo    "$CHKPNT124     >>        $RSLT124 "
echo    "$CHKPNT125     >>        $RSLT125 "
echo    "$CHKPNT126     >>        $RSLT126 "
echo    "$CHKPNT127     >>        $RSLT127 "
echo    "$CHKPNT128     >>        $RSLT128 "
echo    "$CHKPNT129     >>        $RSLT129 "
echo    "$CHKPNT130     >>        $RSLT130 "
echo    "$CHKPNT131     >>        $RSLT131 "
echo    "$CHKPNT132     >>        $RSLT132 "
echo    "$CHKPNT133     >>        $RSLT133 "
echo    "$CHKPNT134     >>        $RSLT134 "
echo    "$CHKPNT135     >>        $RSLT135 "
echo    "$CHKPNT136     >>        $RSLT136 "
echo    "$CHKPNT137     >>        $RSLT137 "
echo    "$CHKPNT138     >>        $RSLT138 "
echo    "$CHKPNT139     >>        $RSLT139 "
echo    "$CHKPNT140     >>        $RSLT140 "
echo    "$CHKPNT141     >>        $RSLT141 "
echo    "$CHKPNT142     >>        $RSLT142 "
echo    "$CHKPNT143     >>        $RSLT143 "
echo    "$CHKPNT144     >>        $RSLT144 "
echo    "$CHKPNT145     >>        $RSLT145 "
echo    "$CHKPNT146     >>        $RSLT146 "
echo    "$CHKPNT147     >>        $RSLT147 "
echo    "$CHKPNT148     >>        $RSLT148 "
echo    "$CHKPNT149     >>        $RSLT149 "
echo    "$CHKPNT150     >>        $RSLT150 "
echo    "$CHKPNT151     >>        $RSLT151 "
echo    "$CHKPNT152     >>        $RSLT152 "
echo    "$CHKPNT153     >>        $RSLT153 "
echo    "$CHKPNT154     >>        $RSLT154 "
echo    "$CHKPNT155     >>        $RSLT155 "

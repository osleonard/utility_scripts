#!/bin/bash 
#Replace 127.0.0.1 with Destination host
HOST='127.0.0.1' 
#Replace nnamdi with username
USER='nnamdi'
#Replace pass1234 with password
PASSWORD='pass1234'
#Replace /home/bblite/ with the path to the upload directory
DIRPATH='/home/bblite/'
cd $DIRPATH
for file in `ls $DIRPATH`
do 
ftp -n -v $HOST <<END_OF_SESSION 
user $USER $PASSWORD 
binary
put $file
bye 
END_OF_SESSION

  echo "Completed uploading $file to $HOST"
  rm -rf "$file"
  echo "Deleted $file ..........."
done

#!/bin/bash
#File to parse broker logs to determine deductions that failed on AIR even when AIR returned previously with adequate AIRTIME for the subscriber

for i in `grep --binary-file=text "\<Subscriber.*balance now is.*, new balance if deduction occurs would be less by\>" catalina.out | sed -e 's/:[0-9][0-9],.*Subscriber\ /,/g' -e 's/\ balance\ now\ is\ /,/g' -e 's/,\ new.*$//g' -e 's/\ /+/g'`; #collect all TIME,MSISDN and AMOUNT from the line where account balance was checked, substituting the space between time with a + because it is read as a new line in the loop
do TIME=`echo $i | cut -d , -f1 | sed -e 's/\+/\ /g'`; #replacing the + with a space
  MSISDN=`echo $i | cut -d , -f2`; 
  AMOUNT=`echo $i | cut -d , -f3`; 
	PARTFORDEDUCT=`grep "$TIME.*The\ actual\ amount\ to\ deduct\ from\ subscriber.*$MSISDN" catalina.out -A 4`; #For all the MSISDN look for matching time of the DAY with minutes as the lowest denom where deduction was attempted
  echo $PARTFORDEDUCT >> deduct.txt #store match in a temporary file
	if [[ -n `grep -E "<name>responseCode</name><value><i4>1[0-9][0-9]</i4></value>" deduct.txt` ]]; #check if response code is greater than or equal to 100
  then echo "At $TIME AIR returned with balance $AMOUNT for $MSISDN" >> failedDeduction.txt;
    echo "At $TIME AIR returned with balance $AMOUNT for $MSISDN";
		echo $PARTFORDEDUCT >> failedDeduction.txt;
		echo "This is error code: "`tail -1  deduct.txt | sed -e 's/.*responseCode<\/name><value><i4>//g' -e 's/<.*//g'`
		echo "Error code from AIR is: "`tail -1  deduct.txt | sed -e 's/.*responseCode<\/name><value><i4>//g' -e 's/<.*//g'` >> failedDeduction.txt;
		echo "" >> failedDeduction.txt;
		echo "" >> failedDeduction.txt;
	fi;
	rm deduct.txt;
done

/*Query for multiple deductions*/
select msisdn,description,count(msisdn) from
(select * from
(select * from transactionlog where date_created BETWEEN TRUNC(SYSDATE - 1) AND TRUNC(SYSDATE) - 1/86400)
where description = 'RENEWAL' or description = 'DEDUCT AIRTIME' order by msisdn)
group by msisdn,description having count(msisdn) > 1 order by msisdn;

/*Select subscribers with prepaidsubscriber equal to 0*/
select * from subscriber where prepaidsubscriber = 0;

/*Query for multiple subscriber records*/
select msisdn from subscriber group by msisdn having count(msisdn) > 1 order by msisdn;

/*Query for an msisdn from the subscriber table*/
select * from subscriber where msisdn = '233265379769';

/*Query for an msisdn from the transactionlog table*/
select * from transactionlog where msisdn = '233264491864';

/*Delete subscriber from the subscriber table*/
delete subscriber where id = 'B79E4AAB879B4894E0405D0A55554C6D';



/*Update subscriber records in subscriber table*/
update subscriber set shortcode = 'bismonth' where id = 'ab67381ec8a598a1958afdde029ae';
update subscriber set prepaidsubscriber = 1 where id = '403a30c823e92710ab78114558aea6';

/*Subscribers that scheduler did not capture*/
select * from
(select * from subscriber where to_char(next_subscription_date, 'DD-MON-YYYY') != '07-APR-2012')
where next_subscription_date < sysdate and statusid = 'Active' order by next_subscription_date desc;

/*Subscribers that the difference between their next subscription date and last subscription date is not equal to their servicetype*/
select * from subscriber where (trunc(next_subscription_date) - trunc(last_subscription_date)) != servicetype;

/*Subscribers that the difference between their next subscription date and last subscription date is less than their servicetype*/
select * from subscriber where (trunc(next_subscription_date) - trunc(last_subscription_date)) < servicetype;

/*Insert to DB*/
insert into subscriber (id,msisdn) values('ab67381ec8a598a1958afdde029ae','233265379769');

/*New query for multiple deduction with reference to only successful ones*/
select msisdn,description,count(msisdn) from (select * from (select * from transactionlog where date_created BETWEEN TRUNC(SYSDATE - 1) AND TRUNC(SYSDATE) - 1/86400) where description = 'RENEWAL' or description = 'DEDUCT AIRTIME' and status='SUCCESSFUL' order by msisdn) group by msisdn,description having count(msisdn) > 1 order by msisdn;

/*Sqlplus things
sqlplus username/password@url:port/serviceid (if it does not work remove the url:port/)
alternatively sqlplus username/password@serviceid
set colsep ,     -- separate columns with a comma
set pagesize 0   -- only one header row
set trimspool on -- remove trailing blanks
set headsep off  -- this may or may not be useful...depends on your headings.
set linesize 700   -- X should be the sum of the column widths

spool myfile.csv -- used to log the queries into the file myfile.csv

*/
/*Query for shortcode,msisdn,servicetype,serviceplanid that are in the subscriber table and not in the billingplan table*/
select s.shortcode,s.msisdn,s.servicetype,s.serviceplanid from subscriber s where s.next_subscription_date <= sysdate and s.statusid = 'Active' and s.shortcode not in (select shortcode from billingplan);

/*Unlock Oracle User Account*/
ALTER USER username ACCOUNT UNLOCK;

/* Modify Table Column Size*/
ALTER TABLE table_name
  MODIFY column_name column_type;

/* Add columns to an existing table */
ALTER TABLE table_name
  ADD (column_1 column-definition,
       column_2 column-definition,
       ...
       column_n column_definition);

/*Change user password*/
alter user bblite identified by new_password replace old_pass;

/*Cross compares subscriber table with billingplan table to return shortcodes*/
select billingplan.shortcode, subscriber.shortcode from subscriber inner join billingplan on subscriber.serviceplanid = billingplan.services where subscriber.servicetype = billingplan.validity;

/*Updates subcriber table with matching shortcodes from billingplan table where the subscriber servicetype and serviceplanid equals the billingplan validity and services respectively*/
update subscriber sub set sub.shortcode = (select billingplan.shortcode from billingplan where sub.serviceplanid = billingplan.services and sub.servicetype = billingplan.validity and rownum <= 1)
where sub.shortcode is not null
and exists
(select billingplan.shortcode from billingplan where sub.serviceplanid = billingplan.services and sub.servicetype = billingplan.validity and rownum <= 1);

/*RENAME COLUMN*/
alter table subscriber rename column auto_renew to autorenew;

/*GET ALL SHORTCODES PROVISIONED FROM THE GUI AND THE USERS WHO PROVISIONED THEM*/
select activitylogger.MSISDN, activitylogger.HOST_IP, activitylogger.USERNAME, subscriber.shortcode from activitylogger inner join subscriber on activitylogger.MSISDN = subscriber.msisdn where activitylogger.action = 'Create new' and subscriber.shortcode LIKE '%prep';

/*GET ALL SHORTCODES PROVISIONED FROM THE GUI AND THE USERS WHO PROVISIONED THEM AND THE DAY THEY WHERE PROVISIONED*/
select activitylogger.MSISDN, activitylogger.date_created, activitylogger.HOST_IP, activitylogger.USERNAME, subscriber.shortcode from activitylogger inner join subscriber on activitylogger.MSISDN = subscriber.msisdn where activitylogger.action = 'Create new' and trunc(activitylogger.date_created) = trunc(subscriber.last_subscription_date) and subscriber.shortcode LIKE '%prep';

/*GET ALL SHORTCODES PROVISIONED YESTERDAY FROM THE GUI AND THE USERS WHO PROVISIONED THEM*/
select activitylogger.MSISDN, activitylogger.date_created, activitylogger.HOST_IP, activitylogger.USERNAME, subscriber.shortcode from activitylogger inner join subscriber on activitylogger.MSISDN = subscriber.msisdn where activitylogger.action = 'Create new' and trunc(activitylogger.date_created) = trunc(sysdate - 1) and subscriber.shortcode LIKE '%prep';

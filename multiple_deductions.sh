#!/bin/bash
filename="/home/bblite/subscribers/multiple_deductions_for_`date -d '1 day ago' +'%a_%d-%m-%Y'`.csv"
query=$(sqlplus bblite/bblite@10.93.85.85/bblite <<EOF
set colsep ,     -- separate columns with a comma
set pagesize 0   -- only one header row
set trimspool on -- remove trailing blanks
set headsep off  -- this may or may not be useful...depends on your headings.
set sqlprompt ''
set linesize 500
set feedback off
spool $filename
select msisdn,description,count(msisdn) from
(select * from
(select * from transactionlog where date_created BETWEEN TRUNC(SYSDATE - 1) AND TRUNC(SYSDATE) - 1/86400)
where description = 'RENEWAL' or description = 'DEDUCT AIRTIME' order by msisdn)
group by msisdn,description having count(msisdn) > 1 order by msisdn;
exit;
EOF)


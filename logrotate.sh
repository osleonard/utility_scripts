#!/bin/bash
function rotate {
  fname=$1
  value=20000 #File maximum accepted value
  FILESIZE=`ls -l $fname | awk '{print $5}'`
  DATE=`date +%Y-%m-%d-%T`
  #filesize in number
  size=`echo $FILESIZE | sed -r 's/M|G|K//'`
  DESTINATIONFOLDER="/BBLITE02/"

  if [[ ${size%%.*} -ge $value ]]
  then 
  echo $fname 
  NEWFILENAME=`echo $fname | awk -F / '{print $NF}'`
  echo "COPYING $fname to $DESTINATIONFOLDER$NEWFILENAME$DATE"
  sleep 5
  cp $fname "$DESTINATIONFOLDER/$NEWFILENAME$DATE"
  echo > $fname
  else
  echo "$fname is smaller than 200MB"
  fi
}
#Specify files to be truncated in the format rotate absolute_path to file e.g:
#rotate /home/bblite/SDP-server/logs/catalina.out 
#the user requires permission to the file to be truncated
rotate /home/bblite/AirtelTanzaniaBB_Server/logs/catalina.out
rotate /home/bblite/blackberryWS-cfx/logs/catalina.out
rotate /home/bblite/SDP-Scheduler-server/logs/catalina.out

for i in `find /var/log/kannel -type f`
  do rotate "$i"
done
